if [ -z ${expDir+x} ]; then
    expDir="experiments-collapsed"
fi
if [ -z ${trainPath+x} ]; then
    echo "Please specify training data path with trainPath=<path> before the command"
    exit 1
fi
if [ -z ${devPath+x} ]; then
    devOpt=""
else
    devOpt="-devPath ${devPath} ${devPath}"
fi
if [ -z ${useSpecificIndicator+x} ]; then
    useSpecificIndicator="true"
fi
if [ -z ${testPath+x} ]; then
    echo "Please specify test data path with testPath=<path> before the command"
    exit 1
fi
if [ -z ${brownPath+x} ]; then
    brownPathOpt=""
else
    brownPathOpt="-brownPath ${brownPath}"
fi
if [ -z ${l2+x} ]; then
    echo "No L2 parameter specified, setting to default 1.0"
    l2="1.0"
fi
if [ -z ${algo+x} ]; then
    echo "Please specify the algorithm: COLLAPSED_SHARED or HEAD_SHARED"
    exit 1
fi
if [ -z ${mp+x} ]; then
    if [ -z ${find_mp+x} ]; then
        find_mp=true
    fi
else
    find_mp=false
fi
if [ -z ${features+x} ]; then
    features="words,word_ngram,pos_tag,pos_tag_ngram,bow,orthographic,transition,mention_penalty"
fi
if [ -z ${hasPOS+x} ]; then
    hasPOS="true"
fi
if [ -z ${halfWindowSize+x} ]; then
    halfWindowSize="2"
fi
if [ -z ${ngramSize+x} ]; then
    ngramSize="4"
fi
if [ -z ${halfBowSize+x} ]; then
    halfBowSize="5"
fi
trainName="${trainPath##*/}"
testName="${testPath##*/}"
if [ "${brownPath}" == "" ]; then
    cluster=""
else
    cluster="${brownPath#*-}"
    cluster="-cluster-${cluster%%-*}"
fi
if [ -z ${maxIter+x} ]; then
    maxIter="2000"
fi
if [ -z ${useBILOU+x} ]; then
    useBILOU="false"
fi
if [ "${useBILOU}" == "true" ]; then
    useBILOUOpt="-useBILOU"
else
    useBILOUOpt=""
fi
if [ -z ${useSSVM+x} ]; then
    useSSVM="false"
fi
if [ "${useSSVM}" == "true" ]; then
    useSSVMOpt="-useSSVM"
else
    useSSVMOpt=""
fi
if [ -z ${ignoreOverlaps+x} ]; then
    ignoreOverlaps="false"
fi
if [ "${ignoreOverlaps}" == "true" ]; then
    ignoreOverlapOpt="-ignoreOverlaps"
else
    ignoreOverlapOpt=""
fi
runName="${algo}-${trainName%%.*}-l2-${l2}${cluster}-sizes_${halfWindowSize}${ngramSize}${halfBowSize}${useSSVMOpt}"
logName="${runName}"
if [ $# -lt 1 ]; then
    if [ "${l2}" == "0.0" ]; then
        writeModelTextOpt="-writeModelText"
    else
        writeModelTextOpt=""
    fi
    time java -Xmx200g -Xms200g -jar experiment-overlapping-1.0-SNAPSHOT.jar \
        -algo ${algo} \
        ${brownPathOpt} \
        -logPath "${expDir}/${logName}.log" \
        -modelPath "${expDir}/${runName}.model" \
        -trainPath "${trainPath}" "${trainPath}" \
        -testPath "${testPath}" "${testPath}" \
        -resultDir "${expDir}/${logName}.result" \
        -labelInterpretation nested_only \
        -networkInterpretation generate_enough \
        -numExamplesPrinted 20 \
        -l2 ${l2} \
        -tokenizer regex \
        -maxBodyCount 1 \
        -instanceFilter all_instances \
        -weightInit 0.0 \
        -maxIter ${maxIter} \
        -printOnlyMistakes \
        -parallelTouch \
        -nThreads 40 \
        -features "${features}" \
        -dataIsRaw false \
        -hasPOS "${hasPOS}" \
        -useSpecificIndicator "${useSpecificIndicator}" \
        ${writeModelTextOpt} \
        ${useBILOUOpt} \
        ${useSSVMOpt} \
        ${ignoreOverlapOpt} \
        -- \
        -word_half_window_size ${halfWindowSize} \
        -pos_half_window_size ${halfWindowSize} \
        -word_ngram_size ${ngramSize} \
        -pos_ngram_size ${ngramSize} \
        -bow_half_window_size ${halfBowSize} \
        -wordtype_half_window_size ${halfWindowSize} \
        -wordtype_ngram_size ${ngramSize} \
        2>&1 | tee "${expDir}/${logName}.runlog"
else
    logName="${logName}-run_on-${testName}"
    time java -Xmx200g -Xms200g -jar experiment-overlapping-1.0-SNAPSHOT.jar \
        -algo ${algo} \
        ${brownPathOpt} \
        -logPath "${expDir}/${logName}.log" \
        -modelPath "${expDir}/${runName}.model" \
        ${devOpt} \
        -testPath "${testPath}" "${testPath}" \
        -resultDir "${expDir}/${logName}.result" \
        -labelInterpretation nested_only \
        -networkInterpretation generate_enough \
        -numExamplesPrinted 20 \
        -l2 ${l2} \
        -tokenizer regex \
        -maxBodyCount 1 \
        -instanceFilter all_instances \
        -weightInit random \
        -printOnlyMistakes \
        -parallelTouch \
        -nThreads 40 \
        -dataIsRaw false \
        -hasPOS "${hasPOS}" \
        -useSpecificIndicator "${useSpecificIndicator}" \
        -features "${features}" \
        ${useBILOUOpt} \
        ${useSSVMOpt} \
        ${ignoreOverlapOpt} \
        2>&1 | tee "${expDir}/${logName}.runlog"
    if [ ${find_mp} == true ]; then
        logName="${logName}-mp_opt"
        time java -Xmx200g -Xms200g -jar experiment-overlapping-1.0-SNAPSHOT.jar \
            -algo ${algo} \
            ${brownPathOpt} \
            -logPath "${expDir}/${logName}.log" \
            -modelPath "${expDir}/${runName}.model" \
            ${devOpt} \
            -testPath "${testPath}" "${testPath}" \
            -resultDir "${expDir}/${logName}.result" \
            -labelInterpretation nested_only \
            -networkInterpretation generate_enough \
            -numExamplesPrinted 20 \
            -l2 ${l2} \
            -tokenizer regex \
            -maxBodyCount 1 \
            -instanceFilter all_instances \
            -weightInit random \
            -printOnlyMistakes \
            -parallelTouch \
            -nThreads 40 \
            -dataIsRaw false \
            -findOptimalMentionPenalty \
            -gridSearchOpts 300 -200 0.1 \
            -hasPOS "${hasPOS}" \
            -useSpecificIndicator "${useSpecificIndicator}" \
            -features "${features}" \
            ${useBILOUOpt} \
            ${useSSVMOpt} \
            ${ignoreOverlapOpt} \
            2>&1 | tee "${expDir}/${logName}.runlog"
    elif [ -n "${mp}" ]; then
        logName="${logName}-mp-${mp}"
        time java -Xmx200g -Xms200g -jar experiment-overlapping-1.0-SNAPSHOT.jar \
            -algo ${algo} \
            ${brownPathOpt} \
            -logPath "${expDir}/${logName}.log" \
            -modelPath "${expDir}/${runName}.model" \
            ${devOpt} \
            -testPath "${testPath}" "${testPath}" \
            -resultDir "${expDir}/${logName}.result" \
            -labelInterpretation nested_only \
            -networkInterpretation generate_enough \
            -numExamplesPrinted 20 \
            -l2 ${l2} \
            -tokenizer regex \
            -maxBodyCount 1 \
            -instanceFilter all_instances \
            -weightInit random \
            -printOnlyMistakes \
            -parallelTouch \
            -nThreads 40 \
            -dataIsRaw false \
            -mentionPenalty ${mp} \
            -hasPOS "${hasPOS}" \
            -useSpecificIndicator "${useSpecificIndicator}" \
            -features "${features}" \
            ${useBILOUOpt} \
            ${useSSVMOpt} \
            ${ignoreOverlapOpt} \
            2>&1 | tee "${expDir}/${logName}.runlog"
    fi
fi
