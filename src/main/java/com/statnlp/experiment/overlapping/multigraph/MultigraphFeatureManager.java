package com.statnlp.experiment.overlapping.multigraph;

import static com.statnlp.experiment.overlapping.OEUtil.setupFeatures;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import com.statnlp.experiment.overlapping.EntitySpan;
import com.statnlp.experiment.overlapping.IFeatureType;
import com.statnlp.experiment.overlapping.OEInstance;
import com.statnlp.experiment.overlapping.OELemmatizer;
import com.statnlp.experiment.overlapping.OENetwork;
import com.statnlp.experiment.overlapping.OELemmatizer.LemmatizerMethod;
import com.statnlp.experiment.overlapping.OEPOSTagger.POSTaggerMethod;
import com.statnlp.experiment.overlapping.WordAttributes.AllAlphanumeric;
import com.statnlp.experiment.overlapping.WordAttributes.AllCaps;
import com.statnlp.experiment.overlapping.WordAttributes.AllDigits;
import com.statnlp.experiment.overlapping.WordAttributes.AllLowercase;
import com.statnlp.experiment.overlapping.WordAttributes.ContainsDigits;
import com.statnlp.experiment.overlapping.WordAttributes.ContainsDots;
import com.statnlp.experiment.overlapping.WordAttributes.ContainsHyphen;
import com.statnlp.experiment.overlapping.WordAttributes.InitialCaps;
import com.statnlp.experiment.overlapping.WordAttributes.LonelyInitial;
import com.statnlp.experiment.overlapping.WordAttributes.PunctuationMark;
import com.statnlp.experiment.overlapping.WordAttributes.RomanNumber;
import com.statnlp.experiment.overlapping.WordAttributes.SingleCharacter;
import com.statnlp.experiment.overlapping.WordAttributes.URL;
import com.statnlp.experiment.overlapping.multigraph.MultigraphNetworkCompiler.NodeType;
import com.statnlp.experiment.overlapping.multigraph.MultigraphNetworkCompiler.OrigNodeType;
import com.statnlp.hybridnetworks.FeatureArray;
import com.statnlp.hybridnetworks.FeatureManager;
import com.statnlp.hybridnetworks.GlobalNetworkParam;
import com.statnlp.hybridnetworks.Network;

import edu.stanford.nlp.ling.CoreLabel;
import edu.stanford.nlp.process.WordShapeClassifier;
import edu.stanford.nlp.util.StringUtils;
import edu.stanford.nlp.util.TypesafeMap;
import edu.stanford.nlp.util.TypesafeMap.Key;

public class MultigraphFeatureManager extends FeatureManager {

	private static final long serialVersionUID = 1359679442997276807L;
	
	public int wordHalfWindowSize;
	public int wordTypeHalfWindowSize;
	public int bowHalfWindowSize;
	public int posHalfWindowSize;
	public int wordNGramSize;
	public int wordTypeNGramSize;
	public int posNGramSize;
	public int prefixLength;
	public int suffixLength;
	public boolean wordOnlyLeftWindow;
	public boolean bowOnlyLeftWindow;
	public boolean posOnlyLeftWindow;
	public boolean useSpecificIndicator;
	
	public boolean isClinicalTask;
	
	public POSTaggerMethod posTaggerMethod;
	public LemmatizerMethod lemmatizerMethod;
	public Map<String, String> brownMap;
	public Map<String, String> umlsSemanticCategory;
	
	public HashSet<String> _func_words;
	
	public String getNEWordType(CoreLabel word){
		if(word.value().equals("\"") || word.value().equals("\'")){
			return "TYPE_Quote";
		}
		if(_func_words.contains(word.value())){
			return "TYPE_Function";
		}
		if(word.getString(PunctuationMark.class).equals("true")){
			return "TYPE_Punctuation";
		}
		if(word.getString(InitialCaps.class).equals("true")){
			return "TYPE_Capitalized";
		}
		if(word.getString(AllLowercase.class).equals("true")){
			return "TYPE_LowerCase";
		}
		return "TYPE_Other";
	}
	
	public static class Attribute implements TypesafeMap.Key<Map<String, String>>{}
	
	public enum FeatureType implements IFeatureType{
		CHEAT(false),
		
		WORDS(true),
		LEXICALIZED_WORDS,
		WORD_NGRAM,
		
		POS_TAG(true),
		POS_TAG_NGRAM,
		
		COMBINED_BOW(true),
		BOW(true),
		
		ORTHOGRAPHIC(true),
//		ALL_CAPS,
//		ALL_DIGITS,
//		ALL_ALPHANUMERIC,
//		CONTAINS_DIGITS,
//		CONTAINS_DOTS,
//		CONTAINS_HYPHEN,
//		INITIAL_CAPS,
//		LONELY_INITIAL,
//		PUNCTUATION_MARK,
//		ROMAN_NUMBER,
//		SINGLE_CHARACTER,
//		URL,
		PREFIX(true),
		SUFFIX(true),
		
		WORDSHAPE,
		WORDSHAPE_WINDOW,
		
		WORDTYPE(true),
		WORDTYPE_NGRAM(true),
		
		MENTION_PENALTY(true),
		CONTIGUOUS_MENTION_PENALTY(true),
		DISCONTIGUOUS_MENTION_PENALTY(true),
		
		NOTE_TYPE(true),
		SECTION_NAME(true),
		BROWN_CLUSTER(true),
		SEMANTIC_CATEGORY(true),
		
		TRANSITION(true),
		
		BOUNDARY(true),
		;
		
		private boolean isEnabled = false;
		
		private FeatureType(){
			this(false);
		}
		
		private FeatureType(boolean enabled){
			this.isEnabled = enabled;
		}

		@Override
		public void enable() {
			this.isEnabled = true;
		}

		@Override
		public void disable() {
			this.isEnabled = false;
		}

		@Override
		public boolean enabled() {
			return this.isEnabled;
		}
	}
	
	public static enum Argument{
		WORD_HALF_WINDOW_SIZE(1,
				"The half window size for unigram word features",
				"word_half_window_size",
				"n"),
		WORD_NGRAM_SIZE(1,
				"The maximum n-gram size for word features",
				"word_ngram_size",
				"n"),
		WORDTYPE_HALF_WINDOW_SIZE(1,
				"The half window size for unigram wordtype features",
				"wordtype_half_window_size",
				"n"),
		WORDTYPE_NGRAM_SIZE(1,
				"The maximum n-gram size for wordtype features",
				"wordtype_ngram_size",
				"n"),
		BOW_HALF_WINDOW_SIZE(1,
				"The half window size for bag-of-words word features",
				"bow_half_window_size",
				"n"),
		POS_HALF_WINDOW_SIZE(1,
				"The half window size for POS tag features",
				"pos_half_window_size",
				"n"),
		POS_NGRAM_SIZE(1,
				"The maximum n-gram size for POS tag features",
				"pos_ngram_size",
				"n"),
		WORD_ONLY_LEFT_WINDOW(0,
				"Whether to use only the left window for word features",
				"word_only_left_window"),
		BOW_ONLY_LEFT_WINDOW(0,
				"Whether to use only the left window for bag-of-words features",
				"bow_only_left_window"),
		POS_ONLY_LEFT_WINDOW(0,
				"Whether to use only the left window for POS tag features",
				"pos_only_left_window"),
		PREFIX_LENGTH(1,
				"The maximum prefix lengths for word prefix features",
				"prefix_length",
				"n"),
		SUFFIX_LENGTH(1,
				"The maximum suffix lengths for word suffix features",
				"suffix_length",
				"n"),
		HELP(0,
				"Print this help message",
				"h,help"),
		;
		
		final public int numArgs;
		final private String[] argNames;
		final private String[] names;
		final private String help;
		private Argument(int numArgs, String help, String names, String... argNames){
			this.numArgs = numArgs;
			this.argNames = argNames;
			this.names = names.split(",");
			this.help = help;
		}
		
		/**
		 * Return the Argument which has the specified name
		 * @param name
		 * @return
		 */
		public static Argument argWithName(String name){
			for(Argument argument: Argument.values()){
				for(String argName: argument.names){
					if(argName.equals(name)){
						return argument;
					}
				}
			}
			throw new IllegalArgumentException("Unrecognized argument: "+name);
		}
		
		/**
		 * Print help message
		 */
		public static void printHelp(){
			StringBuilder result = new StringBuilder();
			result.append("Options:\n");
			for(Argument argument: Argument.values()){
				result.append("-"+StringUtils.join(argument.names, " -"));
				result.append(" "+StringUtils.join(argument.argNames, " "));
				result.append("\n");
				if(argument.help != null && argument.help.length() > 0){
					result.append("\t"+argument.help.replaceAll("\n","\n\t")+"\n");
				}
			}
			System.out.println(result.toString());
		}
	}

	public MultigraphFeatureManager(GlobalNetworkParam param_g, POSTaggerMethod posTaggerMethod, LemmatizerMethod lemmatizerMethod, Map<String, String> brownMap, Map<String, String> umlsSemanticCategory, String[] features, OEInstance[] trainInstances, boolean useSpecificIndicator, String... args) {
		super(param_g);
		this.isClinicalTask = false;
		this.posTaggerMethod = posTaggerMethod;
		this.lemmatizerMethod = lemmatizerMethod;
		this.brownMap = brownMap;
		this.umlsSemanticCategory = umlsSemanticCategory;
		wordHalfWindowSize = 1;
		wordTypeHalfWindowSize = 1;
		bowHalfWindowSize = 1;
		posHalfWindowSize = 1;
		wordNGramSize = 2;
		wordTypeNGramSize = 2;
		posNGramSize = 2;
		prefixLength = 3;
		suffixLength = 3;
		wordOnlyLeftWindow = false;
		bowOnlyLeftWindow = false;
		posOnlyLeftWindow = false;
		this.useSpecificIndicator = useSpecificIndicator;
		setupFeatures(FeatureType.class, features);
		int argIndex = 0;
		while(argIndex < args.length){
			String arg = args[argIndex];
			if(arg.length() > 0 && arg.charAt(0) == '-'){
				Argument argument = Argument.argWithName(args[argIndex].substring(1));
				switch(argument){
				case WORD_HALF_WINDOW_SIZE:
					wordHalfWindowSize = Integer.parseInt(args[argIndex+1]);
					break;
				case WORDTYPE_HALF_WINDOW_SIZE:
					wordTypeHalfWindowSize = Integer.parseInt(args[argIndex+1]);
					break;
				case WORD_NGRAM_SIZE:
					wordNGramSize = Integer.parseInt(args[argIndex+1]);
					break;
				case WORDTYPE_NGRAM_SIZE:
					wordTypeNGramSize = Integer.parseInt(args[argIndex+1]);
					break;
				case BOW_HALF_WINDOW_SIZE:
					bowHalfWindowSize = Integer.parseInt(args[argIndex+1]);
					break;
				case POS_HALF_WINDOW_SIZE:
					posHalfWindowSize = Integer.parseInt(args[argIndex+1]);
					break;
				case POS_NGRAM_SIZE:
					posNGramSize = Integer.parseInt(args[argIndex+1]);
					break;
				case WORD_ONLY_LEFT_WINDOW:
					wordOnlyLeftWindow = true;
					break;
				case BOW_ONLY_LEFT_WINDOW:
					bowOnlyLeftWindow = true;
					break;
				case POS_ONLY_LEFT_WINDOW:
					posOnlyLeftWindow = true;
					break;
				case PREFIX_LENGTH:
					prefixLength = Integer.parseInt(args[argIndex+1]);
					break;
				case SUFFIX_LENGTH:
					suffixLength = Integer.parseInt(args[argIndex+1]);
					break;
				case HELP:
					Argument.printHelp();
					System.exit(0);
				}
				argIndex += argument.numArgs+1;
			} else {
				throw new IllegalArgumentException("Error while parsing: "+arg);
			}
		}
		findFunctionWords(trainInstances);
	}
	
	private void findFunctionWords(OEInstance[] trainInstances){
		HashMap<String, Integer> wordCount = new HashMap<String, Integer>();
		for(OEInstance instance: trainInstances){
			for(EntitySpan entitySpan: instance.output){
				for(CoreLabel word: instance.inputTokenized){
					if(word.beginPosition() < entitySpan.start() || word.endPosition() > entitySpan.end()){
						continue;
					}
					if(!word.getString(AllAlphanumeric.class).equals("true")){
						continue;
					}
					if(!word.getString(AllLowercase.class).equals("true")){
						continue;
					}
					if(word.getString(ContainsDigits.class).equals("true")){
						continue;
					}
					wordCount.put(word.value(), wordCount.getOrDefault(word.value(), 0)+1);
				}
			}
		}
		_func_words = new HashSet<String>();
		for(String word: wordCount.keySet()){
			if(wordCount.get(word) >= 3){
				_func_words.add(word);
			}
		}
	}

	@Override
	protected FeatureArray extract_helper(Network net, int parent_k, int[] children_k) {
		OENetwork network = (OENetwork)net;
		OEInstance instance = (OEInstance)network.getInstance();
		MultigraphNetworkCompiler compiler = (MultigraphNetworkCompiler)network.getCompiler();

		CoreLabel[] words = instance.getInputTokenized().toArray(new CoreLabel[0]);
		if(!instance.posTagged){
			instance.posTag(posTaggerMethod, false);
		}
		int size = words.length;
		
		int[] parentArr = network.getNodeArray(parent_k);
//		System.err.println(Arrays.toString(parentArr));
//		for(int child_k: children_k){
//			System.err.println("\t"+Arrays.toString(network.getNodeArray(child_k)));
//		}
		NodeType parentType = NodeType.values()[parentArr[1]];
		int parentPos = size-parentArr[0]-1;
		int parentLabelId = parentArr[2];
		if(parentType == NodeType.ROOT_NODE){
			int[] parentArrOrig = compiler.toNodeArr_E(parentPos, size);
			int[][] childrenNodeArrOrig = new int[children_k.length][];
			for(int i=0; i<children_k.length; i++){
				int[] childNodeArr = network.getNodeArray(children_k[i]);
				childrenNodeArrOrig[i] = compiler.toNodeArr_T(parentPos, size, childNodeArr[2]);
			}
//			System.err.println("E-T");
//			System.err.println(Arrays.toString(parentArrOrig));
//			for(int[] child: childrenNodeArrOrig){
//				System.err.println("\t"+Arrays.toString(child));
//			}
			return extractHelperOrig(network, instance, words, size, parentArrOrig, childrenNodeArrOrig);
		} else if(parentType == NodeType.TYPE_NODE){
			int[] parentArrOrig = compiler.toNodeArr_T(parentPos, size, parentLabelId);
			int[][] childrenNodeArrOrig;
			int[] childNodeArr = network.getNodeArray(children_k[0]);
			int childState = childNodeArr[3];
			if(childState >> 2*compiler.maxBodyCount-2 == 1){
				childrenNodeArrOrig = new int[][]{compiler.toNodeArr_B(parentPos, size, 0, parentLabelId)};
			} else {
				childrenNodeArrOrig = new int[][]{compiler.toNodeArr_X()};
			}
//			System.err.println("First T-X or T-B");
//			System.err.println(Arrays.toString(parentArrOrig));
//			for(int[] child: childrenNodeArrOrig){
//				System.err.println("\t"+Arrays.toString(child));
//			}
			return extractHelperOrig(network, instance, words, size, parentArrOrig, childrenNodeArrOrig);
		} else if(parentType == NodeType.STATE_NODE){
			int parentState = parentArr[3];
			int[] childNodeArr = network.getNodeArray(children_k[0]);
//			int childState = childNodeArr[3];
			int[] parentStateArr = compiler.toBinaryArr(parentState, 2*compiler.maxBodyCount-1);
//			int[] childStateArr = compiler.toBinaryArr(childState, 2*compiler.maxBodyCount-1);
			NodeType childType = NodeType.values()[childNodeArr[1]];
			if(childType == NodeType.LEAF_NODE){
				FeatureArray result = FeatureArray.EMPTY;
				for(int curStateBitPos=0; curStateBitPos<parentStateArr.length; curStateBitPos+=2){
					if(parentStateArr[curStateBitPos] == 1){
						int[] parentArrOrig = compiler.toNodeArr_B(parentPos, size, curStateBitPos/2, parentLabelId);
						int[][] childrenNodeArrOrig = new int[][]{compiler.toNodeArr_X()};
//						System.err.println("B-X");
//						System.err.println(Arrays.toString(parentArrOrig));
//						for(int[] child: childrenNodeArrOrig){
//							System.err.println("\t"+Arrays.toString(child));
//						}
						result = new FeatureArray(extractHelperOrig(network, instance, words, size, parentArrOrig, childrenNodeArrOrig).getCurrent(), result);
					}
				}
				return result;
			}

			int[] combinationArr = new int[2*compiler.maxBodyCount];
			combinationArr[0] = 1;
			for(int i=1; i<combinationArr.length; i++){
				if(i%2 == 1 && i != combinationArr.length-1){
					combinationArr[i] = combinationArr[i-1] + 3;
				} else {
					combinationArr[i] = combinationArr[i-1] + 2;
				}
			}
			FeatureArray result = FeatureArray.EMPTY;
			List<Integer> activeEdges = new ArrayList<Integer>();
			int combinationArrIdx = 1;
			boolean hasTtoB = false;
			int[] countNodeArr = compiler.toBinaryArr(-children_k[1]-1, 5*compiler.maxBodyCount-2);
			for(int i=0; i<countNodeArr.length; i++){
//				int countNodeValue = network.getNodeArray(children_k[i])[3];
				if(countNodeArr[i] == 0){
					continue;
				}
				int countNodeValue = i;
				if(countNodeValue == 0){
					int[] parentArrOrig = compiler.toNodeArr_T(parentPos+1, size, parentLabelId);
					int[][] childrenNodeArrOrig = new int[][]{compiler.toNodeArr_B(parentPos+1, size, 0, parentLabelId)};
//					System.err.println("T-B");
//					System.err.println(Arrays.toString(parentArrOrig));
//					for(int[] child: childrenNodeArrOrig){
//						System.err.println("\t"+Arrays.toString(child));
//					}
					result = new FeatureArray(extractHelperOrig(network, instance, words, size, parentArrOrig, childrenNodeArrOrig).getCurrent(), result);
					hasTtoB = true;
				} else {
					if(combinationArr[combinationArrIdx] <= countNodeValue && activeEdges.size() > 0){
						// Add features from this edge
						int curStateBitPos = combinationArrIdx-1;
						int[] parentArrOrig;
						if(curStateBitPos % 2 == 0){
							parentArrOrig = compiler.toNodeArr_B(parentPos, size, curStateBitPos/2, parentLabelId);
						} else {
							parentArrOrig = compiler.toNodeArr_O(parentPos, size, (curStateBitPos+1)/2, parentLabelId);
						}
						int[][] childrenNodeArrOrig = new int[activeEdges.size()][];
						for(int edgeIdx=0; edgeIdx<activeEdges.size(); edgeIdx++){
							int edge = activeEdges.get(edgeIdx);
							int diff = edge - combinationArr[combinationArrIdx-1];
							if(combinationArrIdx == combinationArr.length-1){
								diff += 1;
							}
							if(diff == 2){
								childrenNodeArrOrig[activeEdges.size()-1-edgeIdx] = compiler.toNodeArr_X();
							} else {
								int nextStateBitPos = curStateBitPos - diff + 1;
								if(nextStateBitPos % 2 == 0){
									childrenNodeArrOrig[curStateBitPos % 2 == 0 ? activeEdges.size()-1-edgeIdx : edgeIdx] = compiler.toNodeArr_B(parentPos+1, size, nextStateBitPos/2, parentLabelId);
								} else {
									childrenNodeArrOrig[curStateBitPos % 2 == 0 ? activeEdges.size()-1-edgeIdx : edgeIdx] = compiler.toNodeArr_O(parentPos+1, size, (nextStateBitPos+1)/2, parentLabelId);
								}
							}
						}
//						System.err.println("B-B or B-O or O-B or O-O");
//						System.err.println(Arrays.toString(parentArrOrig));
//						for(int[] child: childrenNodeArrOrig){
//							System.err.println("\t"+Arrays.toString(child));
//						}
						result = new FeatureArray(extractHelperOrig(network, instance, words, size, parentArrOrig, childrenNodeArrOrig).getCurrent(), result);
						activeEdges.clear();
					}
					while(combinationArr[combinationArrIdx] <= countNodeValue){
						combinationArrIdx += 1;
					}
					activeEdges.add(countNodeValue);
				}
			}
			if(!hasTtoB){
				int[] parentArrOrig = compiler.toNodeArr_T(parentPos+1, size, parentLabelId);
				int[][] childrenNodeArrOrig = new int[][]{compiler.toNodeArr_X()};
//				System.err.println("T-X");
//				System.err.println(Arrays.toString(parentArrOrig));
//				for(int[] child: childrenNodeArrOrig){
//					System.err.println("\t"+Arrays.toString(child));
//				}
				result = new FeatureArray(extractHelperOrig(network, instance, words, size, parentArrOrig, childrenNodeArrOrig).getCurrent(), result);
			}
			if(activeEdges.size() > 0){
				// Add features from this edge
				int curStateBitPos = combinationArrIdx-1;
				int[] parentArrOrig;
				if(curStateBitPos % 2 == 0){
					parentArrOrig = compiler.toNodeArr_B(parentPos, size, curStateBitPos/2, parentLabelId);
				} else {
					parentArrOrig = compiler.toNodeArr_O(parentPos, size, (curStateBitPos+1)/2, parentLabelId);
				}
				int[][] childrenNodeArrOrig = new int[activeEdges.size()][];
				for(int edgeIdx=0; edgeIdx<activeEdges.size(); edgeIdx++){
					int edge = activeEdges.get(edgeIdx);
					int diff = edge - combinationArr[combinationArrIdx-1];
					if(combinationArrIdx == combinationArr.length-1){
						diff += 1;
					}
					if(diff == 2){
						childrenNodeArrOrig[activeEdges.size()-1-edgeIdx] = compiler.toNodeArr_X();
					} else {
						int nextStateBitPos = curStateBitPos - diff + 1;
						if(nextStateBitPos % 2 == 0){
							childrenNodeArrOrig[curStateBitPos % 2 == 0 ? activeEdges.size()-1-edgeIdx : edgeIdx] = compiler.toNodeArr_B(parentPos+1, size, nextStateBitPos/2, parentLabelId);
						} else {
							childrenNodeArrOrig[curStateBitPos % 2 == 0 ? activeEdges.size()-1-edgeIdx : edgeIdx] = compiler.toNodeArr_O(parentPos+1, size, (nextStateBitPos+1)/2, parentLabelId);
						}
					}
				}
//				System.err.println("B-B or B-O or O-B or O-O");
//				System.err.println(Arrays.toString(parentArrOrig));
//				for(int[] child: childrenNodeArrOrig){
//					System.err.println("\t"+Arrays.toString(child));
//				}
				result = new FeatureArray(extractHelperOrig(network, instance, words, size, parentArrOrig, childrenNodeArrOrig).getCurrent(), result);
			}
			activeEdges.clear();
			return result;
		} else {
			return FeatureArray.EMPTY;
		}
	}
	
	private FeatureArray extractHelperOrig(OENetwork network, OEInstance instance, CoreLabel[] words, int size, int[] parent_arr, int[][] childrenNodeArr){
		if(isClinicalTask){
			return extractHelperClinical(network, instance, words, size, parent_arr, childrenNodeArr);
		} else {
			return extractHelperACE(network, instance, words, size, parent_arr, childrenNodeArr);
		}
	}
	
	private FeatureArray extractHelperClinical(OENetwork network, OEInstance instance, CoreLabel[] words, int size, int[] parent_arr, int[][] childrenNodeArr){
		int pos = size-parent_arr[0]-1;
		int bodyIdx = parent_arr[1];
		OrigNodeType nodeType = OrigNodeType.values()[parent_arr[2]];
		String labelId = String.valueOf(parent_arr[3]);
		
		if(nodeType == OrigNodeType.X_NODE || nodeType == OrigNodeType.A_NODE || nodeType == OrigNodeType.E_NODE){
			return FeatureArray.EMPTY;
		}

		CoreLabel curWord = words[pos];
		String curWordString = curWord.value();
		String postag = curWord.tag();
		List<Integer> features = new ArrayList<Integer>();
		
		GlobalNetworkParam param_g = this._param_g;

		String childListStr = "";
		for(int[] childNodeArr: childrenNodeArr){
			childListStr += "-"+OrigNodeType.values()[childNodeArr[2]]+childNodeArr[1];
		}
		
		if(FeatureType.CHEAT.enabled()){
			int cheatFeature = param_g.toFeature(network, FeatureType.CHEAT.name(), labelId, Math.abs(instance.getInstanceId())+" "+pos+" "+nodeType+bodyIdx+" "+childListStr);
			return new FeatureArray(new int[]{cheatFeature});
		}
		
		if(FeatureType.TRANSITION.enabled()){
			features.add(param_g.toFeature(network, FeatureType.TRANSITION.name(), labelId, nodeType.name()+"-"+childListStr));
		}
		
		boolean createMoreSpecificFeatures = (childrenNodeArr.length > 1);
		for(int[] childNodeArr: childrenNodeArr){
			int childBodyIdx = childNodeArr[1];
			OrigNodeType childNodeType = OrigNodeType.values()[childNodeArr[2]];
			
			boolean isLeftBoundary = false;
			boolean isRightBoundary = false;
			if(nodeType == OrigNodeType.T_NODE && childNodeType == OrigNodeType.B_NODE){
				if(FeatureType.MENTION_PENALTY.enabled()){
					features.add(param_g.toFeature(network, FeatureType.MENTION_PENALTY.name(), "", ""));
				}
				if(FeatureType.CONTIGUOUS_MENTION_PENALTY.enabled()){
					features.add(param_g.toFeature(network, FeatureType.CONTIGUOUS_MENTION_PENALTY.name(), "", ""));
				}
			}
			if(nodeType == OrigNodeType.T_NODE && childNodeType == OrigNodeType.B_NODE){
				isLeftBoundary = true;
			}
			if(nodeType == OrigNodeType.B_NODE && childNodeType == OrigNodeType.X_NODE){
				isRightBoundary = true;
			}
			if(nodeType == OrigNodeType.B_NODE && childNodeType == OrigNodeType.O_NODE){
				if(FeatureType.DISCONTIGUOUS_MENTION_PENALTY.enabled() && bodyIdx == 0){
					features.add(param_g.toFeature(network, FeatureType.DISCONTIGUOUS_MENTION_PENALTY.name(), "", ""));
				}
			}
			
			String generalIndicator = nodeType+"-"+childNodeType;
			String specificIndicator = nodeType.name()+bodyIdx+"-"+childNodeType.name()+childBodyIdx;
			
			if(FeatureType.WORDS.enabled() || FeatureType.LEXICALIZED_WORDS.enabled()){
				int wordWindowSize = wordHalfWindowSize*2+1;
				if(wordWindowSize < 0){
					wordWindowSize = 0;
				}
				for(int i=0; i<wordWindowSize; i++){
					String word = "***";
					int relIdx = (i-wordHalfWindowSize);
					int idx = pos + relIdx;
					if(idx >= 0 && idx < size){
						word = words[idx].value();
					}
					if(wordOnlyLeftWindow && idx > pos) continue;
					if(FeatureType.WORDS.enabled()){
						features.add(param_g.toFeature(network, generalIndicator+FeatureType.WORDS+":"+relIdx, labelId, word));
						features.add(param_g.toFeature(network, specificIndicator+FeatureType.WORDS+":"+relIdx, labelId, word));
						if(createMoreSpecificFeatures){
							features.add(param_g.toFeature(network, childListStr+FeatureType.WORDS+":"+relIdx, labelId, word));
						}
						if(FeatureType.BOUNDARY.enabled()){
							if(isLeftBoundary){
								features.add(param_g.toFeature(network, specificIndicator+FeatureType.WORDS+":"+relIdx+"-"+FeatureType.BOUNDARY+"-LEFT", labelId, word));
							}
							if(isRightBoundary){
								features.add(param_g.toFeature(network, specificIndicator+FeatureType.WORDS+":"+relIdx+"-"+FeatureType.BOUNDARY+"-RIGHT", labelId, word));
							}
						}
					}
					if(FeatureType.LEXICALIZED_WORDS.enabled()){
						features.add(param_g.toFeature(network, generalIndicator+FeatureType.LEXICALIZED_WORDS+":"+relIdx, labelId, curWord.value()+" "+word));
						features.add(param_g.toFeature(network, specificIndicator+FeatureType.LEXICALIZED_WORDS+":"+relIdx, labelId, curWord.value()+" "+word));
						if(createMoreSpecificFeatures){
							features.add(param_g.toFeature(network, childListStr+FeatureType.LEXICALIZED_WORDS+":"+relIdx, labelId, curWord.value()+" "+word));
						}
						if(FeatureType.BOUNDARY.enabled()){
							if(isLeftBoundary){
								features.add(param_g.toFeature(network, specificIndicator+FeatureType.LEXICALIZED_WORDS+":"+relIdx+"-"+FeatureType.BOUNDARY+"-LEFT", labelId, curWord.value()+" "+word));
							}
							if(isRightBoundary){
								features.add(param_g.toFeature(network, specificIndicator+FeatureType.LEXICALIZED_WORDS+":"+relIdx+"-"+FeatureType.BOUNDARY+"-RIGHT", labelId, curWord.value()+" "+word));
							}
						}
					}
				}
			}
			
			if(FeatureType.POS_TAG.enabled()){
				int posWindowSize = posHalfWindowSize*2+1;
				if(posWindowSize < 0){
					posWindowSize = 0;
				}
				for(int i=0; i<posWindowSize; i++){
					String tag = "***";
					int relIdx = (i-posHalfWindowSize);
					int idx = pos + relIdx;
					if(idx >= 0 && idx < size){
						tag = words[idx].tag();
					}
					if(posOnlyLeftWindow && idx > pos) continue;
					features.add(param_g.toFeature(network, generalIndicator+FeatureType.POS_TAG+":"+relIdx, labelId, tag));
					features.add(param_g.toFeature(network, specificIndicator+FeatureType.POS_TAG+":"+relIdx, labelId, tag));
					if(createMoreSpecificFeatures){
						features.add(param_g.toFeature(network, childListStr+FeatureType.POS_TAG+":"+relIdx, labelId, tag));
					}
					if(FeatureType.BOUNDARY.enabled()){
						if(isLeftBoundary){
							features.add(param_g.toFeature(network, specificIndicator+FeatureType.POS_TAG+":"+relIdx+"-"+FeatureType.BOUNDARY+"-LEFT", labelId, tag));
						}
						if(isRightBoundary){
							features.add(param_g.toFeature(network, specificIndicator+FeatureType.POS_TAG+":"+relIdx+"-"+FeatureType.BOUNDARY+"-RIGHT", labelId, tag));
						}
					}
				}
			}
			
			if(FeatureType.WORD_NGRAM.enabled()){
				for(int ngramSize=2; ngramSize<=wordNGramSize; ngramSize++){
					for(int relPos=0; relPos<ngramSize; relPos++){
						String ngram = "";
						for(int idx=pos-ngramSize+relPos+1; idx<pos+relPos+1; idx++){
							if(ngram.length() > 0) ngram += " ";
							if(idx >= 0 && idx < size){
								ngram += words[idx];
							}
						}
						features.add(param_g.toFeature(network, generalIndicator+FeatureType.WORD_NGRAM+" "+ngramSize+" "+relPos, labelId, ngram));
						features.add(param_g.toFeature(network, specificIndicator+FeatureType.WORD_NGRAM+" "+ngramSize+" "+relPos, labelId, ngram));
						if(createMoreSpecificFeatures){
							features.add(param_g.toFeature(network, childListStr+FeatureType.WORD_NGRAM+" "+ngramSize+" "+relPos, labelId, ngram));
						}
						if(FeatureType.BOUNDARY.enabled()){
							if(isLeftBoundary){
								features.add(param_g.toFeature(network, specificIndicator+FeatureType.WORD_NGRAM+" "+ngramSize+" "+relPos+"-"+FeatureType.BOUNDARY+"-LEFT", labelId, ngram));
							}
							if(isRightBoundary){
								features.add(param_g.toFeature(network, specificIndicator+FeatureType.WORD_NGRAM+" "+ngramSize+" "+relPos+"-"+FeatureType.BOUNDARY+"-RIGHT", labelId, ngram));
							}
						}
					}
				}
			}
			
			if(FeatureType.POS_TAG_NGRAM.enabled()){
				for(int ngramSize=2; ngramSize<=posNGramSize; ngramSize++){
					for(int relPos=0; relPos<ngramSize; relPos++){
						String ngram = "";
						for(int idx=pos-ngramSize+relPos+1; idx<pos+relPos+1; idx++){
							if(idx > pos-ngramSize+relPos+1) ngram += " ";
							if(idx >= 0 && idx < size){
								ngram += words[idx].tag();
							}
						}
						features.add(param_g.toFeature(network, generalIndicator+FeatureType.POS_TAG_NGRAM+" "+ngramSize+" "+relPos, labelId, ngram));
						features.add(param_g.toFeature(network, specificIndicator+FeatureType.POS_TAG_NGRAM+" "+ngramSize+" "+relPos, labelId, ngram));
						if(createMoreSpecificFeatures){
							features.add(param_g.toFeature(network, childListStr+FeatureType.POS_TAG_NGRAM+" "+ngramSize+" "+relPos, labelId, ngram));
						}
						if(FeatureType.BOUNDARY.enabled()){
							if(isLeftBoundary){
								features.add(param_g.toFeature(network, specificIndicator+FeatureType.POS_TAG_NGRAM+" "+ngramSize+" "+relPos+"-"+FeatureType.BOUNDARY+"-LEFT", labelId, ngram));
							}
							if(isRightBoundary){
								features.add(param_g.toFeature(network, specificIndicator+FeatureType.POS_TAG_NGRAM+" "+ngramSize+" "+relPos+"-"+FeatureType.BOUNDARY+"-RIGHT", labelId, ngram));
							}
						}
					}
				}
			}
			
			if(FeatureType.COMBINED_BOW.enabled()){
				List<String> bowList = new ArrayList<String>();
				for(int idx=pos-bowHalfWindowSize; idx<=pos+bowHalfWindowSize; idx++){
					if(idx >= 0 && idx < size){
						bowList.add(words[idx].value());
					}
				}
				Collections.sort(bowList);
				String bow = "";
				for(String word: bowList){
					if(bow.length() > 0) bow += " ";
					bow += word;
				}
				features.add(param_g.toFeature(network, generalIndicator+FeatureType.COMBINED_BOW.name(), labelId, bow));
				features.add(param_g.toFeature(network, specificIndicator+FeatureType.COMBINED_BOW.name(), labelId, bow));
				if(createMoreSpecificFeatures){
					features.add(param_g.toFeature(network, childListStr+FeatureType.COMBINED_BOW.name(), labelId, bow));
				}
				if(FeatureType.BOUNDARY.enabled()){
					if(isLeftBoundary){
						features.add(param_g.toFeature(network, specificIndicator+FeatureType.COMBINED_BOW.name()+"-"+FeatureType.BOUNDARY+"-LEFT", labelId, bow));
					}
					if(isRightBoundary){
						features.add(param_g.toFeature(network, specificIndicator+FeatureType.COMBINED_BOW.name()+"-"+FeatureType.BOUNDARY+"-RIGHT", labelId, bow));
					}
				}
			}
			
			if(FeatureType.BOW.enabled()){
				int bowWindowSize = bowHalfWindowSize*2+1;
				if(bowWindowSize < 0){
					bowWindowSize = 0;
				}
				for(int i=0; i<bowWindowSize; i++){
					String word = "***";
					int relIdx = (i-bowHalfWindowSize);
					int idx = pos + relIdx;
					if(idx >= 0 && idx < size){
						word = words[idx].value();
					}
					if(bowOnlyLeftWindow && idx > pos) continue;
					String featureName = generalIndicator+FeatureType.BOW.name();
					if(idx < pos){
						featureName += "-before";
					} if(idx > pos){
						featureName += "-after";
					} else {
						continue;
					}
					features.add(param_g.toFeature(network, featureName, labelId, word));
					featureName = specificIndicator+FeatureType.BOW.name();
					if(idx < pos){
						featureName += "-before";
					} if(idx > pos){
						featureName += "-after";
					} else {
						continue;
					}
					features.add(param_g.toFeature(network, featureName, labelId, word));
					if(createMoreSpecificFeatures){
						featureName = childListStr+FeatureType.BOW.name();
						if(idx < pos){
							featureName += "-before";
						} if(idx > pos){
							featureName += "-after";
						} else {
							continue;
						}
						features.add(param_g.toFeature(network, featureName, labelId, word));
					}
					if(FeatureType.BOUNDARY.enabled()){
						if(isLeftBoundary){
							featureName = specificIndicator+FeatureType.BOW.name()+"-"+FeatureType.BOUNDARY+"-LEFT";
							if(idx < pos){
								featureName += "-before";
							} if(idx > pos){
								featureName += "-after";
							} else {
								continue;
							}
							features.add(param_g.toFeature(network, featureName, labelId, word));
						}
						if(isRightBoundary){
							featureName = specificIndicator+FeatureType.BOW.name()+"-"+FeatureType.BOUNDARY+"-RIGHT";
							if(idx < pos){
								featureName += "-before";
							} if(idx > pos){
								featureName += "-after";
							} else {
								continue;
							}
							features.add(param_g.toFeature(network, featureName, labelId, word));
						}
					}
				}
			}
			
			if(FeatureType.ORTHOGRAPHIC.enabled()){
//				for(FeatureType featureType: FeatureType.values()){
//					switch(featureType){
//					case ALL_CAPS:
//					case ALL_DIGITS:
//					case ALL_ALPHANUMERIC:
//					case CONTAINS_DIGITS:
//					case CONTAINS_HYPHEN:
//					case INITIAL_CAPS:
//					case LONELY_INITIAL:
//					case PUNCTUATION_MARK:
//					case ROMAN_NUMBER:
//					case SINGLE_CHARACTER:
//					case URL:
//						features.add(param_g.toFeature(generalIndicator+featureType.name(), labelId, curWord.get(Attribute.class).get(featureType.name())));
//					default:
//						break;
//					}
//				}
				// Prefix
				for(int i=0; i<3; i++){
					String prefix = curWordString.substring(0, Math.min(curWordString.length(), i+1));
					features.add(param_g.toFeature(network, generalIndicator+FeatureType.ORTHOGRAPHIC+"-PREFIX", labelId, prefix));
					features.add(param_g.toFeature(network, specificIndicator+FeatureType.ORTHOGRAPHIC+"-PREFIX", labelId, prefix));
					if(createMoreSpecificFeatures){
						features.add(param_g.toFeature(network, childListStr+FeatureType.ORTHOGRAPHIC+"-PREFIX", labelId, prefix));
					}
					if(FeatureType.BOUNDARY.enabled()){
						if(isLeftBoundary){
							features.add(param_g.toFeature(network, specificIndicator+FeatureType.ORTHOGRAPHIC+"-PREFIX"+"-"+FeatureType.BOUNDARY+"-LEFT", labelId, prefix));
						}
						if(isRightBoundary){
							features.add(param_g.toFeature(network, specificIndicator+FeatureType.ORTHOGRAPHIC+"-PREFIX"+"-"+FeatureType.BOUNDARY+"-RIGHT", labelId, prefix));
						}
					}
				}
			
				// Suffix
				for(int i=0; i<3; i++){
					String suffix = curWordString.substring(Math.max(0, curWordString.length()-i-1), curWordString.length());
					features.add(param_g.toFeature(network, generalIndicator+FeatureType.ORTHOGRAPHIC+"-SUFFIX", labelId, suffix));
					features.add(param_g.toFeature(network, specificIndicator+FeatureType.ORTHOGRAPHIC+"-SUFFIX", labelId, suffix));
					if(createMoreSpecificFeatures){
						features.add(param_g.toFeature(network, childListStr+FeatureType.ORTHOGRAPHIC+"-SUFFIX", labelId, suffix));
					}
					if(FeatureType.BOUNDARY.enabled()){
						if(isLeftBoundary){
							features.add(param_g.toFeature(network, specificIndicator+FeatureType.ORTHOGRAPHIC+"-SUFFIX"+"-"+FeatureType.BOUNDARY+"-LEFT", labelId, suffix));
						}
						if(isRightBoundary){
							features.add(param_g.toFeature(network, specificIndicator+FeatureType.ORTHOGRAPHIC+"-SUFFIX"+"-"+FeatureType.BOUNDARY+"-RIGHT", labelId, suffix));
						}
					}
				}
				
				// Capitalization
				boolean isCapitalized = StringUtils.isCapitalized(curWordString);
				features.add(param_g.toFeature(network, generalIndicator+FeatureType.ORTHOGRAPHIC+"-CAPITALIZATION", labelId, isCapitalized+""));
				features.add(param_g.toFeature(network, specificIndicator+FeatureType.ORTHOGRAPHIC+"-CAPITALIZATION", labelId, isCapitalized+""));
				if(createMoreSpecificFeatures){
					features.add(param_g.toFeature(network, childListStr+FeatureType.ORTHOGRAPHIC+"-CAPITALIZATION", labelId, isCapitalized+""));
				}
				if(FeatureType.BOUNDARY.enabled()){
					if(isLeftBoundary){
						features.add(param_g.toFeature(network, specificIndicator+FeatureType.ORTHOGRAPHIC+"-CAPITALIZATION"+"-"+FeatureType.BOUNDARY+"-LEFT", labelId, isCapitalized+""));
					}
					if(isRightBoundary){
						features.add(param_g.toFeature(network, specificIndicator+FeatureType.ORTHOGRAPHIC+"-CAPITALIZATION"+"-"+FeatureType.BOUNDARY+"-RIGHT", labelId, isCapitalized+""));
					}
				}
				
				// Lemma
				String lemma = OELemmatizer.lemmatize(curWordString, postag, lemmatizerMethod);
				features.add(param_g.toFeature(network, generalIndicator+FeatureType.ORTHOGRAPHIC+"-LEMMA", labelId, lemma));
				features.add(param_g.toFeature(network, specificIndicator+FeatureType.ORTHOGRAPHIC+"-LEMMA", labelId, lemma));
				if(createMoreSpecificFeatures){
					features.add(param_g.toFeature(network, childListStr+FeatureType.ORTHOGRAPHIC+"-LEMMA", labelId, lemma));
				}
				if(FeatureType.BOUNDARY.enabled()){
					if(isLeftBoundary){
						features.add(param_g.toFeature(network, specificIndicator+FeatureType.ORTHOGRAPHIC+"-LEMMA"+"-"+FeatureType.BOUNDARY+"-LEFT", labelId, lemma));
					}
					if(isRightBoundary){
						features.add(param_g.toFeature(network, specificIndicator+FeatureType.ORTHOGRAPHIC+"-LEMMA"+"-"+FeatureType.BOUNDARY+"-RIGHT", labelId, lemma));
					}
				}
			}
			
			if(FeatureType.BROWN_CLUSTER.enabled()){
				String brownCluster = getBrownCluster(curWord.value());
				for(int i=0; i<brownCluster.length(); i++){
					String brownFeatureString = brownCluster.substring(0, i+1);
					features.add(param_g.toFeature(network, generalIndicator+FeatureType.BROWN_CLUSTER.name(), labelId, brownFeatureString));
					features.add(param_g.toFeature(network, specificIndicator+FeatureType.BROWN_CLUSTER.name(), labelId, brownFeatureString));
					if(createMoreSpecificFeatures){
						features.add(param_g.toFeature(network, childListStr+FeatureType.BROWN_CLUSTER.name(), labelId, brownFeatureString));
					}
					if(FeatureType.BOUNDARY.enabled()){
						if(isLeftBoundary){
							features.add(param_g.toFeature(network, specificIndicator+FeatureType.BROWN_CLUSTER.name()+"-"+FeatureType.BOUNDARY+"-LEFT", labelId, brownFeatureString));
						}
						if(isRightBoundary){
							features.add(param_g.toFeature(network, specificIndicator+FeatureType.BROWN_CLUSTER.name()+"-"+FeatureType.BOUNDARY+"-RIGHT", labelId, brownFeatureString));
						}
					}
				}
			}

			if(FeatureType.NOTE_TYPE.enabled()){
				features.add(param_g.toFeature(network, generalIndicator+FeatureType.NOTE_TYPE.name(), labelId, instance.sourceDoc.docType.name()));
				features.add(param_g.toFeature(network, specificIndicator+FeatureType.NOTE_TYPE.name(), labelId, instance.sourceDoc.docType.name()));
				if(createMoreSpecificFeatures){
					features.add(param_g.toFeature(network, childListStr+FeatureType.NOTE_TYPE.name(), labelId, instance.sourceDoc.docType.name()));
				}
				if(FeatureType.BOUNDARY.enabled()){
					if(isLeftBoundary){
						features.add(param_g.toFeature(network, specificIndicator+FeatureType.NOTE_TYPE.name()+"-"+FeatureType.BOUNDARY+"-LEFT", labelId, instance.sourceDoc.docType.name()));
					}
					if(isRightBoundary){
						features.add(param_g.toFeature(network, specificIndicator+FeatureType.NOTE_TYPE.name()+"-"+FeatureType.BOUNDARY+"-RIGHT", labelId, instance.sourceDoc.docType.name()));
					}
				}
			}
			
			if(FeatureType.SECTION_NAME.enabled()){
				features.add(param_g.toFeature(network, generalIndicator+FeatureType.SECTION_NAME.name(), labelId, instance.sectionName));
				features.add(param_g.toFeature(network, specificIndicator+FeatureType.SECTION_NAME.name(), labelId, instance.sectionName));
				if(createMoreSpecificFeatures){
					features.add(param_g.toFeature(network, childListStr+FeatureType.SECTION_NAME.name(), labelId, instance.sectionName));
				}
				if(FeatureType.BOUNDARY.enabled()){
					if(isLeftBoundary){
						features.add(param_g.toFeature(network, specificIndicator+FeatureType.SECTION_NAME.name()+"-"+FeatureType.BOUNDARY+"-LEFT", labelId, instance.sectionName));
					}
					if(isRightBoundary){
						features.add(param_g.toFeature(network, specificIndicator+FeatureType.SECTION_NAME.name()+"-"+FeatureType.BOUNDARY+"-RIGHT", labelId, instance.sectionName));
					}
				}
			}
			
			if(FeatureType.SEMANTIC_CATEGORY.enabled()){
				String semanticCategory = getUmlsSemanticCategory(curWord.value());
				features.add(param_g.toFeature(network, generalIndicator+FeatureType.SEMANTIC_CATEGORY.name(), labelId, semanticCategory));
				features.add(param_g.toFeature(network, specificIndicator+FeatureType.SEMANTIC_CATEGORY.name(), labelId, semanticCategory));
				if(createMoreSpecificFeatures){
					features.add(param_g.toFeature(network, childListStr+FeatureType.SEMANTIC_CATEGORY.name(), labelId, semanticCategory));
				}
				if(FeatureType.BOUNDARY.enabled()){
					if(isLeftBoundary){
						features.add(param_g.toFeature(network, specificIndicator+FeatureType.SEMANTIC_CATEGORY.name()+"-"+FeatureType.BOUNDARY+"-LEFT", labelId, semanticCategory));
					}
					if(isRightBoundary){
						features.add(param_g.toFeature(network, specificIndicator+FeatureType.SEMANTIC_CATEGORY.name()+"-"+FeatureType.BOUNDARY+"-RIGHT", labelId, semanticCategory));
					}
				}
			}
			
			createMoreSpecificFeatures = false;
		}
		
		int[] featuresInt = new int[features.size()];
		for(int i=0; i<featuresInt.length; i++){
			featuresInt[i] = features.get(i);
		}
		FeatureArray result = new FeatureArray(featuresInt);
		return result;
	}
	
	public static enum ACEFeatureType {
		WORD,
		WORD_NGRAM,
		
		POS_TAG,
		POS_TAG_NGRAM,
		
		WORDSHAPE,
		WORDSHAPE_WINDOW,
		
		WORDTYPE,
		WORDTYPE_NGRAM,
		
		BOW,
		
		ALL_CAPS(AllCaps.class),
		ALL_DIGITS(AllDigits.class),
		ALL_ALPHANUMERIC(AllAlphanumeric.class),
		ALL_LOWERCASE(AllLowercase.class),
		CONTAINS_DIGITS(ContainsDigits.class),
		CONTAINS_DOTS(ContainsDots.class),
		CONTAINS_HYPHEN(ContainsHyphen.class),
		INITIAL_CAPS(InitialCaps.class),
		LONELY_INITIAL(LonelyInitial.class),
		PUNCTUATION_MARK(PunctuationMark.class),
		ROMAN_NUMBER(RomanNumber.class),
		SINGLE_CHARACTER(SingleCharacter.class),
		URL(URL.class),
		
		PREFIX,
		SUFFIX,
		
		BROWN_CLUSTER,
		
		MENTION_PENALTY,
		;
		
		public Class<? extends Key<String>> cls = null;
		
		private ACEFeatureType(){}
		
		private ACEFeatureType(Class<? extends Key<String>> cls){
			this.cls = cls;
		}
	}
	
	private static class FeatureCandidate{
		public String type;
		public String output;
		public String input;
		
		public FeatureCandidate(String type, String output, String input){
			this.type = type;
			this.output = output;
			this.input = input;
		}
	}
	
	protected FeatureArray extractHelperACE(OENetwork network, OEInstance instance, CoreLabel[] words, int size, int[] parentArr, int[][] childrenNodeArr) {
//		AttributedWord[] words = instance.input.words;
//		String[] posTags = instance.input.posTags;
		
		int pos = size-parentArr[0]-1;
		int bodyIdx = parentArr[1];
		OrigNodeType nodeType = OrigNodeType.values()[parentArr[2]];
		String labelId = String.valueOf(parentArr[3]);
		
		if(nodeType == OrigNodeType.X_NODE || nodeType == OrigNodeType.A_NODE || nodeType == OrigNodeType.E_NODE){
			return FeatureArray.EMPTY;
		}
		if(nodeType == OrigNodeType.T_NODE && childrenNodeArr[0][2] == OrigNodeType.X_NODE.ordinal()){
			// Do nothing
			// Do not need to add this feature as the feature T->B is a complement of this feature
			return FeatureArray.EMPTY;
		}
		
		List<Integer> features = new ArrayList<Integer>();
		GlobalNetworkParam param_g = this._param_g;
		
		String childListStr = "";
		for(int[] childNodeArr: childrenNodeArr){
			if(childListStr.length() > 0){
				childListStr += "#";
			}
			childListStr += OrigNodeType.values()[childNodeArr[2]].name()+childNodeArr[1];
		}
		String specificIndicator = nodeType+"-"+childListStr;
		
		if(FeatureType.CHEAT.enabled()){
			int cheatFeature = param_g.toFeature(network, FeatureType.CHEAT.name(), labelId, Math.abs(instance.getInstanceId())+" "+pos+" "+nodeType+bodyIdx+" "+childListStr);
			return new FeatureArray(new int[]{cheatFeature});
		}
		
		if(FeatureType.TRANSITION.enabled()){
			features.add(param_g.toFeature(network, FeatureType.TRANSITION.name(), labelId, specificIndicator));
		}

		boolean first = true;

		ArrayList<FeatureCandidate> featureCandidates = extractFeatureAtPos(words, size, pos, labelId);
		for(int[] childNodeArr: childrenNodeArr){
			OrigNodeType childNodeType = OrigNodeType.values()[childNodeArr[2]];
		
			if(FeatureType.MENTION_PENALTY.enabled() && nodeType == OrigNodeType.T_NODE && childNodeType == OrigNodeType.B_NODE){
				features.add(param_g.toFeature(network, ACEFeatureType.MENTION_PENALTY.name(), "MP", "MP"));
			}
			
			String indicator = nodeType+"-"+childNodeType;
			for(FeatureCandidate candidate: featureCandidates){
				if(useSpecificIndicator){
					features.add(param_g.toFeature(network, indicator+":"+candidate.type, candidate.output, candidate.input));
					if(childrenNodeArr.length > 1 && first){
						features.add(param_g.toFeature(network, specificIndicator+":"+candidate.type, candidate.output, candidate.input));
					}
				} else {
					if(first){
						switch(nodeType){
						case T_NODE:
							features.add(param_g.toFeature(network, "START_MENTION:"+candidate.type, candidate.output, candidate.input));
							break;
						case B_NODE:
							features.add(param_g.toFeature(network, "WITHIN_MENTION:"+candidate.type, candidate.output, candidate.input));
							break;
						default:
							break;
						}
					}
					if(childNodeType == OrigNodeType.X_NODE){
						features.add(param_g.toFeature(network, "END_MENTION:"+candidate.type, candidate.output, candidate.input));
					}
				}
			}
//			if(!useSpecificIndicator){
//				if (nodeType == OrigNodeType.B_NODE && childNodeType == OrigNodeType.B_NODE) {
//					ArrayList<FeatureCandidate> nextFeatureCandidates = extractFeatureAtPos(words, size, pos+1, labelId);
//					for(FeatureCandidate candidate: nextFeatureCandidates){
//						features.add(param_g.toFeature(network, "WITHIN_MENTION:"+candidate.type, candidate.output, candidate.input));
//					}
//				}
//			}
			first = false;
		}
		
		int[] featuresInt = new int[features.size()];
		int featurePos = 0;
		Iterator<Integer> featureIter = features.iterator();
		while(featureIter.hasNext()){
			featuresInt[featurePos] = featureIter.next();
			featurePos += 1;
		}
		FeatureArray result = new FeatureArray(featuresInt);
		return result;
	}

	private ArrayList<FeatureCandidate> extractFeatureAtPos(CoreLabel[] words, int size, int pos, String labelId) {
		ArrayList<FeatureCandidate> featureCandidates = new ArrayList<FeatureCandidate>();
		if(FeatureType.WORDS.enabled()){
			for(int idx=pos-wordHalfWindowSize; idx<=pos+wordHalfWindowSize; idx++){
				String word = "";
				if(idx >= 0 && idx < size){
					word = words[idx].value();
				}
				featureCandidates.add(new FeatureCandidate(ACEFeatureType.WORD.name()+"["+(idx-pos)+"]", labelId, word));
			}
		}
		if(FeatureType.POS_TAG.enabled()){
			for(int idx=pos-posHalfWindowSize; idx<=pos+posHalfWindowSize; idx++){
				String postag = "";
				if(idx >= 0 && idx < size){
					postag = words[idx].tag();// posTags[idx];
				}
				featureCandidates.add(new FeatureCandidate(ACEFeatureType.POS_TAG.name()+"["+(idx-pos)+"]", labelId, postag));
			}
		}
		if(FeatureType.WORDSHAPE.enabled()){
			for(int idx=pos-1; idx<=pos+1; idx++){
				if(idx < 0 || idx >= size){
					continue;
				}
				String wordshape = WordShapeClassifier.wordShape(words[idx].value(), WordShapeClassifier.WORDSHAPEJENNY1);
				featureCandidates.add(new FeatureCandidate(ACEFeatureType.WORDSHAPE.name()+"["+(idx-pos)+"]", labelId, wordshape));
				if(idx < pos+1 && idx+1 < size){
					String nextWordshape = WordShapeClassifier.wordShape(words[idx+1].value(), WordShapeClassifier.WORDSHAPEJENNY1);
					featureCandidates.add(new FeatureCandidate(ACEFeatureType.WORDSHAPE.name()+"["+(idx-pos)+","+(idx-pos+1)+"]", labelId, wordshape+"_"+nextWordshape));
				}
				if(idx == pos){
					if(idx-1 >= 0){
						featureCandidates.add(new FeatureCandidate(ACEFeatureType.WORD.name()+"[-1]-"+ACEFeatureType.WORDSHAPE.name()+"[0]", labelId, words[idx-1].value()+"_"+wordshape));
					}
					if(idx+1 < size){
						featureCandidates.add(new FeatureCandidate(ACEFeatureType.WORDSHAPE.name()+"[0]-"+ACEFeatureType.WORD.name()+"[1]", labelId, wordshape+"_"+words[idx+1].value()));
					}
				}
			}
		}
		if(FeatureType.WORDSHAPE_WINDOW.enabled()){
			for(int idx=pos-wordHalfWindowSize; idx<=pos+wordHalfWindowSize; idx++){
				if(idx < 0 || idx >= size){
					continue;
				}
				String wordshape = WordShapeClassifier.wordShape(words[idx].value(), WordShapeClassifier.WORDSHAPEJENNY1);
				featureCandidates.add(new FeatureCandidate(ACEFeatureType.WORDSHAPE.name()+"["+(idx-pos)+"]", labelId, wordshape));
			}
		}
		if(FeatureType.WORD_NGRAM.enabled()){
			for(int ngramSize=2; ngramSize<=wordNGramSize; ngramSize++){
				for(int relPos=0; relPos<ngramSize; relPos++){
					String ngram = "";
					for(int idx=pos-ngramSize+relPos+1; idx<pos+relPos+1; idx++){
						if(ngram.length() > 0) ngram += " ";
						if(idx >= 0 && idx < size){
							ngram += words[idx].value();
						}
					}
					featureCandidates.add(new FeatureCandidate(ACEFeatureType.WORD_NGRAM+"["+ngramSize+","+relPos+"]", labelId, ngram));
				}
			}
		}
		if(FeatureType.POS_TAG_NGRAM.enabled()){
			for(int ngramSize=2; ngramSize<=posNGramSize; ngramSize++){
				for(int relPos=0; relPos<ngramSize; relPos++){
					String ngram = "";
					for(int idx=pos-ngramSize+relPos+1; idx<pos+relPos+1; idx++){
						if(idx > pos-ngramSize+relPos+1) ngram += " ";
						if(idx >= 0 && idx < size){
							ngram += words[idx].tag(); //posTags[idx];
						}
					}
					featureCandidates.add(new FeatureCandidate(ACEFeatureType.POS_TAG_NGRAM+"["+ngramSize+","+relPos+"]", labelId, ngram));
				}
			}
		}
		if(FeatureType.BOW.enabled()){
			List<String> bowList = new ArrayList<String>();
			for(int idx=pos-bowHalfWindowSize; idx<=pos+bowHalfWindowSize; idx++){
				if(idx >= 0 && idx < size){
					bowList.add(words[idx].value());
				}
			}
			Collections.sort(bowList);
//				String bow = "";
			for(String word: bowList){
//					if(bow.length() > 0) bow += " ";
//					bow += word;
				featureCandidates.add(new FeatureCandidate(ACEFeatureType.BOW.name(), labelId, word));
			}
//				features.add(param_g.toFeature(network, indicator+":"+ACEFeatureType.BOW.name(), labelId, bow));
//				if(childrenNodeArr.length > 1){
//					features.add(param_g.toFeature(network, specificIndicator+":"+ACEFeatureType.BOW.name(), labelId, bow));
//				}
		}
		
		if(FeatureType.WORDTYPE.enabled()){
			for(int idx=pos-wordTypeHalfWindowSize; idx<=pos+wordTypeHalfWindowSize; idx++){
				String word = "";
				if(idx >= 0 && idx < size){
					word = getNEWordType(words[idx]);
				}
				featureCandidates.add(new FeatureCandidate(ACEFeatureType.WORDTYPE.name()+"["+(idx-pos)+"]", labelId, word));
			}
		}
		
		if(FeatureType.WORDTYPE_NGRAM.enabled()){
			for(int ngramSize=2; ngramSize<=wordTypeNGramSize; ngramSize++){
				for(int relPos=0; relPos<ngramSize; relPos++){
					String ngram = "";
					for(int idx=pos-ngramSize+relPos+1; idx<pos+relPos+1; idx++){
						if(ngram.length() > 0) ngram += " ";
						if(idx >= 0 && idx < size){
							ngram += getNEWordType(words[idx]);
						}
					}
					featureCandidates.add(new FeatureCandidate(ACEFeatureType.WORDTYPE_NGRAM+"["+ngramSize+","+relPos+"]", labelId, ngram));
				}
			}
		}
		
		if(FeatureType.ORTHOGRAPHIC.enabled()){
			for(ACEFeatureType featureType: ACEFeatureType.values()){
				switch(featureType){
				case ALL_CAPS:
				case ALL_DIGITS:
				case ALL_ALPHANUMERIC:
				case ALL_LOWERCASE:
				case CONTAINS_DIGITS:
				case CONTAINS_DOTS:
//					case CONTAINS_HYPHEN:
				case INITIAL_CAPS:
				case LONELY_INITIAL:
				case PUNCTUATION_MARK:
				case ROMAN_NUMBER:
				case SINGLE_CHARACTER:
				case URL:
//						System.err.println(curWord+"["+featureType+"]: "+curWord.get(featureType.cls));
					featureCandidates.add(new FeatureCandidate(featureType.name(), labelId, words[pos].get(featureType.cls)));
					break;
				default:
					break;
				}
			}
		}
		if(FeatureType.PREFIX.enabled()){
			// Prefix
			String curWordString = words[pos].value();
			for(int i=0; i<prefixLength; i++){
				if(i >= curWordString.length()){
					break;
				}
				String prefix = curWordString.substring(0, Math.min(curWordString.length(), i+1));
				featureCandidates.add(new FeatureCandidate(ACEFeatureType.PREFIX+"["+(i+1)+"]", labelId, prefix));
			}
		}

		if(FeatureType.SUFFIX.enabled()){
			// Suffix
			String curWordString = words[pos].value();
			for(int i=0; i<suffixLength; i++){
				if(i >= curWordString.length()){
					break;
				}
				String suffix = curWordString.substring(Math.max(0, curWordString.length()-i-1), curWordString.length());
				featureCandidates.add(new FeatureCandidate(ACEFeatureType.SUFFIX+"["+(i+1)+"]", labelId, suffix));
			}
		}
		
		if(FeatureType.BROWN_CLUSTER.enabled()){
			for(int i=-1; i<=1; i++){
				if(pos+i >= 0 && pos+i < size){
					for(int len: new int[]{4, 6, 9, 15}){
						String cluster = getBrownCluster(words[pos+i].value());
						while(cluster.length() < len){
							cluster += "-";
						}
						cluster = cluster.substring(0, len);
						featureCandidates.add(new FeatureCandidate(ACEFeatureType.BROWN_CLUSTER+"["+i+",len="+len+"]", labelId, cluster));
					}
				}
			}
		}
		return featureCandidates;
	}
	
	private String getBrownCluster(String word){
		if(brownMap == null){
			throw new NullPointerException("Feature requires brown clusters but no brown clusters info is provided");
		}
		String clusterId = brownMap.get(word);
		if(clusterId == null){
			clusterId = "-";
		}
		return clusterId;
	}
	
	private String getUmlsSemanticCategory(String word){
		if(umlsSemanticCategory == null){
			throw new NullPointerException("Feature requires UMLS semantic category but no UMLS semantic category info is provided");
		}
		String semanticCategory = umlsSemanticCategory.get(word);
		if(semanticCategory == null){
			semanticCategory = "General";
		}
		return semanticCategory;
	}
	
	private void writeObject(ObjectOutputStream oos) throws IOException{
		oos.writeObject(posTaggerMethod);
		oos.writeObject(lemmatizerMethod);
		oos.writeObject(brownMap);
		oos.writeObject(umlsSemanticCategory);
		oos.writeObject(_func_words);
		oos.writeInt(wordHalfWindowSize);
		oos.writeInt(wordTypeHalfWindowSize);
		oos.writeInt(bowHalfWindowSize);
		oos.writeInt(posHalfWindowSize);
		oos.writeInt(wordNGramSize);
		oos.writeInt(wordTypeNGramSize);
		oos.writeInt(posNGramSize);
		oos.writeInt(prefixLength);
		oos.writeInt(suffixLength);
		oos.writeBoolean(wordOnlyLeftWindow);
		oos.writeBoolean(bowOnlyLeftWindow);
		oos.writeBoolean(posOnlyLeftWindow);
		oos.writeBoolean(useSpecificIndicator);
		oos.writeInt(FeatureType.values().length);
		for(FeatureType featureType: FeatureType.values()){
			oos.writeObject(featureType.name());
			oos.writeBoolean(featureType.isEnabled);
		}
	}
	
	@SuppressWarnings("unchecked")
	private void readObject(ObjectInputStream ois) throws IOException, ClassNotFoundException{
		posTaggerMethod = (POSTaggerMethod)ois.readObject();
		lemmatizerMethod = (LemmatizerMethod)ois.readObject();
		brownMap = (Map<String, String>)ois.readObject();
		umlsSemanticCategory = (Map<String, String>)ois.readObject();
		_func_words = (HashSet<String>)ois.readObject();
		wordHalfWindowSize = ois.readInt();
		wordTypeHalfWindowSize = ois.readInt();
		bowHalfWindowSize = ois.readInt();
		posHalfWindowSize = ois.readInt();
		wordNGramSize = ois.readInt();
		wordTypeNGramSize = ois.readInt();
		posNGramSize = ois.readInt();
		prefixLength = ois.readInt();
		suffixLength = ois.readInt();
		wordOnlyLeftWindow = ois.readBoolean();
		bowOnlyLeftWindow = ois.readBoolean();
		posOnlyLeftWindow = ois.readBoolean();
		useSpecificIndicator = ois.readBoolean();
		int numFeatureTypes = ois.readInt();
		for(int i=0; i<numFeatureTypes; i++){
			FeatureType featureType = FeatureType.valueOf((String)ois.readObject());
			featureType.isEnabled = ois.readBoolean();
		}
	}

}
