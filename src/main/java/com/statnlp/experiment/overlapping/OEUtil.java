package com.statnlp.experiment.overlapping;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.io.Serializable;
import java.lang.reflect.Method;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.Random;
import java.util.Scanner;
import java.util.Set;
import java.util.Stack;
import java.util.TreeSet;
import java.util.regex.Pattern;

import com.statnlp.experiment.overlapping.Main.UMLSCategory;
import com.statnlp.experiment.overlapping.OESplitter.SplitterMethod;
import com.statnlp.experiment.overlapping.OETokenizer.TokenizerMethod;
import com.statnlp.experiment.overlapping.WordAttributes.Chunk;
import com.statnlp.experiment.overlapping.multigraph.MultigraphFeatureManager.ACEFeatureType;

import edu.stanford.nlp.ling.CoreLabel;
import edu.stanford.nlp.util.StringUtils;

public class OEUtil {
	
	public static final String GENERAL_SECTION = "general";
	
	public static final Random RANDOM = new Random(1023);
	
	public static void print(String message, boolean printEndline, PrintStream... outstream){
		if(outstream.length == 0){
			outstream = new PrintStream[]{System.out};
		}
		for(PrintStream stream: outstream){
			if(stream != null){
				if(printEndline){
					stream.println(message);
				} else {
					stream.print(message);
				}
			}
		}
	}
	
	/**
	 * Read a source clinical text
	 * @param sourcePath
	 * @return
	 * @throws IOException
	 */
	public static OEDocument readSource(String sourcePath) throws IOException{
		byte[] textByte = Files.readAllBytes(Paths.get(sourcePath));
		String text = new String(textByte, "UTF-8");
		return new OEDocument(text, sourcePath);
	}
	
	/**
	 * Read data from the source and annotation files.<br>
	 * The source is a simple text file, while the annotation file contains one or more lines
	 * in the format of SemEval 2014 or ShARe/CLEF 2013.<br>
	 * For SemEval 2014, the format is:<br>
	 * - Each token is separated by double pipe "||"<br>
	 * - The first token is the file name<br>
	 * - The second token is the annotation type<br>
	 * - The third token is the CUI (Concept Unique Identifier)<br>
	 * - Every next two tokens specify the start (inclusive) and end (exclusive) character offset of this 
	 * 	 annotation, possibly non-contiguous or overlapping with other annotations<br>
	 * <br>
	 * For ShARe/CLEF 2013, the format is:<br>
	 * - Each token is separated by single pipe "|"<br>
	 * - The first token is the file name<br>
	 * - The second token is entity span (nnn-nnn), separated by comma for discontiguous entities<br>
	 * - The third token is the CUI (Concept Unique Identifier)<br>
	 * - The rest is unrelated to this task<br>
	 * <br>
	 * This method assumes that each annotation file corresponds to exactly one source text, which is
	 * specified in the annotation file.
	 * @param sourceDir
	 * @param annotationPath
	 * @param hierarchyLevel TODO
	 * @param randomSplitLabels TODO
	 * @param numRandomLabels TODO
	 * @return
	 * @throws IOException
	 */
	public static OEDocument readAnnotatedData(String sourceDir, String annotationPath, Map<String, UMLSCategory> cuiToCat, int hierarchyLevel, boolean ignoreNullCategory, boolean randomSplitLabels, int numRandomLabels) throws IOException{
		List<EntitySpan> annotations = new ArrayList<EntitySpan>();
		String sourceFilename = readAnnotations(annotationPath, annotations, cuiToCat, hierarchyLevel, ignoreNullCategory, randomSplitLabels, numRandomLabels);
		OEDocument sourceDoc;
		if(sourceFilename != null){
			sourceDoc = readSource(sourceDir+File.separator+sourceFilename);
		} else {
			int lastSlash = annotationPath.lastIndexOf(File.separator);
			sourceDoc = readSource(sourceDir+File.separator+annotationPath.replace(".pipe", "").substring(lastSlash+1));
		}
		sourceDoc.annotations = annotations;
		return sourceDoc;
	}

	/**
	 * Read annotation from the specified annotation file, and add the annotations found to the given list.<br>
	 * Returns the source file name
	 * @param annotationFileName
	 * @param annotations
	 * @param hierarchyLevel TODO
	 * @param randomSplitLabels TODO
	 * @param numRandomLabels TODO
	 * @return
	 * @throws IOException
	 */
	public static String readAnnotations(String annotationFileName, List<EntitySpan> annotations, Map<String, UMLSCategory> cuiToCat, int hierarchyLevel, boolean ignoreNullCategory, boolean randomSplitLabels, int numRandomLabels) throws IOException {
		String sourceFilename = null;
		InputStreamReader isr = new InputStreamReader(new FileInputStream(annotationFileName), "UTF-8");
		BufferedReader br = new BufferedReader(isr);
		while(br.ready()){
			String line = br.readLine();
			if(line.contains("||")){
				String[] tokens = line.split("\\|\\|");
				sourceFilename = tokens[0];
				String cui = normalizeCUI(tokens[2]);
				SpanLabel label;
				if(randomSplitLabels){
					label = SpanLabel.get(tokens[1]+"_"+RANDOM.nextInt(numRandomLabels));
				} else if(cuiToCat != null && !cui.equals("null") && cuiToCat.get(cui) != null){
					label = SpanLabel.get(cuiToCat.get(cui).getCodeHierAt(hierarchyLevel));
				} else if(cuiToCat != null && ignoreNullCategory){
					continue;
				} else {
					label = SpanLabel.get(tokens[1]);
				}
				int[] startEnd = new int[tokens.length-3];
				for(int i=3; i<tokens.length; i++){
					startEnd[i-3] = Integer.parseInt(tokens[i]);
				}
				EntitySpan annotation = new EntitySpan(cui, label, startEnd);
				annotations.add(annotation);
			} else {
				String[] tokens = line.split("\\|");
				sourceFilename = tokens[0];
				String cui = normalizeCUI(tokens[2]);
				SpanLabel label;
				if(randomSplitLabels){
					label = SpanLabel.get(tokens[1]+"_"+RANDOM.nextInt(numRandomLabels));
				} else if(cuiToCat != null && !cui.equals("null") && cuiToCat.get(cui) != null){
					label = SpanLabel.get(cuiToCat.get(cui).getCodeHierAt(hierarchyLevel));
				} else if(cuiToCat != null && ignoreNullCategory){
					continue;
				} else {
					label = SpanLabel.get("Disease_Disorder");
				}
				String[] startEndTokens = tokens[1].split("[,-]");
				int[] startEnd = new int[startEndTokens.length];
				for(int i=0; i<startEndTokens.length; i++){
					startEnd[i] = Integer.parseInt(startEndTokens[i]);
				}
				EntitySpan annotation = new EntitySpan(cui, label, startEnd);
				annotations.add(annotation);
			}
		}
		br.close();
		return sourceFilename;
	}

	private static String normalizeCUI(String cui) {
		if(!cui.matches("C\\d{7}")){
			cui = "null";
		}
		return cui;
	}
	
	public static enum InstanceFilter {
		ALL_INSTANCES,
		ALL_WITH_ENTITIES,
		ONLY_CONTIGUOUS,
		CONTAIN_DISCONTIGUOUS,
		NO_DISCONTIGUOUS,
	}
	
	/**
	 * Return the instances extracted from a source document.<br>
	 * If the source document contains annotations, the instances will contain annotations also.<br>
	 * @param sourceDoc
	 * @param isLabeled Whether the instances are set as labeled
	 * @param splitterMethod
	 * @param tokenizerMethod
	 * @return
	 */
	public static OEInstance[] getInstances(List<OEDocument> sourceDocs, boolean isLabeled, SplitterMethod splitterMethod, TokenizerMethod tokenizerMethod, InstanceFilter instanceFilter){
		int instanceId = 1;
		List<OEInstance> instances = new ArrayList<OEInstance>();
		for(OEDocument sourceDoc: sourceDocs){
			List<CoreLabel> sentences = OESplitter.split(sourceDoc.text, splitterMethod);
			String sectionName = normalizeSectionName(null);
			for(CoreLabel sentence: sentences){
				String sentenceText = sentence.value();
				if(sentenceText.trim().length() == 0){
					continue;
				}
				if(sentenceText.endsWith(":")){
					sectionName = normalizeSectionName(sentenceText.substring(0, sentenceText.length()-1));
				}
				Span sentenceSpan = new Span(sentence.beginPosition(), sentence.endPosition());
				List<EntitySpan> entitySpans = getEntitySpans(sourceDoc.annotations, sentenceSpan);
				switch(instanceFilter){
				case ALL_INSTANCES:
					break;
				case ALL_WITH_ENTITIES:
					if(entitySpans.size() == 0){
						continue;
					}
					break;
				case ONLY_CONTIGUOUS:
					if(entitySpans.size() == 0 || hasDiscontiguousEntity(entitySpans)){
						continue;
					}
					break;
				case CONTAIN_DISCONTIGUOUS:
					if(!hasDiscontiguousEntity(entitySpans)){
						continue;
					}
					break;
				case NO_DISCONTIGUOUS:
					if(hasDiscontiguousEntity(entitySpans)){
						continue;
					}
				}
				OEInstance instance = new OEInstance(instanceId, 1.0, sentenceText, entitySpans, sourceDoc, sentenceSpan, sectionName);
				if(isLabeled){
					instance.setLabeled();
				} else {
					instance.setUnlabeled();
				}
				instances.add(instance);
				instanceId++;
			}
		}
		return instances.toArray(new OEInstance[instances.size()]);
	}
	
	public static String escapeBracket(String word){
		if(word.contains("(")){
			return "-LRB-";
		} else if(word.contains(")")){
			return "-RRB-";
		} else if(word.contains("[")){
			return "-LSB-";
		} else if(word.contains("]")){
			return "-RSB-";
		} else if(word.contains("{")){
			return "-LCB-";
		} else if(word.contains("}")){
			return "-RCB-";
		} else {
			return word;
		}
	}
	
	public static OEInstance[] readFromFormattedData(String file, boolean isLabeled, boolean hasPOS, boolean normalizeDateAndNumbers) throws FileNotFoundException{
		List<OEInstance> result = new ArrayList<OEInstance>();
		Scanner scanner = new Scanner(new File(file), "UTF-8");
		int instanceId = 1;
		while(scanner.hasNextLine()){
			String input = scanner.nextLine();
			List<CoreLabel> inputTokenized = new ArrayList<CoreLabel>();
			int pos = 0;
			for(String wordStr: input.split(" ")){
				CoreLabel word = new CoreLabel();
				if(normalizeDateAndNumbers){
					wordStr = normalizeDateAndNumbers(wordStr);
				}
				word.setWord(escapeBracket(wordStr));
				word.setValue(wordStr);
				word.setOriginalText(wordStr);
				word.setAfter(" ");
				word.setBefore(" ");
				word.setBeginPosition(pos);
				word.setEndPosition(pos+wordStr.length());
				boolean alphanumeric = wordStr.matches("[\\p{L}\\p{N}]+");
				word.set(ACEFeatureType.ALL_CAPS.cls, ((alphanumeric && wordStr.toUpperCase().equals(wordStr))+""));
				word.set(ACEFeatureType.ALL_DIGITS.cls, (wordStr.matches("[\\p{N}]+([.,][\\p{N}]+)*"))+"");
				word.set(ACEFeatureType.ALL_ALPHANUMERIC.cls, alphanumeric+"");
				word.set(ACEFeatureType.ALL_LOWERCASE.cls, ((wordStr.matches("[a-z]+"))+""));
				word.set(ACEFeatureType.CONTAINS_DIGITS.cls, (wordStr.matches(".*[0-9].*"))+"");
				word.set(ACEFeatureType.CONTAINS_DOTS.cls, (wordStr.matches(".*\\..*"))+"");
				word.set(ACEFeatureType.CONTAINS_HYPHEN.cls, (wordStr.matches(".*-.*"))+"");
				word.set(ACEFeatureType.INITIAL_CAPS.cls, (wordStr.matches("[A-Z].*"))+"");
				word.set(ACEFeatureType.LONELY_INITIAL.cls, (wordStr.matches("[A-Z]+\\."))+"");
				word.set(ACEFeatureType.PUNCTUATION_MARK.cls, (wordStr.matches("[-!@#$%^&*()+=,.<>/?\\;:'\"{}\\[\\]]"))+"");
				word.set(ACEFeatureType.ROMAN_NUMBER.cls, (wordStr.matches("[MDCLXVI]+"))+"");
				word.set(ACEFeatureType.SINGLE_CHARACTER.cls, (wordStr.length() == 1)+"");
				word.set(ACEFeatureType.URL.cls, (wordStr.matches("((https?://|www\\.).*|.*(\\.com|\\.net|\\.org)(/.*|$))"))+"");
				inputTokenized.add(word);
				pos += wordStr.length()+1;
			}
			if(hasPOS){
				String[] posTags = scanner.nextLine().split(" ");
				for(int i=0; i<posTags.length; i++){
					if(posTags[i].contains("|")){
						String[] pos_chunk = posTags[i].split("\\|");
						inputTokenized.get(i).setTag(pos_chunk[0]);
						inputTokenized.get(i).set(Chunk.class, pos_chunk[1]);
					} else {
						inputTokenized.get(i).setTag(posTags[i]);
					}
					if(!inputTokenized.get(i).tag().equals("*")){
						for(int j=i-1; j>=0 && inputTokenized.get(j).tag().equals("*"); j--){
							inputTokenized.get(j).setTag(posTags[i]);
						}
					}
				}
			}
			List<EntitySpan> entitySpans = new ArrayList<EntitySpan>();
			String[] annotations = scanner.nextLine().split("\\|");
			for(String annotation: annotations){
				if(annotation.length() == 0){
					continue;
				}
				String[] offsetsAndLabel = annotation.split(" ");
				String[] offsetsStr = offsetsAndLabel[0].split("\\+");
				String entityText = "";
				Span[] spans = new Span[offsetsStr.length];
				for(int componentIdx=0; componentIdx < spans.length; componentIdx++){
					int beginPosition = -1;
					int endPosition = -1;
					String[] startEnd = offsetsStr[componentIdx].split(",");
					for(int i=Integer.parseInt(startEnd[0]); i<Integer.parseInt(startEnd[1]); i++){
						if(entityText.length() > 0){
							entityText += " ";
						}
						entityText += inputTokenized.get(i).value();
						if(beginPosition == -1){
							beginPosition = inputTokenized.get(i).beginPosition();
						}
						endPosition = inputTokenized.get(i).endPosition();
					}
					spans[componentIdx] = new Span(beginPosition, endPosition);
				}
				SpanLabel label = SpanLabel.get(offsetsAndLabel[1]);
				entitySpans.add(new EntitySpan(entityText, label, spans));
			}
			OEInstance instance = new OEInstance(instanceId, 1.0, input, entitySpans, null, null, "");
			instance.inputTokenized = inputTokenized;
			instance.posTagged = hasPOS;
			if(isLabeled){
				instance.setLabeled();	
			} else {
				instance.setUnlabeled();
			}
			result.add(instance);
			scanner.nextLine();
			instanceId += 1;
		}
		scanner.close();
		return result.toArray(new OEInstance[result.size()]);
	}
	
	public static final Set<String> MONTHS = new HashSet<String>();
	static{
		String[] months = new String[]{"Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec",
										"January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"};
		for(String month: months){
			MONTHS.add(month);
		}
	}
	
	public static final Pattern DATE_PATTERN = Pattern.compile("(\\d{1,2}[-/]\\d{1,2}[-/]\\d\\d(\\d\\d)?|\\d\\d(\\d\\d)?[-/]\\d{1,2}[-/]\\d{1,2})");
	public static final Pattern NUMBER_PATTERN = Pattern.compile("\\d+([^\\dA-Za-z]\\d+)*");
	
	public static String normalizeDateAndNumbers(String word){
		if(MONTHS.contains(word)){
			return "*MONTH*";
		}
		if(DATE_PATTERN.matcher(word).matches()){
			return "*DATE*";
		}
		if(NUMBER_PATTERN.matcher(word).matches()){
			return "*"+word.replaceAll("\\d", "D")+"*";
		}
		return word;
	}
	
	private static boolean hasDiscontiguousEntity(List<EntitySpan> entities){
		for(EntitySpan entity: entities){
			if(entity.spans.length > 1){
				return true;
			}
		}
		return false;
	}
	
	private static List<EntitySpan> getEntitySpans(List<EntitySpan> annotations, Span span){
		List<EntitySpan> result = new ArrayList<EntitySpan>();
		if(annotations == null){
			return result;
		}
		for(EntitySpan annotation: annotations){
			Span firstSpan = annotation.spans[0];
			Span lastSpan = annotation.spans[annotation.spans.length-1];
			if(firstSpan.start >= span.start && lastSpan.end <= span.end){
				EntitySpan offsetEntity = new EntitySpan(annotation);
				for(Span entitySpan: offsetEntity.spans){
					entitySpan.start -= span.start;
					entitySpan.end -= span.start;
				}
				result.add(offsetEntity);
			}
		}
		return result;
	}
	
	public static String normalizeSectionName(String sectionName){
		if(sectionName == null){
			return GENERAL_SECTION;
		}
		if(sectionName.contains("||")){
			return GENERAL_SECTION;
		}
		return sectionName.toLowerCase();
	}
	
	public static OEInstance[] readFromPathAndGetInstances(String sourcePath, String annotationPath, boolean isLabeled, SplitterMethod splitterMethod, TokenizerMethod tokenizerMethod, InstanceFilter instanceFilter, Map<String, UMLSCategory> cuiToCat, int hierarchyLevel, boolean ignoreNullCategory, boolean randomSplitLabels, int numRandomLabels) throws IOException{
		OEDocument sourceDoc = readSource(sourcePath);
		List<EntitySpan> annotations = new ArrayList<EntitySpan>();
		readAnnotations(annotationPath, annotations, cuiToCat, 0, ignoreNullCategory, false, 0);
		sourceDoc.annotations = annotations;
		return getInstances(Arrays.asList(new OEDocument[]{sourceDoc}), isLabeled, splitterMethod, tokenizerMethod, instanceFilter);
	}
	
	public static OEInstance[] readFromDirAndGetInstances(String sourceDir, String annotationDir, boolean isLabeled, SplitterMethod splitterMethod, TokenizerMethod tokenizerMethod, InstanceFilter instanceFilter, Map<String, UMLSCategory> cuiToCat, int hierarchyLevel, boolean ignoreNullCategory, boolean randomSplitLabel, int numRandomLabels) throws IOException{
		List<OEDocument> sourceDocs = new ArrayList<OEDocument>();
		for(File annotationPath: new File(annotationDir).listFiles()){
			if(!annotationPath.getName().endsWith(".pipe.txt")){
				continue;
			}
			OEDocument sourceDoc = readAnnotatedData(sourceDir, annotationPath.getAbsolutePath(), cuiToCat, hierarchyLevel, ignoreNullCategory, randomSplitLabel, numRandomLabels);
			sourceDocs.add(sourceDoc);
		}
		return getInstances(sourceDocs, isLabeled, splitterMethod, tokenizerMethod, instanceFilter);
	}

	public static List<WordLabel> spansToLabels(List<EntitySpan> output, List<Span> wordSpansList, boolean ignoreOverlap, boolean useBILOU){
		List<WordLabel> result = new ArrayList<WordLabel>();
		Collections.sort(output);
		Collections.sort(wordSpansList);
		WordLabel[] labels = new WordLabel[wordSpansList.size()];
		Span[] wordSpans = wordSpansList.toArray(new Span[wordSpansList.size()]);
		int startWordIdx = 0;
		List<EntitySpan> filteredOutput = new ArrayList<EntitySpan>();
		if(ignoreOverlap){
			List<EntitySpan> sortedOutputByLength = new ArrayList<EntitySpan>();
			sortedOutputByLength.addAll(output);
			Collections.sort(sortedOutputByLength, new Comparator<EntitySpan>(){

				@Override
				public int compare(EntitySpan o1, EntitySpan o2) {
					Integer l1 = o1.end()-o1.start();
					Integer l2 = o2.end()-o2.start();
					return -l1.compareTo(l2);
				}
				
			});
			for(EntitySpan entity: sortedOutputByLength){
				boolean shouldBeAdded = true;
				for(EntitySpan otherEntity: filteredOutput){
					if(otherEntity.overlapsWith(entity)){
						shouldBeAdded = false;
						break;
					}
				}
				if(shouldBeAdded){
					filteredOutput.add(entity);
				}
			}
			Collections.sort(filteredOutput);
		} else {
			filteredOutput.addAll(output);
		}
		for(EntitySpan entitySpan: filteredOutput){
			boolean discontiguous = (entitySpan.spans.length > 1);
			for(int i=startWordIdx; i<wordSpans.length; i++){
				Span wordSpan = wordSpans[i];
				if(wordSpan.end < entitySpan.spans[0].start){
					startWordIdx++;
				} else {
					break;
				}
			}
			for(Span span: entitySpan.spans){
				int wordIdx = startWordIdx;
				for(; wordIdx<wordSpans.length; wordIdx++){
					Span wordSpan = wordSpans[wordIdx];
					if(wordSpan.start < span.start){
						if(wordSpan.end <= span.start){
							;
						} else {
							// Output span starts at the middle of this word span
							// Label this whole word as the output
							if(labels[wordIdx] == null){
								if(discontiguous){
									labels[wordIdx] = WordLabel.get("BD-"+entitySpan.label.form);
								} else {
									labels[wordIdx] = WordLabel.get("B-"+entitySpan.label.form);
								}
							} else {
								if(ignoreOverlap){
									throw new RuntimeException("Ignoring overlap but overlapping entity still exists.");
								}
								labels[wordIdx] = WordLabel.get("BH-"+entitySpan.label.form);
							}
						}
					} else if(wordSpan.start == span.start){
						if(labels[wordIdx] == null){
							if(discontiguous){
								labels[wordIdx] = WordLabel.get("BD-"+entitySpan.label.form);
							} else {
								labels[wordIdx] = WordLabel.get("B-"+entitySpan.label.form);
							}
						} else {
							if(ignoreOverlap){
								throw new RuntimeException("Ignoring overlap but overlapping entity still exists.");
							}
							labels[wordIdx] = WordLabel.get("BH-"+entitySpan.label.form);
						}
					} else {
						if(labels[wordIdx] == null){
							if(discontiguous){
								labels[wordIdx] = WordLabel.get("ID-"+entitySpan.label.form);	
							} else {
								labels[wordIdx] = WordLabel.get("I-"+entitySpan.label.form);
							}
						} else {
							if(ignoreOverlap){
								throw new RuntimeException("Ignoring overlap but overlapping entity still exists.");
							}
							if(wordIdx > 0 && labels[wordIdx-1] != null && (labels[wordIdx-1].form.startsWith("BH") || labels[wordIdx-1].form.startsWith("IH")) && !labels[wordIdx].form.startsWith("BH")){
								labels[wordIdx] = WordLabel.get("IH-"+entitySpan.label.form);
							} else {
								labels[wordIdx] = WordLabel.get("BH-"+entitySpan.label.form);
							}
						}
					}
					if(wordSpan.end >= span.end){
						if(wordSpan.end > span.end){
							// Output span ends at the middle of this word span
						}
						break;
					}
				}
				if(useBILOU){
					if(labels[wordIdx].form.startsWith("B")){
						labels[wordIdx] = WordLabel.get("U"+labels[wordIdx].form.substring(1));
					} else if(labels[wordIdx].form.startsWith("I")){
						labels[wordIdx] = WordLabel.get("L"+labels[wordIdx].form.substring(1));
					} else {
						throw new RuntimeException("Error: last word must have label starting with 'B' or 'I', not "+labels[wordIdx].form);
					}
				}
			}
		}
		for(WordLabel label: labels){
			if(label == null){
				result.add(WordLabel.get("O"));
			} else {
				result.add(label);
			}
		}
		return result;
	}
	
	public static enum LabelInterpretation {
		HEAD_ALL(99, false),
		HEAD_LIMITED(2, true),
		HEAD_BODY_LIMITED(2, false),
		NESTED_ONLY(1, false),
		;
		
		public int bodyLimit;
		public boolean combineAllBodies;
		
		private LabelInterpretation(int bodyLimit, boolean combineAllBodies){
			this.bodyLimit = bodyLimit;
			this.combineAllBodies = combineAllBodies;
		}
	}
	
	public static enum PartType {
		HEAD,
		BODY,
		CONT,
	}
	
	private static class EntityPart implements Comparable<EntityPart>, Serializable {
		
		private static final long serialVersionUID = 9032380818030127847L;
		public PartType type;
		public Span span;
		public SpanLabel spanLabel;
		public boolean canStart;
		public boolean canEnd;
		
		public EntityPart(Span span, SpanLabel spanLabel, PartType type, boolean canStart, boolean canEnd){
			this.span = span;
			this.spanLabel = spanLabel;
			this.type = type;
			this.canStart = canStart;
			this.canEnd = canEnd;
		}
		
		public int compareTo(EntityPart e){
			return span.compareTo(e.span);
		}
		
		public String toString(){
			return String.format("%s: %s (%s)%s", spanLabel, span, type.name(), canStart ? "✓" : "x");
		}
	}
	
	private static boolean isNewPart(String curLabel, String nextLabel){
		 if(nextLabel.matches("^[BOU].*")){
			 return true;
		 }
		 if(!curLabel.substring(1).equals(nextLabel.substring(1))){
			 return true;
		 }
		 if(curLabel.matches("^[LU].*")){
			 return true;
		 }
		 return false;
	}
	
	private static final class EntityCandidate {
		public boolean isStillContiguous;
		public boolean shouldBeContiguous;
		public boolean shouldBeDiscontiguous;
		public List<EntityPart> parts;
		public int lastPos;
		public boolean canEnd;
		
		public static final EntityCandidate EMPTY = new EntityCandidate();
		
		private EntityCandidate(){}
		
		public EntityCandidate(EntityPart firstPart){
			this.parts = new ArrayList<EntityPart>();
			this.parts.add(firstPart);
			if(firstPart.type == PartType.CONT){
				shouldBeContiguous = true;
			} else {
				shouldBeContiguous = false;
			}
			isStillContiguous = true;
			if(firstPart.type == PartType.BODY){
				shouldBeDiscontiguous = true;
			} else {
				shouldBeDiscontiguous = false;
			}
			if(shouldBeDiscontiguous && isStillContiguous){
				canEnd = false;
			} else {
				canEnd = true;
			}
			lastPos = firstPart.span.end;
		}
		
		public EntityCandidate(EntityCandidate candidate){
			this.parts = new ArrayList<EntityPart>(candidate.parts);
			this.isStillContiguous = candidate.isStillContiguous;
			this.shouldBeContiguous = candidate.shouldBeContiguous;
			this.shouldBeDiscontiguous = candidate.shouldBeDiscontiguous;
			this.lastPos = candidate.lastPos;
			this.canEnd = candidate.canEnd;
		}
		
		public boolean compatibleWith(EntityPart part){
			if(part.type == PartType.CONT){
				if(part.canStart || shouldBeDiscontiguous){
					return false;
				}
				if(isStillContiguous && lastPos == part.span.start){
					return true;
				}
				return false;
			} else if(part.type == PartType.BODY){
				if(shouldBeContiguous){
					return false;
				}
				if(part.canStart){
					if(lastPos == part.span.start){
						return false;
					} else {
						return true;
					}
				} else {
					if(lastPos != part.span.start){
						return false;
					} else {
						return true;
					}
				}
			} else { // Head
				if(shouldBeContiguous && lastPos != part.span.start){
					return false;
				} else {
					return true;
				}
			}
		}
		
		public EntityCandidate add(EntityPart part){
			EntityCandidate result = new EntityCandidate(this);
			if(result.lastPos != part.span.start){
				result.isStillContiguous = false;
			}
			if(!shouldBeDiscontiguous && !shouldBeContiguous){
				if(part.type == PartType.CONT){
					result.shouldBeContiguous = true;
				} else if (part.type == PartType.BODY){
					result.shouldBeDiscontiguous = true;
				}
			}
			result.parts.add(part);
			result.lastPos = part.span.end;
			if(result.isStillContiguous && result.shouldBeDiscontiguous){
				result.canEnd = false;
			} else {
				result.canEnd = true;
			}
			return result;
		}
	}
	
	public static List<EntitySpan> labelsToSpans(List<WordLabel> labelsList, List<Span> wordSpansList, String input, LabelInterpretation labelInterpretation){
		Set<EntitySpan> result = new TreeSet<EntitySpan>();
		WordLabel[] labels = labelsList.toArray(new WordLabel[labelsList.size()]);
		Span[] wordSpans = wordSpansList.toArray(new Span[wordSpansList.size()]);
		List<EntityPart> partsList = new ArrayList<EntityPart>();
		int startIdx = 0;
		Map<SpanLabel, Integer> unmatchedBodyCount = new HashMap<SpanLabel, Integer>();
		for(SpanLabel label: SpanLabel.LABELS.values()){
			unmatchedBodyCount.put(label, 0);
		}
		String prevForm = "--";
		for(int i=0; i<labels.length; i++){
			String form = labels[i].form;
			if(form.equals("O")){
				prevForm = form;
				continue;
			}
			if(form.startsWith("B") || form.startsWith("U") || prevForm.equals("O") || prevForm.startsWith("L") || prevForm.startsWith("U") || !prevForm.substring(1).equals(form.substring(1))){
				startIdx = i;
			}
			if(i==labels.length-1 || isNewPart(form, labels[i+1].form)){
				int formDashIdx = form.indexOf('-');
				SpanLabel spanLabel = SpanLabel.get(form.substring(formDashIdx+1));
				PartType partType = null;
				if(form.charAt(1) == 'D'){
					partType = PartType.BODY;
					unmatchedBodyCount.put(spanLabel, unmatchedBodyCount.get(spanLabel)+1);
				} else if(form.charAt(1) == 'H'){
					partType = PartType.HEAD;
				} else {
					partType = PartType.CONT;
				}
				boolean canStart = labels[startIdx].form.startsWith("B") || labels[startIdx].form.startsWith("U") || startIdx == 0 || labels[startIdx-1].form.equals("O") || labels[startIdx-1].form.matches("[LU]-.*") || !labels[startIdx-1].form.split("-")[1].equals(labels[startIdx].form.split("-")[1]);
				boolean canEnd = labels[i].form.startsWith("L") || labels[i].form.startsWith("U") || i == labels.length-1 || labels[i+1].form.equals("O") || labels[i+1].form.matches("[BU]-.*") || !labels[i].form.split("-")[1].equals(labels[i+1].form.split("-")[1]);
				partsList.add(new EntityPart(new Span(startIdx, i+1), spanLabel, partType, canStart, canEnd));
			}
			prevForm = form;
		}
//		System.out.println(input);
//		System.out.println(labelsList);
//		System.out.println(partsList);
		EntityPart[] parts = partsList.toArray(new EntityPart[partsList.size()]);
		if(labelInterpretation == LabelInterpretation.HEAD_ALL){
			Queue<EntityCandidate> candidates = new LinkedList<EntityCandidate>();
			candidates.offer(EntityCandidate.EMPTY);
			EntityCandidate candidate;
			for(int i=0; i<parts.length; i++){
				EntityPart curPart = parts[i];
				while((candidate = candidates.poll()) != EntityCandidate.EMPTY){
					candidates.offer(candidate);
					if(candidate.compatibleWith(curPart)){
						candidates.offer(candidate.add(curPart));
					}
				}
				if(curPart.canStart){
					candidates.offer(new EntityCandidate(curPart));
				}
				candidates.offer(candidate);
			}
			while((candidate = candidates.poll()) != EntityCandidate.EMPTY){
				if(candidate.canEnd){
					result.add(partsToSpan(wordSpans, candidate.parts.toArray(new EntityPart[0])));
				}
			}
		} else if(labelInterpretation == LabelInterpretation.HEAD_BODY_LIMITED){
			EntityPart lastHead = null;
			// Process the heads
			for(int i=0; i<parts.length; i++){
				EntityPart part = parts[i];
				if(part == null) continue;
				if(part.type == PartType.HEAD){
					lastHead = part;
					int bodyCount = 0;
					// Match contiguous before
					if(i>0 && parts[i-1] != null && parts[i-1].type == PartType.CONT && parts[i-1].span.end == part.span.start && parts[i-1].spanLabel == part.spanLabel){
						bodyCount++;
						result.add(partsToSpan(wordSpans, parts[i-1], part));
						parts[i-1] = null;
					}
					// Match contiguous after
					if(i<parts.length-1 && parts[i+1] != null && parts[i+1].type == PartType.CONT && parts[i+1].span.start == part.span.end && parts[i+1].spanLabel == part.spanLabel){
						bodyCount++;
						result.add(partsToSpan(wordSpans, part, parts[i+1]));
						parts[i+1] = null;
					}
					int leftBodyCount = 0;
					for(int j=i-1; j>=0; j--){
						if(parts[j] != null && parts[j].type == PartType.BODY && parts[j].spanLabel == part.spanLabel){
							leftBodyCount++;
						}
					}
					int rightBodyCount = 0;
					for(int j=i+1; j<parts.length; j++){
						if(parts[j] != null && parts[j].type == PartType.BODY && parts[j].spanLabel == part.spanLabel){
							rightBodyCount++;
						}
					}
					// Match lone body before or after
					if(leftBodyCount == 1){
						for(int j=i-1; j>=0; j--){
							EntityPart oPart = parts[j];
							if(oPart != null && oPart.type == PartType.BODY && oPart.spanLabel == part.spanLabel){
								bodyCount++;
								unmatchedBodyCount.put(part.spanLabel, unmatchedBodyCount.get(part.spanLabel)-1);
								leftBodyCount--;
								result.add(partsToSpan(wordSpans, oPart, part));
								// Prevent this body to be combined with other heads
								parts[j] = null;
								break;
							}
						}
					}
					if(rightBodyCount == 1){
						for(int j=i+1; j<parts.length; j++){
							EntityPart oPart = parts[j];
							if(oPart != null && oPart.type == PartType.BODY && oPart.spanLabel == part.spanLabel){
								bodyCount++;
								unmatchedBodyCount.put(part.spanLabel, unmatchedBodyCount.get(part.spanLabel)-1);
								rightBodyCount--;
								result.add(partsToSpan(wordSpans, part, oPart));
								// Prevent this body to be combined with other heads
								parts[j] = null;
								break;
							}
						}
					}
					// Match other bodies, starting from closest left
					for(int j=i-1; j>=0; j--){
						if(bodyCount < labelInterpretation.bodyLimit || unmatchedBodyCount.get(part.spanLabel) == 1 || leftBodyCount == 1){
							EntityPart oPart = parts[j];
							if(oPart == null) continue;
							if(oPart.type == PartType.BODY && oPart.spanLabel == part.spanLabel){
								bodyCount++;
								unmatchedBodyCount.put(part.spanLabel, unmatchedBodyCount.get(part.spanLabel)-1);
								leftBodyCount--;
								result.add(partsToSpan(wordSpans, oPart, part));
								// Prevent this body to be combined with other heads
								parts[j] = null;
							}
						} else {
							break;
						}
					}
					// Match other bodies to the right
					for(int j=i+1; j<parts.length; j++){
						if(bodyCount < labelInterpretation.bodyLimit || unmatchedBodyCount.get(part.spanLabel) == 1 || rightBodyCount == 1){
							EntityPart oPart = parts[j];
							if(oPart == null) continue;
							if(oPart.type == PartType.BODY && oPart.spanLabel == part.spanLabel){
								bodyCount++;
								unmatchedBodyCount.put(part.spanLabel, unmatchedBodyCount.get(part.spanLabel)-1);
								rightBodyCount--;
								result.add(partsToSpan(wordSpans, oPart, part));
								// Prevent this body to be combined with other heads
								parts[j] = null;
							}
						} else {
							break;
						}
					}
					parts[i] = null;
				}
			}
			Map<SpanLabel, List<EntityPart>> bodyMap = new HashMap<SpanLabel, List<EntityPart>>();
			// Process the contiguous entities
			for(int i=0; i<parts.length; i++){
				EntityPart part = parts[i];
				if(part == null) continue;
				if(part.type == PartType.CONT){
					result.add(partsToSpan(wordSpans, part));
				} else if(part.type == PartType.BODY){
					List<EntityPart> bodyList = bodyMap.get(part.spanLabel);
					if(bodyList == null){
						bodyList = new ArrayList<EntityPart>();
						bodyMap.put(part.spanLabel, bodyList);
					}
					bodyList.add(part);
				}
			}
			// Process the remaining bodies
			for(List<EntityPart> bodies: bodyMap.values()){
				while(bodies.size() > 0){
					if(labelInterpretation.combineAllBodies){
						result.add(partsToSpan(wordSpans, bodies.toArray(new EntityPart[bodies.size()])));
						break;
					} else {
						if(bodies.size() == 3){
							result.add(partsToSpan(wordSpans, bodies.remove(0), bodies.remove(0), bodies.remove(0)));
						} else if (bodies.size() >= 2){
							result.add(partsToSpan(wordSpans, bodies.remove(0), bodies.remove(0)));
						} else {
							if(lastHead != null){
								result.add(partsToSpan(wordSpans, bodies.remove(0), lastHead));
							} else {
								result.add(partsToSpan(wordSpans, bodies.remove(0)));
							}
						}
					}
				}
			}
		} else if (labelInterpretation == LabelInterpretation.NESTED_ONLY){
			Stack<Stack<EntityPart>> stack = new Stack<Stack<EntityPart>>();
			for(int i=0; i<partsList.size(); i++){
				EntityPart curPart = partsList.get(i);
				int stackSizeLimit = stack.size();
//				if(stack.size() > 0 && stack.peek().peek().span.end < curPart.span.start
//						|| stack.size() == 0){
//					curPart.canStart = true;
//				}
				if(curPart.canStart){
					if(stack.size() > 0 &&
							(stack.peek().peek().span.end < curPart.span.start
									|| curPart.type == PartType.CONT)){
						// Separate components, complete the previous ones
						stackSizeLimit = 0;
					}
				// Now the case that current part cannot start an entity
				// Therefore it must be the case that the stack is not empty
				// and that it must be connected to current part
				} else if(curPart.type == PartType.CONT){
					// All previous parts must be completed except one
					// which is to be connected with current part.
					stackSizeLimit = 1;
				} else {
					// At least one part is completed, we don't know how many
					// Assume the least, only one part is completed.
					stackSizeLimit -= 1;
					if(stack.peek().peek().span.end == curPart.span.start){
						stackSizeLimit = Math.max(1, stackSizeLimit);
					}
				}
				if(stackSizeLimit < 0){
					// Should not happen if the model is good
					stackSizeLimit = 0;
				}
				while(stack.size() > stackSizeLimit){
					result.add(partsToSpan(wordSpans, stack.pop().toArray(new EntityPart[0])));
				}
				for(Stack<EntityPart> substack: stack){
					substack.push(curPart);
				}
				if(curPart.canStart){
					// If a part can start an entity, then we assume that there is an entity starting
					Stack<EntityPart> newStack = new Stack<EntityPart>();
					newStack.add(curPart);
					stack.push(newStack);
				}
				if(curPart.canEnd){
					result.add(partsToSpan(wordSpans, stack.peek().toArray(new EntityPart[0])));
					if(curPart.type == PartType.CONT){
						stack.pop();
					}
				}
			}
			while(stack.size() > 0){
				result.add(partsToSpan(wordSpans, stack.pop().toArray(new EntityPart[0])));
			}
		} else {
			throw new RuntimeException("Unhandled label interpretation: "+labelInterpretation.name());
		}
		return new ArrayList<EntitySpan>(result);
	}
	
	private static EntitySpan partsToSpan(Span[] wordSpans, EntityPart... parts){
		List<Span> spanList = new ArrayList<Span>();
		List<EntityPart> partList = new ArrayList<EntityPart>(Arrays.asList(parts));
		Collections.sort(partList);
		for(int i=partList.size()-1; i>0; i--){
			EntityPart curPart = partList.get(i);
			EntityPart prevPart = partList.get(i-1);
			if(prevPart.span.end == curPart.span.start){
				EntityPart newPart = new EntityPart(new Span(prevPart.span.start, curPart.span.end), prevPart.spanLabel, PartType.CONT, prevPart.canStart, curPart.canEnd);
				partList.set(i-1, newPart);
				partList.remove(i);
			}
		}
		for(EntityPart part: partList){
			spanList.add(new Span(wordSpans[part.span.start].start, wordSpans[part.span.end-1].end));
		}
		Collections.sort(spanList);
		return new EntitySpan(null, parts[0].spanLabel, spanList.toArray(new Span[spanList.size()]));
	}
	
	/**
	 * Return the CoNLL format of the specified input tokens, output tokens, and optional prediction tokens.
	 * @param inputTokenized
	 * @param outputTokenized
	 * @param predictionTokenized
	 * @return
	 */
	public static String toCoNLLString(List<CoreLabel> inputTokenized, List<WordLabel> outputTokenized, List<WordLabel> predictionTokenized, String[]... additionalFeatures){
		StringBuilder builder = new StringBuilder();
		for(int i=0; i<inputTokenized.size(); i++){
			builder.append(inputTokenized.get(i).value());
			for(int additionalFeatureIndex=0; additionalFeatureIndex<additionalFeatures.length; additionalFeatureIndex++){
				builder.append(" "+additionalFeatures[additionalFeatureIndex][i]);
			}
			builder.append(" "+outputTokenized.get(i).form);
			if(predictionTokenized != null){
				builder.append(" "+predictionTokenized.get(i).form);
			}
			builder.append("\n");
		}
		return builder.toString();
	}
	
	public static void setupFeatures(Class<? extends IFeatureType> featureTypeClass, String[] features){
		try {
			Method valueOf = featureTypeClass.getMethod("valueOf", String.class);
			IFeatureType[] featureTypes = (IFeatureType[])featureTypeClass.getMethod("values").invoke(null);
			if(features != null && features.length > 0){
				for(IFeatureType feature: featureTypes){
					feature.disable();
				}
				for(String feature: features){
					((IFeatureType)valueOf.invoke(null, feature.toUpperCase())).enable();
				}
			}
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	public static int[] listToArray(List<Integer> list){
		for(int i=list.size()-1; i>=0; i--){
			if(list.get(i) == -1){
				list.remove(i);
			}
		}
		int[] result = new int[list.size()];
		for(int i=0; i<list.size(); i++){
			result[i] = list.get(i);
		}
		return result;
	}
	
	/**
	 * Split a CoreLabel into multiple CoreLabel by splitting at the given split indices
	 * @param label
	 * @param splitIndices
	 * @return
	 */
	public static List<CoreLabel> split(CoreLabel label, int... splitIndices){
		List<CoreLabel> result = new ArrayList<CoreLabel>();
		for(int i=0; i<splitIndices.length; i++){
			if(splitIndices[i] <= 0) continue;
			int begin = label.beginPosition();
			CoreLabel first = new CoreLabel(label);
			CoreLabel second = new CoreLabel(label);
			first.setEndPosition(begin+splitIndices[i]);
			first.setAfter("");
			first.setValue(label.value().substring(0, splitIndices[i]));
			first.setWord(label.word().substring(0, splitIndices[i]));
			first.setOriginalText(label.originalText().substring(0,  splitIndices[i]));
			second.setBeginPosition(begin+splitIndices[i]);
			second.setBefore("");
			second.setValue(label.value().substring(splitIndices[i]));
			second.setWord(label.word().substring(splitIndices[i]));
			second.setOriginalText(label.originalText().substring(splitIndices[i]));
			if(first.value().length() > 0){
				result.add(first);
			}
			label = second;
		}
		if(label.value().length() > 0){
			result.add(label);
		}
		return result;
	}
	
	/**
	 * Merge an ordered list of CoreLabel into one.<br>
	 * Note that the word and value of the merged CoreLabel will be constructed from the original text.
	 * @param labels
	 * @return
	 */
	public static CoreLabel merge(CoreLabel... labels){
		if(labels.length == 0){
			return new CoreLabel();
		}
		CoreLabel result = new CoreLabel(labels[0]);
		for(int i=1; i<labels.length; i++){
			CoreLabel label = labels[i];
			String merged = result.originalText()+result.after()+label.originalText();
			result.setValue(merged);
			result.setWord(merged);
			result.setOriginalText(merged);
			result.setEndPosition(label.endPosition());
			result.setAfter(label.after());
		}
		return result;
	}

	private static enum Argument{
//		INCLUDE_PERCENTILE(1,
//				"The list of percentiles to be included in the statistics",
//				"includePercentile",
//				"<comma-separated values>"),
//		LABELS(1,
//				"The list of labels to be included in calculation",
//				"labels",
//				"<comma-separated labels>"),
		ANNOTATION_FILE(1,
				"An annotation file",
				"annPath",
				"<annotationFile>"),
		ANNOTATION_DIR(1,
				"The directory containing list of annotation files",
				"annDir",
				"<annotationDir>"),
		SOURCE_DIR(1,
				"Directory containing the source document",
				"sourceDir",
				"<sourceDir>"),
		DATA_IS_RAW(1,
				"The data is in raw format, requiring two files, one the text, another the annotation.",
				"dataIsRaw",
				"(true|false)"),
		OUTPUT_DIR(1,
				"Directory to output the tokenized files",
				"outputDir",
				"<outputDir>"),
		SPLITTER(1,
				"The sentence splitter to be used. Default to stanford",
				"splitter",
				"[stanford]"),
		TOKENIZER(1,
				"The tokenizer to be used to tokenize. Default to regex",
				"tokenizer",
				"[regex|whitespace]"),
		UMLS_DIR(1,
				"Directory containing UMLS files",
				"umlsDir",
				"<umlsDir>"),
		USE_MORE_LABEL_TYPES(0,
				"Whether to use more label types based on the semantic category",
				"useMoreLabelTypes"),
		HIERARCHY_LEVEL(1,
				"The hierarchy in semantic category used for deciding labels",
				"hierarchyLevel",
				"<n>"),
		IGNORE_NULL_LABELS(0,
				"Whether to exclude the CUI-less entities",
				"ignoreNullLabels"),
		RANDOM_SPLIT_LABELS(1,
				"Whether to randomly split labels into n labels",
				"randomSplitLabels",
				"<n>"),
		NO_NORMALIZE(0,
				"Do not normalize the tokens",
				"no_normalize"
				),
		INSTANCE_FILTER(1,
				"How to do instance filtering. Default to all_with_entities",
				"instanceFilter",
				"[all_instances|all_with_entities|only_contiguous|contain_discontiguous|no_discontiguous]"),
		PRINT_NOT_INCLUDED(0,
				"Print those entities not included in any sentences",
				"print_not_included"
				),
		NO_EXAMPLES(0,
				"Do not print examples",
				"no_examples"
				),
		NO_SECTIONS(0,
				"Do not print section names",
				"no_sections"
				),
		HELP(0,
				"Print this help message",
				"h,help"),
		;
		
		final private int numArgs;
		final private String[] argNames;
		final private String[] names;
		final private String help;
		private Argument(int numArgs, String help, String names, String... argNames){
			this.numArgs = numArgs;
			this.argNames = argNames;
			this.names = names.split(",");
			this.help = help;
		}
		
		/**
		 * Return the Argument which has the specified name
		 * @param name
		 * @return
		 */
		public static Argument argWithName(String name){
			for(Argument argument: Argument.values()){
				for(String argName: argument.names){
					if(argName.equals(name)){
						return argument;
					}
				}
			}
			throw new IllegalArgumentException("Unrecognized argument: "+name);
		}
		
		/**
		 * Print help message
		 */
		private static void printHelp(){
			StringBuilder result = new StringBuilder();
			result.append("Usage:\n");
			result.append("CTUtil (tokenize|printOne|getStats) [options]\n\n");
			result.append("Options:\n");
			for(Argument argument: Argument.values()){
				result.append("-"+StringUtils.join(argument.names, " -"));
				result.append(" "+StringUtils.join(argument.argNames, " "));
				result.append("\n");
				if(argument.help != null && argument.help.length() > 0){
					result.append("\t"+argument.help.replaceAll("\n","\n\t")+"\n");
				}
			}
			System.out.println(result.toString());
		}
	}
	
	private static int getWordCount(EntitySpan entity, OEInstance instance){
		int sum = 0;
		for(Span span: entity.spans){
			sum += getWordCount(span, instance);
		}
		return sum;
	}
	
	private static int getWordCount(Span span, OEInstance instance){
		int start = -1;
		int end = -1;
		int idx = 0;
		for(CoreLabel word: instance.getInputTokenized()){
			if(word.endPosition() > span.start && start == -1){
				start = idx;
			}
			if(word.endPosition() <= span.end && start != -1){
				end = idx+1;
			}
			idx += 1;
		}
		return end-start;
	}
	
	private static <T extends Comparable<T>> List<T> sorted(Set<? extends T> set){
		ArrayList<T> result = new ArrayList<T>();
		result.addAll(set);
		result.sort(new Comparator<T>(){

			@Override
			public int compare(T o1, T o2) {
				return o1.compareTo(o2);
			}
			
		});
		return result;
	}
	
	public static void main(String[] args) throws Exception{
		String annPath = null;
		String annDir = null;
		String sourceDir = null;
		String outputDir = null;
		String umlsDir = null;
		TokenizerMethod tokenizerMethod = TokenizerMethod.REGEX;
		SplitterMethod splitterMethod = SplitterMethod.STANFORD;
		InstanceFilter instanceFilter = InstanceFilter.ALL_WITH_ENTITIES;
		boolean normalize = true;
		
		boolean doTokenize = false;
		boolean printOne = false;
		boolean getStats = false;
		boolean printNotIncluded = false;
		boolean printExamples = true;
		boolean printSectionNames = true;
		boolean ignoreNullLabels = false;
		boolean useMoreLabelTypes = false;
		boolean dataIsRaw = true;
		int hierarchyLevel = 0;
		boolean randomSplitLabels = false;
		int numRandomLabels = 0;
		if(args.length == 0){
			Argument.printHelp();
			System.exit(0);
		}
		String command = args[0];
		switch(command){
		case "tokenize":
			doTokenize = true;
			break;
		case "printOne":
			printOne = true;
			break;
		case "getStats":
			getStats = true;
			break;
		default:
			throw new IllegalArgumentException(
					"Unrecognized command: \""+command+"\".\n"
					+ "Only recognize these:\n"
					+ "* tokenize: to tokenize source files in the path given by -sourceDir into -outputDir\n"
					+ "* printOne: print the annotations from the annotation file given by -annPath\n"
					+ "* getStats: print the statistics of the data given by -sourceDir and -annDir");
		}
		int argIndex = 1;
		while(argIndex < args.length){
			String arg = args[argIndex];
			if(arg.length() > 0 && arg.charAt(0) == '-'){
				Argument argument = Argument.argWithName(args[argIndex].substring(1));
				switch(argument){
				case ANNOTATION_FILE:
					annPath = args[argIndex+1];
					break;
				case ANNOTATION_DIR:
					annDir = args[argIndex+1];
					break;
				case SOURCE_DIR:
					sourceDir = args[argIndex+1];
					break;
				case OUTPUT_DIR:
					outputDir = args[argIndex+1];
					break;
				case DATA_IS_RAW:
					dataIsRaw = Boolean.parseBoolean(args[argIndex+1]);
					break;
				case SPLITTER:
					splitterMethod = SplitterMethod.valueOf(args[argIndex+1].toUpperCase());
					break;
				case TOKENIZER:
					tokenizerMethod = TokenizerMethod.valueOf(args[argIndex+1].toUpperCase());
					break;
				case NO_NORMALIZE:
					normalize = false;
					break;
				case INSTANCE_FILTER:
					instanceFilter = InstanceFilter.valueOf(args[argIndex+1].toUpperCase());
					break;
				case UMLS_DIR:
					umlsDir = args[argIndex+1];
					break;
				case USE_MORE_LABEL_TYPES:
					useMoreLabelTypes = true;
					break;
				case HIERARCHY_LEVEL:
					hierarchyLevel = Integer.parseInt(args[argIndex+1]);
					break;
				case IGNORE_NULL_LABELS:
					ignoreNullLabels = true;
					break;
				case RANDOM_SPLIT_LABELS:
					randomSplitLabels = true;
					numRandomLabels = Integer.parseInt(args[argIndex+1]);
					break;
				case PRINT_NOT_INCLUDED:
					printNotIncluded = true;
					break;
				case NO_EXAMPLES:
					printExamples = false;
					break;
				case NO_SECTIONS:
					printSectionNames = false;
					break;
				case HELP:
					Argument.printHelp();
					System.exit(0);
				}
				argIndex += argument.numArgs+1;
			} else {
				throw new IllegalArgumentException("Error while parsing: "+arg);
			}
		}
		Map<String, UMLSCategory> cuiToCat = new HashMap<String, UMLSCategory>();
		Scanner input;
		if(useMoreLabelTypes){
			input = new Scanner(new File(umlsDir+File.separator+"MRSTY.RRF"));
			while(input.hasNextLine()){
				String[] tokens = input.nextLine().split("\\|");
				cuiToCat.put(tokens[0], new UMLSCategory(tokens[2], tokens[3]));
			}
			input.close();
		} else {
			cuiToCat = null;
		}
		if(doTokenize){
			if(sourceDir == null || outputDir == null){
				System.err.println("Source directory (-sourceDir) or output directory (-outputDir) not specified");
				System.exit(0);
			}
			List<OEDocument> documents = new ArrayList<OEDocument>();
			for(File sourceFile: new File(sourceDir).listFiles()){
				if(sourceFile.getName().startsWith(".")){
					continue;
				}
				documents.add(readSource(sourceFile.getAbsolutePath()));
			}
			for(OEDocument document: documents){
				new File(outputDir).mkdirs();
				PrintStream output = new PrintStream(outputDir+File.separator+new File(document.filename).getName());
				OEInstance[] instances = getInstances(Arrays.asList(document), false, splitterMethod, tokenizerMethod, instanceFilter);
				for(OEInstance instance: instances){
					output.println(StringUtils.join(instance.getInputTokenized(tokenizerMethod, true, normalize), " "));
				}
				output.close();
			}
		}
		if(printOne){
			if(annPath == null || sourceDir == null){
				System.err.println("Annotation file (-annPath) or source directory (-sourceDir) is not specified");
				System.exit(0);
			}
			OEDocument document = readAnnotatedData(sourceDir, annPath, cuiToCat, hierarchyLevel, ignoreNullLabels, randomSplitLabels, numRandomLabels);
			document.printAnnotations();
		}
		if(getStats){
			if(sourceDir == null || annDir == null){
				System.err.println("Source directory (-sourceDir) or annotation directory (-annDir) not specified");
				System.exit(0);
			}
			List<OEDocument> sourceDocs = new ArrayList<OEDocument>();
			Map<OEDocument, Set<EntitySpan>> docToEntities = new HashMap<OEDocument, Set<EntitySpan>>();
			int totalEntitiesActual = 0;
			int totalDiscontiguousActual = 0;
			int totalDiscontiguousMoreActual = 0;
			Set<SpanLabel> distinctLabels = new HashSet<SpanLabel>();
			OEInstance[] instances;
			if(dataIsRaw){
				for(File annotationPath: new File(annDir).listFiles()){
					OEDocument sourceDoc = readAnnotatedData(sourceDir, annotationPath.getAbsolutePath(), cuiToCat, hierarchyLevel, ignoreNullLabels, randomSplitLabels, numRandomLabels);
					sourceDocs.add(sourceDoc);
					totalEntitiesActual += sourceDoc.annotations.size();
					for(EntitySpan entity: sourceDoc.annotations){
						if(entity.spans.length > 1){
							totalDiscontiguousActual += 1;
						}
						if(entity.spans.length > 2){
							totalDiscontiguousMoreActual += 1;
						}
						distinctLabels.add(entity.label);
					}
					docToEntities.put(sourceDoc, new TreeSet<EntitySpan>(sourceDoc.annotations));
				}
				instances = getInstances(sourceDocs, true, splitterMethod, tokenizerMethod, instanceFilter);
			} else {
				instances = readFromFormattedData(sourceDir, true, true, false);
				for(OEInstance instance: instances){
					for(EntitySpan entity: instance.output){
						if(entity.spans.length > 1){
							totalDiscontiguousActual += 1;
						}
						if(entity.spans.length > 2){
							totalDiscontiguousMoreActual += 1;
						}
						distinctLabels.add(entity.label);
					}
				}
			}
			int totalInstances = instances.length;
			int totalEntities = 0;
			int totalDiscontiguous = 0;
			int totalDiscontiguousMore = 0;
			int maxInstanceLength = 0;
			int numWords = 0;
			OEInstance exInstanceLength = null;
			int maxEntitiesPerInstance = 0;
			OEInstance exEntitiesPerInstance = null;
			int maxDiscontiguousPerInstance = 0;
			OEInstance exDiscontiguousPerInstance = null;
			Set<String> sectionNames = new HashSet<String>();
			
			Set<String> sourceDocs1 = new HashSet<String>();
			int overlapCount = 0;
			int multiTypeOverlapCount = 0;
			int overlapDiscCount = 0;
			int multiTypeOverlapDiscCount = 0;
			int totalInstancesWithOverlaps = 0;
			int totalInstancesWithSameTypeOverlaps = 0;
			int numEntitiesOverlapping = 0;
			int numEntitiesOverlappingDiffType = 0;
			int numEntitiesOverlappingSameType = 0;
			int numEntitiesNested = 0;
			int numEntitiesNestedDiffType = 0;
			int numEntitiesNestedSameType = 0;
			int numEntitiesStrictlyNested = 0;
			int numEntitiesStrictlyNestedDiffType = 0;
			int numEntitiesStrictlyNestedSameType = 0;
			boolean hasOverlap = false;
			boolean hasSameTypeOverlap = false;
			for(OEInstance instance: instances){
//				boolean hasDiffTypeOverlap = false;
				for(EntitySpan mention: instance.getOutput()){
					if(ignoreNullLabels && mention.label == SpanLabel.get("Disease_Disorder")){
						continue;
					}
					if(mention.spans.length > 1){
						try{
							sourceDocs1.add(instance.sourceDoc.filename);
						} catch (NullPointerException e){
							
						}
					}
					boolean isOverlapping = false;
					boolean isOverlappingDiffType = false;
					boolean isOverlappingSameType = false;
					boolean isNested = false;
					boolean isNestedDiffType = false;
					boolean isNestedSameType = false;
					boolean isStrictlyNested = false;
					boolean isStrictlyNestedDiffType = false;
					boolean isStrictlyNestedSameType = false;
					for(EntitySpan mention2: instance.getOutput()){
						if(mention == mention2 || (ignoreNullLabels && mention2.label == SpanLabel.get("Disease_Disorder"))){
							continue;
						}
						if(mention.overlapsWith(mention2)){
							hasOverlap = true;
							isOverlapping = true;
							if(mention.start() >= mention2.start() && mention.end() <= mention2.end()){
								isNested = true;
								if(mention.label == mention2.label){
									isNestedSameType = true;
								} else {
									isNestedDiffType = true;
								}
								if(mention.start() > mention2.start() && mention.end() < mention2.end()){
									isStrictlyNested = true;
									if(mention.label == mention2.label){
										isStrictlyNestedSameType = true;
									} else {
										isStrictlyNestedDiffType = true;
									}
								}
							}
							overlapCount += 1;
							if(mention.label != mention2.label){
//								hasDiffTypeOverlap = true;
								multiTypeOverlapCount += 1;
								isOverlappingDiffType = true;
							} else {
								hasSameTypeOverlap = true;
								isOverlappingSameType = true;
							}
							if(mention.spans.length > 1 || mention2.spans.length > 1){
								overlapDiscCount += 1;
								if(mention.label != mention2.label){
									multiTypeOverlapDiscCount += 1;
								}
							}
						}
					}
					if(isOverlapping){
						numEntitiesOverlapping += 1;
					}
					if(isOverlappingDiffType){
						numEntitiesOverlappingDiffType += 1;
					}
					if(isOverlappingSameType){
						numEntitiesOverlappingSameType += 1;
					}
					if(isNested){
						numEntitiesNested += 1;
					}
					if(isNestedDiffType){
						numEntitiesNestedDiffType += 1;
					}
					if(isNestedSameType){
						numEntitiesNestedSameType += 1;
					}
					if(isStrictlyNested){
						numEntitiesStrictlyNested += 1;
					}
					if(isStrictlyNestedDiffType){
						numEntitiesStrictlyNestedDiffType += 1;
					}
					if(isStrictlyNestedSameType){
						numEntitiesStrictlyNestedSameType += 1;
					}
				}
				if(hasOverlap){
					totalInstancesWithOverlaps += 1;
				}
				if(hasSameTypeOverlap){
//					if(hasDiffTypeOverlap){
//						System.out.println(instance);
//						System.out.println();
//					}
					totalInstancesWithSameTypeOverlaps += 1;
				}
				hasOverlap = false;
				hasSameTypeOverlap = false;
			}
			overlapCount /= 2;
			multiTypeOverlapCount /= 2;
			overlapDiscCount /= 2;
			multiTypeOverlapDiscCount /= 2;
			System.out.println("Number of labels: "+distinctLabels.size()+" ("+distinctLabels+")");
			System.out.println("Overlap count: "+overlapCount);
			System.out.println("Diff-type Overlap count: "+multiTypeOverlapCount);
			System.out.println("Same-type Overlap count: "+(overlapCount-multiTypeOverlapCount));
			System.out.println("Overlap (with disc) count: "+overlapDiscCount);
			System.out.println("Diff-type Overlap (with disc) count: "+multiTypeOverlapDiscCount);
			
			System.out.println("Unique documents containing discontiguous mentions: "+sourceDocs1.size());
			
			Map<String, Integer> perTypeCount = new HashMap<String, Integer>();
			int maxEntityLength = 0;
			String longestEntity = null;
			int longEntityCount = 0;
			for(OEInstance instance: instances){
				int instanceLength = instance.getInputTokenized(tokenizerMethod, false, normalize).size();
				numWords += instanceLength;
				if(instanceLength > maxInstanceLength){
					maxInstanceLength = instanceLength;
					exInstanceLength = instance;
				}
				int numEntities = instance.output.size();
				if(numEntities > maxEntitiesPerInstance){
					maxEntitiesPerInstance = numEntities;
					exEntitiesPerInstance = instance;
				}
				totalEntities += numEntities;
				int numDiscontiguous = 0;
				int numDiscontiguousMore = 0;
				for(EntitySpan entity: instance.output){
					EntitySpan tmpEntity = new EntitySpan(entity);
					perTypeCount.put(entity.label.form, perTypeCount.getOrDefault(entity.label.form, 0)+1);
					int entityWordCount = getWordCount(entity, instance);
					if(entityWordCount > 6){
						longEntityCount += 1;
					}
					if(entityWordCount > maxEntityLength){
						maxEntityLength = entityWordCount;
						longestEntity = entity.getText(instance.input);
					}
					if(instance.sourceSpan != null){
						for(Span span: tmpEntity.spans){
							span.start += instance.sourceSpan.start;
							span.end += instance.sourceSpan.start;
						}
					}
					if(instance.sourceDoc != null){
						docToEntities.get(instance.sourceDoc).remove(tmpEntity);
					}
					if(entity.spans.length > 1){
						numDiscontiguous += 1;
					}
					if(entity.spans.length > 2){
						numDiscontiguousMore += 1;
					}
				}
				totalDiscontiguous += numDiscontiguous;
				totalDiscontiguousMore += numDiscontiguousMore;
				if(numDiscontiguous > maxDiscontiguousPerInstance){
					maxDiscontiguousPerInstance = numDiscontiguous;
					exDiscontiguousPerInstance = instance;
				}
				sectionNames.add(instance.sectionName);
			}
			if(printNotIncluded){
				for(OEDocument doc: docToEntities.keySet()){
					Set<EntitySpan> entities = docToEntities.get(doc);
					if(entities.size() > 0){
						System.out.println("Not included in any instance (from "+doc.filename+"):");
						for(EntitySpan entity: entities){
							int begin = entity.spans[0].start;
							int end = entity.spans[entity.spans.length-1].end;
							begin = Math.max(0, begin-20);
							end = Math.min(doc.text.length(), end+20);
							String context = doc.text.substring(begin, end);
							System.out.println(entity.toString(doc.text)+" (in \"" + context + "\")");
						}
						System.out.println();
					}
				}
			}
			System.out.println("Num instances: "+totalInstances);
			System.out.println("Num instances with overlaps: "+totalInstancesWithOverlaps+" "+String.format("(%.1f%%)", 100.0*totalInstancesWithOverlaps/totalInstances));
			System.out.println("Num instances with same type overlaps: "+totalInstancesWithSameTypeOverlaps+" "+String.format("(%.1f%%)", 100.0*totalInstancesWithSameTypeOverlaps/totalInstances));
			System.out.println("Num entities: "+totalEntities);
			for(String label: sorted(perTypeCount.keySet())){
				System.out.println("\t"+label+": "+perTypeCount.get(label));
			}
			System.out.println(String.format("Num entities overlapping: %d (%.1f%%)", numEntitiesOverlapping, 100.0*numEntitiesOverlapping/totalEntities));
			System.out.println(String.format("Num entities overlapping diff type: %d (%.1f%%)", numEntitiesOverlappingDiffType, 100.0*numEntitiesOverlappingDiffType/totalEntities));
			System.out.println(String.format("Num entities overlapping same type: %d (%.1f%%)", numEntitiesOverlappingSameType, 100.0*numEntitiesOverlappingSameType/totalEntities));
			System.out.println(String.format("Num entities nested: %d (%.1f%%)", numEntitiesNested, 100.0*numEntitiesNested/totalEntities));
			System.out.println(String.format("Num entities nested diff type: %d (%.1f%%)", numEntitiesNestedDiffType, 100.0*numEntitiesNestedDiffType/totalEntities));
			System.out.println(String.format("Num entities nested same type: %d (%.1f%%)", numEntitiesNestedSameType, 100.0*numEntitiesNestedSameType/totalEntities));
			System.out.println(String.format("Num entities strictly nested: %d (%.1f%%)", numEntitiesStrictlyNested, 100.0*numEntitiesStrictlyNested/totalEntities));
			System.out.println(String.format("Num entities strictly nested diff type: %d (%.1f%%)", numEntitiesStrictlyNestedDiffType, 100.0*numEntitiesStrictlyNestedDiffType/totalEntities));
			System.out.println(String.format("Num entities strictly nested same type: %d (%.1f%%)", numEntitiesStrictlyNestedSameType, 100.0*numEntitiesStrictlyNestedSameType/totalEntities));
			if(dataIsRaw){
				System.out.println("Num entities actual: "+totalEntitiesActual);
			}
			System.out.println("Num discontiguous: "+totalDiscontiguous+String.format(" (%.2f%%)", 100.0*totalDiscontiguous/totalEntities));
			if(dataIsRaw){
				System.out.println("Num discontiguous actual: "+totalDiscontiguousActual);
			}
			System.out.println("Num discontiguous >2: "+totalDiscontiguousMore);
			if(dataIsRaw){
				System.out.println("Num discontiguous >2 actual: "+totalDiscontiguousMoreActual);
			}
			System.out.println("Total number of words: "+numWords);
			System.out.println("Max instance length: "+maxInstanceLength+" words");
			if(printExamples){
				System.out.println("Example in "+exInstanceLength.sourceDoc.filename+": "+exInstanceLength.toString());
			}
			System.out.println("Max entities per instance: "+maxEntitiesPerInstance);
			System.out.println("Num entities >= 7 words: "+longEntityCount+" "+String.format("(%.1f%%)", 100.0*longEntityCount/totalEntities));
			System.out.println("Max entity length: "+maxEntityLength+" words --> "+longestEntity);
			if(printExamples){
				System.out.println("Example in "+exEntitiesPerInstance.sourceDoc.filename+": "+exEntitiesPerInstance.toString());	
			}
			System.out.println("Max discontiguous per instance: "+maxDiscontiguousPerInstance);
			if(exDiscontiguousPerInstance != null && printExamples){
				System.out.println("Example in "+exDiscontiguousPerInstance.sourceDoc.filename+": "+exDiscontiguousPerInstance.toString());
			}
			if(printSectionNames){
				System.out.println("Section names:");
				List<String> sortedSectionNames = new ArrayList<String>(sectionNames);
				Collections.sort(sortedSectionNames);
				for(String sectionName: sortedSectionNames){
					System.out.println("\t"+sectionName);
				}
			}
		}
//		CTInstance[] instances = null;
//		List<Integer> numChars = new ArrayList<Integer>();
//		List<Integer> numTokens = new ArrayList<Integer>();
//		List<Integer> spanCharLengths = new ArrayList<Integer>();
//		List<Integer> spanTokenLengths = new ArrayList<Integer>();
//		Set<String> tokenLabels = new HashSet<String>();
//		Set<String> spanLabels = new HashSet<String>();
//		for(CTInstance instance: instances){
//			numChars.add(instance.size());
//			instance.getInputTokenized(tokenizerMethod, false, false);
//			instance.getOutputTokenized(tokenizerMethod, false, false);
//			numTokens.add(instance.getInputTokenized().length);
//			for(EntitySpan span: instance.output){
//				if(labels == null || labels.contains(span.label.form)){
//					spanCharLengths.add(span.end-span.start);
//					spanLabels.add(span.label.form);
//				}
//			}
//			List<WordLabel> outputTokenized = instance.getOutputTokenized();
//			int start = 0;
//			for(int pos=0; pos<outputTokenized.size(); pos++){
//				WordLabel label = outputTokenized.get(pos);
//				String nextLabelForm = (pos == outputTokenized.size()-1) ? null : outputTokenized.get(pos+1).form;
//				if(nextLabelForm == null || nextLabelForm.startsWith("O") || nextLabelForm.startsWith("B")){
//					if(labels == null || labels.contains(label.form)){
//						spanTokenLengths.add(pos-start+1);
//						tokenLabels.add(label.form);
//					}
//					start = pos+1;
//				}
//			}
//		}
//		Collections.sort(numChars);
//		Collections.sort(numTokens);
//		Collections.sort(spanCharLengths);
//		Collections.sort(spanTokenLengths);
//		printStatistics(numChars, includePercentiles, "instance length (in char)");
//		printStatistics(numTokens, includePercentiles, "instance length (in tokens)");
//		printStatistics(spanCharLengths, includePercentiles, "span length (in char) "+spanLabels);
//		printStatistics(spanTokenLengths, includePercentiles, "span length (in tokens) "+tokenLabels);
	}
	
//	private static void printStatistics(List<Integer> nums, double[] includePercentiles, String name){
//		System.out.println("Statistics for "+name);
//		System.out.println(String.format("Total: %d", nums.size()));
//		Statistics stat = new Statistics(nums);
//		System.out.println(String.format("Max: %d", stat.max));
//		System.out.println(String.format("Min: %d", stat.min));
//		System.out.println(String.format("Mode: %d (%d)", stat.mode, stat.modeCount));
//		System.out.println(String.format("50%% percentile: %d", getPercentileAt(nums, 0.50)));
//		System.out.println(String.format("75%% percentile: %d", getPercentileAt(nums, 0.75)));
//		System.out.println(String.format("80%% percentile: %d", getPercentileAt(nums, 0.80)));
//		System.out.println(String.format("90%% percentile: %d", getPercentileAt(nums, 0.90)));
//		System.out.println(String.format("95%% percentile: %d", getPercentileAt(nums, 0.95)));
//		System.out.println(String.format("99%% percentile: %d", getPercentileAt(nums, 0.99)));
//		for(double percentile: includePercentiles){
//			System.out.println(String.format("%s%% percentile: %d", 100*percentile, getPercentileAt(nums, percentile)));
//		}
//	}
//	
//	private static int getPercentileAt(List<Integer> num, double percentile){
//		return num.get((int)Math.round(percentile*num.size()));
//	}
//	
//	private static class Statistics{
//		List<Integer> nums;
//		int max;
//		int min;
//		int mode;
//		int modeCount;
//		public Statistics(List<Integer> nums){
//			this.nums = nums;
//			getMode();
//			getMinMax();
//		}
//		
//		private void getMinMax(){
//			min = Integer.MAX_VALUE;
//			max = Integer.MIN_VALUE;
//			for(int num: nums){
//				min = Math.min(num, min);
//				max = Math.max(num, max);
//			}
//		}
//		
//		private void getMode(){
//			HashMap<Integer, Integer> counts = new HashMap<Integer, Integer>();
//			modeCount = 0;
//			mode = -1;
//			for(int num: nums){
//				int count = counts.getOrDefault(num, 0);
//				counts.put(num, count+1);
//				if(count+1 > modeCount){
//					modeCount = count+1;
//					mode = num;
//				}
//			}
//			
//		}
//	}

}
