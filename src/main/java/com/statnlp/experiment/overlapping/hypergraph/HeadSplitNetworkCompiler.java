package com.statnlp.experiment.overlapping.hypergraph;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

import com.statnlp.commons.types.Instance;
import com.statnlp.experiment.overlapping.EntitySpan;
import com.statnlp.experiment.overlapping.OEInstance;
import com.statnlp.experiment.overlapping.OENetwork;
import com.statnlp.experiment.overlapping.Span;
import com.statnlp.experiment.overlapping.SpanLabel;
import com.statnlp.experiment.overlapping.OETokenizer.TokenizerMethod;
import com.statnlp.experiment.overlapping.hypergraph.MentionHypergraphNetworkCompiler.NetworkInterpretation;
import com.statnlp.hybridnetworks.LocalNetworkParam;
import com.statnlp.hybridnetworks.Network;
import com.statnlp.hybridnetworks.NetworkCompiler;
import com.statnlp.hybridnetworks.NetworkException;
import com.statnlp.hybridnetworks.NetworkIDMapper;

import edu.stanford.nlp.ling.CoreLabel;

public class HeadSplitNetworkCompiler extends NetworkCompiler {

	private static final long serialVersionUID = -4353692924395630953L;
	public static final boolean DEBUG = false;
	
	public SpanLabel[] _labels;
	public int maxSize = 500;
	public int maxBodyCount = 3;
	public OENetwork unlabeledNetwork;
	public TokenizerMethod tokenizerMethod;
	public NetworkInterpretation networkInterpretation;
	
	public static enum NodeType{
		X_NODE,
		B_NODE,
		O_NODE,
		T_NODE,
		E_NODE,
		A_NODE,
	}
	
	static {
		NetworkIDMapper.setCapacity(new int[]{10000, 10, 10, 10, 100});
	}

	public HeadSplitNetworkCompiler(SpanLabel[] labels, int maxLength, int maxBodyCount, TokenizerMethod tokenizerMethod, NetworkInterpretation networkInterpretation){
		this._labels = labels;
		this.maxSize = Math.max(this.maxSize, maxLength);
		this.maxBodyCount = Math.max(this.maxBodyCount, maxBodyCount);
		this.tokenizerMethod = tokenizerMethod;
		this.networkInterpretation = networkInterpretation;
		buildUnlabeled();
	}

	@Override
	public OENetwork compile(int networkId, Instance inst, LocalNetworkParam param) {
		OEInstance instance = (OEInstance)inst;
		if(instance.isLabeled()){
			return compileLabeled(networkId, instance, param);
		} else {
			return compileUnlabeled(networkId, instance, param);
		}
	}
	
	private OENetwork compileLabeled(int networkId, OEInstance instance, LocalNetworkParam param){
		OENetwork network = new OENetwork(networkId, instance, param, this);
		CoreLabel[] words = instance.getInputTokenized().toArray(new CoreLabel[0]);
		int size = words.length;
		
		long xNode = toNode_X();
		network.addNode(xNode);
		for(EntitySpan entitySpan: instance.output){
			int labelId = entitySpan.label.id;
			long prevNode = -1;
			int pos = -1;
			int splitIndex = entitySpan.spans.length-1;
			for(Span span: entitySpan.spans){
				if(pos != -1){
					boolean hasSomeONodes = false;
					for(; words[pos].beginPosition()<span.start; pos++){
						hasSomeONodes = true;
					}
					if(!hasSomeONodes){
						splitIndex--;
					}
				} else {
					for(pos=0; words[pos].endPosition() <= span.start; pos++);
				}
				for(; pos<size && words[pos].beginPosition()<span.end; pos++);
			}
			pos = -1;
			for(int spanIdx=0, bodyIdx=0; spanIdx<entitySpan.spans.length; spanIdx++, bodyIdx++){
				Span span = entitySpan.spans[spanIdx];
				if(pos != -1){
					boolean hasSomeONodes = false;
					for(; words[pos].beginPosition()<span.start; pos++){
						long curONode = toNode_O(pos, size, splitIndex, bodyIdx, labelId);
						if(!network.contains(curONode)){
							network.addNode(curONode);
						}
						try{
							network.addEdge(prevNode, new long[]{curONode});
						} catch (NetworkException e){
							// do nothing, edge from previous node to this current O node is already added
						}
						prevNode = curONode;
						hasSomeONodes = true;
					}
					if(!hasSomeONodes){
						bodyIdx--;
					}
				} else {
					for(pos=0; words[pos].endPosition() <= span.start; pos++);
				}
				for(; pos<size && words[pos].beginPosition()<span.end; pos++){
					long curBNode = toNode_B(pos, size, splitIndex, bodyIdx, labelId);
					if(!network.contains(curBNode)){
						network.addNode(curBNode);
					}
					if(prevNode == -1){
						long tNode = toNode_T(pos, size, splitIndex, labelId);
						if(!network.contains(tNode)){
							network.addNode(tNode);
						}
						try{
							network.addEdge(tNode, new long[]{curBNode});
						} catch (NetworkException e){
							// do nothing, edge from T to B already added (two mentions with the same start index)
						}
					} else {
						try{
							network.addEdge(prevNode, new long[]{curBNode});
						} catch (NetworkException e){
							// do nothing, edge from prevI to curB already added (overlapping mentions)
						}
					}
					prevNode = curBNode;
				}
			}
			try{
				network.addEdge(prevNode, new long[]{xNode});
			} catch (NetworkException e){
				// do nothing, edge from B to X already added (two mentions with the same end index)
			}
		}
		// Post processing
		for(int pos=size-1; pos>=0; pos--){
			long[] tNodes = new long[_labels.length*this.maxBodyCount];
			for(int idx=0; idx<_labels.length; idx++){
				SpanLabel label = _labels[idx];
				for(int splitIndex=0; splitIndex<this.maxBodyCount; splitIndex++){
					for(int spanIdx=0; spanIdx<splitIndex+1; spanIdx++){
						// Collate multiple edges to a single hyperedge for B node and O node
						long bNode = toNode_B(pos, size, splitIndex, spanIdx, label.id);
						if(network.contains(bNode)){
							ArrayList<long[]> childrenList = network.getChildren_tmp(bNode);
							if(childrenList.size() > 1){
								collateChildren(childrenList);
							}
						}
						if(spanIdx > 0){
							long oNode = toNode_O(pos, size, splitIndex, spanIdx, label.id);
							if(network.contains(oNode)){
								ArrayList<long[]> childrenList = network.getChildren_tmp(oNode);
								if(childrenList.size() > 1){
									collateChildren(childrenList);
								}
							}
						}
					}
					// Add missing T nodes
					long tNode = toNode_T(pos, size, splitIndex, label.id);
					if(!network.contains(tNode)){
						network.addNode(tNode);
						network.addEdge(tNode, new long[]{xNode});
					}
					tNodes[idx*this.maxBodyCount+splitIndex] = tNode;
				}
			}
			long eNode = toNode_E(pos, size);
			network.addNode(eNode);
			network.addEdge(eNode, tNodes);
			long aNode = toNode_A(pos, size);
			network.addNode(aNode);
			if(pos < size-1){
				long nextANode = toNode_A(pos+1, size);
				network.addEdge(aNode, new long[]{eNode, nextANode});
			} else {
				network.addEdge(aNode, new long[]{eNode});
			}
		}
		
		network.finalizeNetwork();
		
		if(DEBUG){
			OENetwork unlabeled = compileUnlabeled(networkId, instance, param);
			System.out.println(instance);
			System.out.println(network);
			System.out.println("Contained: "+unlabeled.contains(network));
		}
		return network;
	}
	
	private void collateChildren(List<long[]> childrenList){
		long[] children = new long[childrenList.size()];
		Iterator<long[]> iter = childrenList.iterator();
		int idx = 0;
//		System.out.println("Collating");
		while(iter.hasNext()){
			long[] child = iter.next();
//			System.out.print("[");
//			for(int i=0; i<child.length; i++){
//				System.out.print(child[i]+" ");
//			}
//			System.out.println("]");
			children[idx] = child[0];
			idx += 1;
		}
//		System.out.println("Result");
		childrenList.clear();
		Arrays.sort(children);
//		printArray(children);
		childrenList.add(children);
	}
	
//	private void printArray(long[] arr){
//		StringBuilder builder = new StringBuilder();
//		builder.append("[");
//		for(long str: arr){
//			if(builder.length() > 1) builder.append(",");
//			builder.append(str);
//		}
//		builder.append("]");
//		System.out.println(builder.toString());
//	}

	private OENetwork compileUnlabeled(int networkId, OEInstance instance, LocalNetworkParam param){
		int size = instance.getInputTokenized().size();
		long root = toNode_A(0, size);
		long[] allNodes = unlabeledNetwork.getAllNodes();
		int[][][] allChildren = unlabeledNetwork.getAllChildren();
		int root_k = Arrays.binarySearch(allNodes, root);
		int numNodes = root_k+1;
		OENetwork network = new OENetwork(networkId, instance, allNodes, allChildren, param, numNodes, this);
		return network;
	}
	
	private void buildUnlabeled(){
		System.err.print("Building generic unlabeled tree up to size "+maxSize+"...");
		long startTime = System.currentTimeMillis();
		OENetwork network = new OENetwork();
		int size = maxSize;
		
		long xNode = toNode_X();
		network.addNode(xNode);
		for(int pos=size-1; pos>=0; pos--){
			long[] tNodes = new long[_labels.length*this.maxBodyCount];
			for(int idx=0; idx<_labels.length; idx++){
				SpanLabel label = _labels[idx];
				for(int splitIndex=0; splitIndex<this.maxBodyCount; splitIndex++){
					long firstBNode = -1; // To be connected to T node
					for(int bodyIdx=0; bodyIdx < splitIndex+1; bodyIdx++){
						long bNode = toNode_B(pos, size, splitIndex, bodyIdx, label.id);
						if(pos >= size-1 - 2*(splitIndex-bodyIdx) + 1){
							continue;
						}
						network.addNode(bNode);
						if(bodyIdx == 0){ // Top-most B-node
							firstBNode = bNode;
						}
						if(bodyIdx == splitIndex){
							network.addEdge(bNode, new long[]{xNode});
						}
						long oNode = -1;
						if(bodyIdx > 0 && pos < size-1 - 2*(splitIndex-bodyIdx)){ // The O-node
							oNode = toNode_O(pos, size, splitIndex, bodyIdx, label.id);
							network.addNode(oNode);
						}
						if(pos < size-1){
							long nextONode = toNode_O(pos+1, size, splitIndex, bodyIdx, label.id);
							if(!network.contains(nextONode)){
								nextONode = -1;
							}
							long nextBNode = toNode_B(pos+1, size, splitIndex, bodyIdx, label.id);
							if(!network.contains(nextBNode)){
								nextBNode = -1;
							}
							long nextHighONode = toNode_O(pos+1, size, splitIndex, bodyIdx+1, label.id);
							if(!network.contains(nextHighONode)){
								nextHighONode = -1;
							}
							
							if(nextBNode != -1){
								network.addEdge(bNode, new long[]{nextBNode});
								if(bodyIdx == splitIndex){
									network.addEdge(bNode, new long[]{xNode, nextBNode});
								}
								if(oNode != -1){
									network.addEdge(oNode, new long[]{nextBNode});
									if(nextONode != -1){
										network.addEdge(oNode, new long[]{nextONode});
										network.addEdge(oNode, new long[]{nextBNode, nextONode});
									}
								}
							}
							if(nextHighONode != -1){
								network.addEdge(bNode, new long[]{nextHighONode});
								if(bodyIdx == splitIndex){
									network.addEdge(bNode, new long[]{xNode, nextHighONode});
								}
								if(nextBNode != -1){
									network.addEdge(bNode, new long[]{nextBNode, nextHighONode});
									if(bodyIdx == splitIndex){
										network.addEdge(bNode, new long[]{xNode, nextBNode, nextHighONode});
									}
								}
							}
						}
					}
					long tNode = toNode_T(pos, size, splitIndex, label.id);
					network.addNode(tNode);
					network.addEdge(tNode, new long[]{xNode});
					if(firstBNode != -1){
						network.addEdge(tNode, new long[]{firstBNode});
					}
					tNodes[idx*this.maxBodyCount+splitIndex] = tNode;
				}
			}
			long eNode = toNode_E(pos, size);
			network.addNode(eNode);
			network.addEdge(eNode, tNodes);
			long aNode = toNode_A(pos, size);
			network.addNode(aNode);
			if(pos < size-1){
				long nextANode = toNode_A(pos+1, size);
				network.addEdge(aNode, new long[]{eNode, nextANode});
			} else {
				network.addEdge(aNode, new long[]{eNode});
			}
		}
		
		network.finalizeNetwork();
		
		this.unlabeledNetwork = network;
		
		long endTime = System.currentTimeMillis();
		System.err.println(String.format("Done in %.3fs", (endTime-startTime)/1000.0));
	}
	
	private long toNode_X(){
		return toNode(0, 1, NodeType.X_NODE, 0, 0, 0);
	}
	
	private long toNode_B(int pos, int size, int splitIndex, int bodyIndex, int labelId){
		return toNode(pos, size, NodeType.B_NODE, splitIndex, bodyIndex, labelId);
	}

	private long toNode_O(int pos, int size, int splitIndex, int bodyIndex, int labelId){
		return toNode(pos, size, NodeType.O_NODE, splitIndex, bodyIndex, labelId);
	}
	
	private long toNode_T(int pos, int size, int splitIndex, int labelId){
		return toNode(pos, size, NodeType.T_NODE, splitIndex, 9, labelId);
	}
	
	private long toNode_E(int pos, int size){
		return toNode(pos, size, NodeType.E_NODE, 9, 9, 0);
	}
	
	private long toNode_A(int pos, int size){
		return toNode(pos, size, NodeType.A_NODE, 9, 9, 0);
	}
	
	private long toNode(int pos, int size, NodeType nodeType, int splitIndex, int bodyIndex, int labelId){
		int[] arr = new int[]{size-pos-1, splitIndex, bodyIndex, nodeType.ordinal(), labelId};
		return NetworkIDMapper.toHybridNodeID(arr);
	}
	
	private static class JunctionInfo {
		public int highestUnprocessedChildIdx;
		public int startIdx;
		public SpanLabel label;
		public List<NodeType> nodeSeq;
		public int node_k;
		
		public JunctionInfo(int node_k, int highestIdx, int startIdx, SpanLabel label, List<NodeType> nodeSeq){
			this.node_k = node_k;
			this.highestUnprocessedChildIdx = highestIdx;
			this.startIdx = startIdx;
			this.label = label;
			this.nodeSeq = new ArrayList<NodeType>(nodeSeq);
		}
	}
	
	private static EntitySpan createEntitySpan(int start, List<NodeType> nodeSeq, List<CoreLabel> inputTokenized, SpanLabel label){
		List<Span> spans = new ArrayList<Span>();
		int startIdx = -1;
		for(int i=0; i<nodeSeq.size(); i++){
			NodeType curNode = nodeSeq.get(i);
			if(curNode == NodeType.O_NODE){
				startIdx = -1;
				continue;
			}
			if(startIdx == -1){
				startIdx = inputTokenized.get(start+i).beginPosition();
			}
			
			if(shouldCreateNewSpan(start, i, nodeSeq, inputTokenized)){
				spans.add(new Span(startIdx, inputTokenized.get(start+i).endPosition()));
				startIdx = -1;
			}
		}
		return new EntitySpan(null, label, spans.toArray(new Span[spans.size()]));
	}
	
	private static boolean shouldCreateNewSpan(int start, int i, List<NodeType> nodeSeq, List<CoreLabel> inputTokenized){
		if(i == nodeSeq.size()-1){
			return true;
		}
		NodeType next = nodeSeq.get(i+1);
		if(next == NodeType.O_NODE){
			return true;
		}
		return false;
	}

	@Override
	public OEInstance decompile(Network net) {
		OENetwork network = (OENetwork)net;
		OEInstance result = (OEInstance)network.getInstance().duplicate();
		List<CoreLabel> inputTokenized = result.getInputTokenized();
		int size = inputTokenized.size();
		
		List<EntitySpan> prediction = new ArrayList<EntitySpan>();
		long[] nodes = network.getAllNodes();
		List<JunctionInfo> unfinishedJunctions = new ArrayList<JunctionInfo>();
		long aNode = network.getRoot();
		int aNode_k = Arrays.binarySearch(nodes, aNode);
		for(int pos=0; pos<size; pos++){
			int[] curChildren = network.getMaxPath(aNode_k);
			int eNode_k = curChildren[0];
			int[] curTNodes = network.getMaxPath(eNode_k);
			for(int idx=0; idx<curTNodes.length; idx++){
				int tNode_k = curTNodes[idx];
				int[] tNode_arr = network.getNodeArray(tNode_k);
				int labelId = tNode_arr[4];
				SpanLabel label = SpanLabel.get(labelId);
				int[] child = network.getMaxPath(tNode_k);
				int node_k = child[0]; // either a B-node or an X-node
				int[] node_arr = network.getNodeArray(node_k);
				NodeType nodeType = NodeType.values()[node_arr[3]];
				if(nodeType == NodeType.B_NODE){ // Has a mention starting here
					List<NodeType> nodeSeq = new ArrayList<NodeType>();
					traverseMaxNetwork(network, inputTokenized, prediction, unfinishedJunctions, nodeSeq, pos, node_k,
							nodeType, label);
				}
			}
			if(curChildren.length == 2){
				aNode_k = curChildren[1];
			}
		}
		int junctionIndex = 0;
		if(networkInterpretation == NetworkInterpretation.GENERATE_ALL){
			junctionIndex = unfinishedJunctions.size()-1;
		}
		while(junctionIndex < unfinishedJunctions.size() && junctionIndex >= 0){
			JunctionInfo junction = unfinishedJunctions.get(junctionIndex);
			List<NodeType> nodeSeq = new ArrayList<NodeType>(junction.nodeSeq);
			int start = junction.startIdx;
			int[] child = network.getMaxPath(junction.node_k);
			int node_k = child[junction.highestUnprocessedChildIdx];
			if(networkInterpretation == NetworkInterpretation.GENERATE_ENOUGH){
				junction.highestUnprocessedChildIdx -= 1;
				if(junction.highestUnprocessedChildIdx == -1){
					unfinishedJunctions.remove(junctionIndex);
				}
			}
			int[] node_arr = network.getNodeArray(node_k);
			NodeType nodeType = NodeType.values()[node_arr[3]];
			SpanLabel label = junction.label;
			traverseMaxNetwork(network, inputTokenized, prediction, unfinishedJunctions, nodeSeq, start, node_k,
					nodeType, label);
			if(networkInterpretation == NetworkInterpretation.GENERATE_ALL){
				junctionIndex = unfinishedJunctions.size()-1;
			}
		}
		result.setPrediction(prediction);
		return result;
	}

	private void traverseMaxNetwork(OENetwork network, List<CoreLabel> inputTokenized, List<EntitySpan> prediction,
			List<JunctionInfo> unfinishedJunctions, List<NodeType> nodeSeq, int start, int node_k, NodeType nodeType,
			SpanLabel label) throws RuntimeException {
		int[] child;
		int[] node_arr;
		while(nodeType != NodeType.X_NODE){
			nodeSeq.add(nodeType);
			child = network.getMaxPath(node_k);
			if(child.length == 0){
				throw new RuntimeException("Decoding ends at a leaf node which is not an X-node");
			} else if(child.length == 1){
				node_k = child[0];
			} else { // there are more than one children for this edge (overlapping mention)
				node_k = nextChild(unfinishedJunctions, nodeSeq, start, node_k, label, child);
			}
			node_arr = network.getNodeArray(node_k);
			nodeType = NodeType.values()[node_arr[3]];
		}
		if(networkInterpretation == NetworkInterpretation.GENERATE_ALL){
			int unfinishedSize = unfinishedJunctions.size();
			while(unfinishedSize > 0){
				JunctionInfo junction = unfinishedJunctions.get(unfinishedSize-1);
				if(junction.startIdx != start){
					break;
				}
				junction.highestUnprocessedChildIdx -= 1;
				if(junction.highestUnprocessedChildIdx == -1){
					unfinishedJunctions.remove(unfinishedSize-1);
					unfinishedSize -= 1;
					continue;
				}
				break;
			}
		}
		prediction.add(createEntitySpan(start, nodeSeq, inputTokenized, label));
	}

	private int nextChild(List<JunctionInfo> unfinishedJunctions, List<NodeType> nodeSeq, int start, int node_k,
			SpanLabel label, int[] child) {
		int childIdx = -1;
		for(int i=unfinishedJunctions.size()-1; i>=0; i--){
			JunctionInfo info = unfinishedJunctions.get(i);
			if(info.node_k == node_k && (networkInterpretation == NetworkInterpretation.GENERATE_ENOUGH || info.startIdx == start)){
				childIdx = info.highestUnprocessedChildIdx;
				if(networkInterpretation == NetworkInterpretation.GENERATE_ENOUGH){
					info.highestUnprocessedChildIdx -= 1;
					if(info.highestUnprocessedChildIdx == -1){
						unfinishedJunctions.remove(i);
					}	
				}
				break;
			}
		}
		if(childIdx == -1){
			int highestUnprocessed = -1;
			switch(networkInterpretation){
			case GENERATE_ALL:
				highestUnprocessed = child.length-1;
				break;
			case GENERATE_ENOUGH:
				highestUnprocessed = child.length-2;
				break;
			}
			JunctionInfo curInfo = new JunctionInfo(node_k, highestUnprocessed, start, label, nodeSeq);
			unfinishedJunctions.add(curInfo);
			childIdx = child.length-1;
		}
		node_k = child[childIdx];
		return node_k;
	}

}