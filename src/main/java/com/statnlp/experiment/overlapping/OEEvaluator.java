package com.statnlp.experiment.overlapping;

import static com.statnlp.experiment.overlapping.OEUtil.print;

import java.io.IOException;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

import com.statnlp.commons.types.Instance;
import com.statnlp.experiment.overlapping.OESplitter.SplitterMethod;
import com.statnlp.experiment.overlapping.OETokenizer.TokenizerMethod;
import com.statnlp.experiment.overlapping.OEUtil.InstanceFilter;

import edu.stanford.nlp.util.StringUtils;

public class OEEvaluator {
	
	public static class Statistics {
		public int correct=0;
		public int totalPred=0;
		public int totalGold=0;
		
		public void add(Statistics s){
			this.correct += s.correct;
			this.totalPred += s.totalPred;
			this.totalGold += s.totalGold;
		}
		
		public double calculatePrecision(){
			if(totalPred == 0){
				return 0;
			}
			return 1.0*correct/totalPred;
		}
		
		public double calculateRecall(){
			if(totalGold == 0){
				return 0;
			}
			return 1.0*correct/totalGold;
		}
		
		public double calculateF1(){
			double precision = calculatePrecision();
			double recall = calculateRecall();
			double f1 = precision*recall;
			if(f1 == 0){
				return 0;
			}
			f1 = 2*f1/(precision+recall);
			return f1;
		}
		
		public void printScore(PrintStream... outstreams){
			double precision = calculatePrecision();
			double recall = calculateRecall();
			double f1 = calculateF1();
			print(String.format("Correct: %1$3d, Predicted: %2$3d, Gold: %3$3d ", correct, totalPred, totalGold), true, outstreams);
			print(String.format("Overall P: %#5.2f%%, R: %#5.2f%%, F: %#5.2f%%", 100*precision, 100*recall, 100*f1), true, outstreams);
		}
	}
	
	private static enum Argument{
		RESULT_DIR(1,
				"The directory containing predicted annotation files to be evaluated",
				"resultDir",
				"resultDir"),
		RESULT2_DIR(1,
				"The directory containing second predicted annotation files to be evaluated",
				"result2Dir",
				"result2Dir"),
		GOLD_DIR(1,
				"The directory containing gold annotation files",
				"goldDir",
				"goldDir"),
		SOURCE_DIR(1,
				"The directory containing the original clincal text",
				"sourceDir",
				"sourceDir"),
		SPLITTER(1,
				"The sentence splitter method to be used: stanford. Default to stanford",
				"splitter",
				"splitter"),
		TOKENIZER(1,
				"The tokenizer to be used: stanford, regex, or whitespace. Default to regex",
				"tokenizer",
				"tokenizer"),
		INSTANCE_FILTER(1,
				"How to do instance filtering. Default to all_with_entities",
				"instanceFilter",
				"[all_instances|all_with_entities|only_contiguous|contain_discontiguous]"),
		RESAMPLE(0,
				"To do bootstrap resampling",
				"resample"),
		N(1,
				"The number of resampling to be done, used for bootstrap resampling and AR Sigtest",
				"n",
				"n"),
		NUM_EXAMPLES_PRINTED(1,
				"The number of examples to be printed, default to 10",
				"numExamplesPrinted",
				"numExamplesPrinted"),
		PRINT_ONLY_MISTAKES(0,
				"Whether to print only instances containing mistakes (if gold annotation is provided)",
				"printOnlyMistakes"),
		USE_RELAXED_CRITERION(0,
				"Whether to use the relaxed evaluation criterion (overlap counts as correct)",
				"useRelaxedCriterion"),
		DATA_IS_RAW(1,
				"Whether the data is raw format (text and annotation files are separate), or formatted.",
				"dataIsRaw",
				"(true|false)"
				),
		NORMALIZE_DATE_AND_NUMBERS(0,
				"Whether to normalize dates and numbers",
				"normalizeDateAndNumbers"),
		IGNORE_NESTED(0,
				"Whether to ignore nested entities during evaluation",
				"ignoreNested"),
		DO_AR_SIGTEST(0,
				"Whether to do approximate randomization significance test.",
				"doARSigtest"
				),
		HELP(0,
				"Print this help message",
				"h,help"),
		;

		final private int numArgs;
		final private String[] argNames;
		final private String[] names;
		final private String help;
		private Argument(int numArgs, String help, String names, String... argNames){
			this.numArgs = numArgs;
			this.argNames = argNames;
			this.names = names.split(",");
			this.help = help;
		}
		
		/**
		 * Return the Argument which has the specified name
		 * @param name
		 * @return
		 */
		public static Argument argWithName(String name){
			for(Argument argument: Argument.values()){
				for(String argName: argument.names){
					if(argName.equals(name)){
						return argument;
					}
				}
			}
			throw new IllegalArgumentException("Unrecognized argument: "+name);
		}
		
		/**
		 * Print help message
		 */
		private static void printHelp(){
			StringBuilder result = new StringBuilder();
			result.append("Options:\n");
			for(Argument argument: Argument.values()){
				result.append("-"+StringUtils.join(argument.names, " -"));
				result.append(" "+StringUtils.join(argument.argNames, " "));
				result.append("\n");
				if(argument.help != null && argument.help.length() > 0){
					result.append("\t"+argument.help.replaceAll("\n","\n\t")+"\n");
				}
			}
			System.out.println(result.toString());
		}
	}
	
	public static void main(String[] args) throws IOException{
		String testSourceDir = null;
		String testGoldDir = null;
		String testPredDir = null;
		String test2PredDir = null;
		SplitterMethod splitterMethod = SplitterMethod.STANFORD;
		TokenizerMethod tokenizerMethod = TokenizerMethod.REGEX;
		InstanceFilter instanceFilter = InstanceFilter.ALL_INSTANCES;
		boolean resample = false;
		boolean doARSigtest = false;
		int n = 10000;
		int numExamplesPrinted = 10;
		boolean printOnlyMistakes = false;
		boolean useRelaxedCriterion = false;
		boolean ignoreNested = false;
		boolean dataIsRaw = true;
		boolean normalizeDateAndNumbers = false;
		int argIndex = 0;
		while(argIndex < args.length){
			String arg = args[argIndex];
			if(arg.length() > 0 && arg.charAt(0) == '-'){
				Argument argument = Argument.argWithName(arg.substring(1));
				switch(argument){
				case RESULT_DIR:
					testPredDir = args[argIndex+1];
					break;
				case RESULT2_DIR:
					test2PredDir = args[argIndex+1];
					break;
				case GOLD_DIR:
					testGoldDir = args[argIndex+1];
					break;
				case SOURCE_DIR:
					testSourceDir = args[argIndex+1];
					break;
				case SPLITTER:
					splitterMethod = SplitterMethod.valueOf(args[argIndex+1].toUpperCase());
					break;
				case TOKENIZER:
					tokenizerMethod = TokenizerMethod.valueOf(args[argIndex+1].toUpperCase());
					break;
				case INSTANCE_FILTER:
					instanceFilter = InstanceFilter.valueOf(args[argIndex+1].toUpperCase());
					break;
				case RESAMPLE:
					resample = true;
					break;
				case N:
					n = Integer.parseInt(args[argIndex+1]);
					break;
				case NUM_EXAMPLES_PRINTED:
					numExamplesPrinted = Integer.parseInt(args[argIndex+1]);
					break;
				case PRINT_ONLY_MISTAKES:
					printOnlyMistakes = true;
					break;
				case USE_RELAXED_CRITERION:
					useRelaxedCriterion = true;
					break;
				case DATA_IS_RAW:
					dataIsRaw = Boolean.parseBoolean(args[argIndex+1]);
					break;
				case NORMALIZE_DATE_AND_NUMBERS:
					normalizeDateAndNumbers = true;
					break;
				case IGNORE_NESTED:
					ignoreNested = true;
					break;
				case DO_AR_SIGTEST:
					doARSigtest = true;
					break;
				case HELP:
					Argument.printHelp();
					System.exit(0);
				}
				argIndex += argument.numArgs+1;
			} else {
				throw new IllegalArgumentException("Error while parsing: "+arg);
			}
		}
		if(testPredDir == null && testGoldDir == null){
			Argument.printHelp();
			System.exit(0);
		}
		OEInstance[] predInstances = null;
		OEInstance[] pred2Instances = null;
		OEInstance[] goldInstances = null;
		if(dataIsRaw){
			predInstances = OEUtil.readFromDirAndGetInstances(testSourceDir, testPredDir, true, splitterMethod, tokenizerMethod, InstanceFilter.ALL_INSTANCES, null, 0, false, false, 0);
			if(test2PredDir != null){
				pred2Instances = OEUtil.readFromDirAndGetInstances(testSourceDir, test2PredDir, true, splitterMethod, tokenizerMethod, InstanceFilter.ALL_INSTANCES, null, 0, false, false, 0);
			}
			goldInstances = OEUtil.readFromDirAndGetInstances(testSourceDir, testGoldDir, true, splitterMethod, tokenizerMethod, InstanceFilter.ALL_INSTANCES, null, 0, false, false, 0);
		} else {
			predInstances = OEUtil.readFromFormattedData(testPredDir, true, true, normalizeDateAndNumbers);
			if(test2PredDir != null){
				pred2Instances = OEUtil.readFromFormattedData(test2PredDir, true, true, normalizeDateAndNumbers);
			}
			goldInstances = OEUtil.readFromFormattedData(testGoldDir, true, true, normalizeDateAndNumbers);
		}
		List<Instance> instancesList = new ArrayList<Instance>();
		List<Instance> instances2List = new ArrayList<Instance>();
		for(int i=0; i<goldInstances.length; i++){
			OEInstance goldInstance = goldInstances[i];
			boolean containDiscontiguous = false;
			for(EntitySpan entity: goldInstance.output){
				if(entity.spans.length > 1){
					containDiscontiguous = true;
					break;
				}
			}
			if(InstanceFilter.ALL_WITH_ENTITIES == instanceFilter){
				if(goldInstance.output.size() == 0){
					continue;
				}
			} else if(InstanceFilter.CONTAIN_DISCONTIGUOUS == instanceFilter){
				if(!containDiscontiguous){
					continue;
				}
			} else if(InstanceFilter.NO_DISCONTIGUOUS == instanceFilter){
				if(containDiscontiguous){
					continue;
				}
			} else if(InstanceFilter.ONLY_CONTIGUOUS == instanceFilter){
				if(goldInstance.output.size() == 0 || containDiscontiguous){
					continue;
				}
			}
			OEInstance predInstance = predInstances[i];
			if(test2PredDir != null){
				OEInstance gold2Instance = goldInstance.duplicate();
				gold2Instance.setPrediction(pred2Instances[i].getOutput());
				instances2List.add(gold2Instance);
			}
			goldInstance.setPrediction(predInstance.getOutput());
			instancesList.add(goldInstance);
		}
		Instance[] instances = instancesList.toArray(new Instance[instancesList.size()]);
		Instance[] instances2 = instances2List.toArray(new Instance[instances2List.size()]);
		if(resample){
			if(instances2.length == instances.length){
				evaluatePairedBootstrap(instances, instances2, null, n, useRelaxedCriterion, ignoreNested);
			} else {
				evaluateBootstrap(instances, null, n, useRelaxedCriterion, ignoreNested);
			}
		} else if (doARSigtest){
			evaluateARSigtest(instances, instances2, null, n, useRelaxedCriterion, ignoreNested);
		} else {
			evaluate(instances, null, numExamplesPrinted, printOnlyMistakes, false, useRelaxedCriterion, ignoreNested);
		}
	}

	public static void evaluateARSigtest(Instance[] predictions, Instance[] predictions2, PrintStream outstream, int n, boolean useRelaxedCriterion, boolean ignoreNested){
		System.out.println("Not implemented yet");
	}

	public static void evaluatePairedBootstrap(Instance[] predictions, Instance[] predictions2, PrintStream outstream, int n, boolean useRelaxedCriterion, boolean ignoreNested){
		Random rand = new Random(17);
		int popSize = predictions.length;
		Statistics[] origResults1 = getScore(predictions, useRelaxedCriterion, ignoreNested);
		Statistics[] origResults2 = getScore(predictions2, useRelaxedCriterion, ignoreNested);
		Statistics overallResult1 = sum(origResults1);
		Statistics overallResult2 = sum(origResults2);
		
		int[] multipliers = new int[origResults1.length+1];
		for(int i=0; i<origResults1.length; i++){
			if(origResults1[i].calculateF1() > origResults2[i].calculateF1()){
				multipliers[i] = -1;
			} else {
				multipliers[i] = 1;
			}
		}
		if(overallResult1.calculateF1() > overallResult2.calculateF1()){
			multipliers[multipliers.length-1] = -1;
		} else {
			multipliers[multipliers.length-1] = 1;
		}
		int[] oneMoreThanTwoCount = new int[origResults1.length+1];
		for(int sampleIdx=0; sampleIdx < n; sampleIdx++){
			int[] selectedIndices = new int[popSize];
			for(int i=0; i<popSize; i++){
				selectedIndices[i] = rand.nextInt(popSize);
			}
			Statistics[] sampledResults1 = getSampledStatistics(predictions, useRelaxedCriterion, ignoreNested, selectedIndices);
			Statistics[] sampledResults2 = getSampledStatistics(predictions2, useRelaxedCriterion, ignoreNested, selectedIndices);
			for(int i=0; i<sampledResults1.length; i++){
				if(sampledResults1[i].calculateF1()*multipliers[i] > sampledResults2[i].calculateF1()*multipliers[i]){
					oneMoreThanTwoCount[i] += 1;
				}
			}
		}
		for(int i=0; i<oneMoreThanTwoCount.length-1; i++){
			double pValue = 1.0*oneMoreThanTwoCount[i]/n;
			double threshold = 1e-5;
			boolean flag = true;
			while(threshold <= 5.01e-2){
				if(pValue <= threshold){
					System.out.println(String.format("Label: %s. F1-score %.2f%% vs %.2f%% is significant. (p ≤ %s -- %d/%d = %.6g)", SpanLabel.get(i), 100*origResults1[i].calculateF1(), 100*origResults2[i].calculateF1(), threshold, oneMoreThanTwoCount[i], n, pValue));
					break;
				}
				if(flag){
					threshold *= 5;
					flag = false;
				} else {
					threshold *= 2;
					flag = true;
				}
			}
			if(threshold > 5.01e-2){
				System.out.println(String.format("Label: %s. F1-score %.2f%% vs %.2f%% is not significant. (p > 0.05 -- %d/%d = %.6g)", SpanLabel.get(i), 100*origResults1[i].calculateF1(), 100*origResults2[i].calculateF1(), oneMoreThanTwoCount[i], n, pValue));
			}
		}
		int last = oneMoreThanTwoCount.length-1;
		double pValue = 1.0*oneMoreThanTwoCount[last]/n;
		double threshold = 1e-5;
		boolean flag = true;
		while(threshold <= 5.01e-2){
			if(pValue <= threshold){
				System.out.println(String.format("Overall F1-score %.2f%% vs %.2f%% is significant. (p ≤ %s -- %d/%d = %.6g)", 100*overallResult1.calculateF1(), 100*overallResult2.calculateF1(), threshold, oneMoreThanTwoCount[last], n, pValue));
				break;
			}
			if(flag){
				threshold *= 5;
				flag = false;
			} else {
				threshold *= 2;
				flag = true;
			}
		}
		if(threshold > 5.01e-2){
			System.out.println(String.format("Overall F1-score %.2f%% vs %.2f%% is not significant. (p > 0.05 -- %d/%d = %.6g)", 100*overallResult1.calculateF1(), 100*overallResult2.calculateF1(), oneMoreThanTwoCount[last], n, pValue));
		}
	}
	
	private static Statistics[] getSampledStatistics(Instance[] predictions, boolean useRelaxedCriterion, boolean ignoreNested, int[] selectedIndices){
		int popSize = predictions.length;
		Instance[] sampledInstances = new Instance[predictions.length];
		for(int k=0; k<popSize; k++){
			sampledInstances[k] = predictions[selectedIndices[k]];
		}
		Statistics[] typedStatistics = getScore(sampledInstances, useRelaxedCriterion, ignoreNested);
		Statistics[] result = new Statistics[typedStatistics.length+1];
		for(int i=0; i<typedStatistics.length; i++){
			result[i] = typedStatistics[i];
		}
		result[result.length-1] = sum(typedStatistics);
		return result;
	}
	
	public static void evaluateBootstrap(Instance[] predictions, PrintStream outstream, int n, boolean useRelaxedCriterion, boolean ignoreNested){
		Random rand = new Random(17);
		int popSize = predictions.length;
		Statistics[] microAverageScores = new Statistics[n];
		double[] microPrecisions = new double[n];
		double[] microRecalls = new double[n];
		double[] microF1s = new double[n];
		for(int i=0; i<n; i++){
			microAverageScores[i] = new Statistics();
		}
		for(int labelID=0; labelID<SpanLabel.LABELS.size(); labelID++){
			double[] precisions = new double[n];
			double[] recalls = new double[n];
			double[] f1s = new double[n];
			for(int i=0; i<n; i++){
				Instance[] sampledPredictions = new Instance[popSize];
				for(int j=0; j<popSize; j++){
					sampledPredictions[j] = predictions[rand.nextInt(popSize)];
				}
				Statistics[] sampledResult = getScore(sampledPredictions, useRelaxedCriterion, ignoreNested);
				microAverageScores[i].add(sampledResult[labelID]);
				if(sampledResult[labelID].totalGold == sampledResult[labelID].totalPred && sampledResult[labelID].totalGold == 0){
					precisions[i] = -1;
					recalls[i] = -1;
					f1s[i] = -1;
				} else {
					precisions[i] = sampledResult[labelID].calculatePrecision();
					recalls[i] = sampledResult[labelID].calculateRecall();
					f1s[i] = sampledResult[labelID].calculateF1();
				}
			}
			ConfidenceInterval precisionCI = calculateCI(precisions);
			ConfidenceInterval recallCI = calculateCI(recalls);
			ConfidenceInterval f1CI = calculateCI(f1s);
			System.out.println("For label: "+SpanLabel.get(labelID));
			System.out.printf("Precision: %.2f(±%.2f)%% Recall: %.2f(±%.2f)%% F1: %.2f(±%.2f)%%\n",
					100*precisionCI.value, 100*precisionCI.margin,
					100*recallCI.value, 100*recallCI.margin,
					100*f1CI.value, 100*f1CI.margin);
		}
		for(int i=0; i<n; i++){
			microPrecisions[i] = microAverageScores[i].calculatePrecision();
			microRecalls[i] = microAverageScores[i].calculateRecall();
			microF1s[i] = microAverageScores[i].calculateF1();
		}
		ConfidenceInterval precisionCI = calculateCI(microPrecisions);
		ConfidenceInterval recallCI = calculateCI(microRecalls);
		ConfidenceInterval f1CI = calculateCI(microF1s);
		System.out.println("Overall Scores");
		System.out.printf("Precision: %.2f(±%.2f)%% Recall: %.2f(±%.2f)%% F1: %.2f(±%.2f)%%\n",
				100*precisionCI.value, 100*precisionCI.margin,
				100*recallCI.value, 100*recallCI.margin,
				100*f1CI.value, 100*f1CI.margin);
	}
	
	private static ConfidenceInterval calculateCI(double[] values){
		int count = (int)Arrays.stream(values).filter((double val) -> val >= 0).count();
		double mean = Arrays.stream(values).filter((double val) -> val >= 0).average().getAsDouble();
		double stdDev = Math.sqrt(Arrays.stream(values).filter((double val) -> val >= 0).reduce(0, (double result, double val) -> result+Math.pow(val-mean, 2))/(count-1));
		double margin = 1.96 * stdDev / Math.sqrt(count);
		return new ConfidenceInterval(mean, margin);
	}
	
	private static class ConfidenceInterval{
		public double value;
		public double margin;
		public ConfidenceInterval(double value, double margin){
			this.value = value;
			this.margin = margin;
		}
		
//		public boolean overlap(ConfidenceInterval interval){
//			double low = value-margin;
//			double high = value+margin;
//			double iLow = interval.value-interval.margin;
//			double iHigh = interval.value+interval.margin;
//			return (iLow < low && low < iHigh) || (low < iLow && iLow < high);
//		}
	}
	
	private static String entityListToString(List<EntitySpan> entities, OEInstance instance){
		StringBuilder builder = new StringBuilder();
		for(EntitySpan entitySpan: entities){
			if(builder.length() > 0){
				builder.append(", ");
			}
			builder.append(entitySpan.toString(instance.input));
		}
		builder.insert(0, "(");
		builder.append(")");
		return builder.toString();
	}
	
	public static Statistics[] evaluate(Instance[] predictions, PrintStream outstream, int printLimit, boolean printOnlyMistakes, boolean noPrint, boolean useRelaxedCriterion, boolean ignoreOverlaps){
		int count = 0;
		PrintStream[] outstreams = new PrintStream[]{outstream, System.out};
		if(noPrint){
			outstreams = new PrintStream[]{null};
		}
		Statistics finalResult = new Statistics();
		for(Instance inst: predictions){
			if(count >= printLimit && !noPrint){
				outstreams = new PrintStream[]{outstream};
			}
			OEInstance instance = (OEInstance)inst;
			Statistics[] scores = getScore(new Instance[]{instance}, useRelaxedCriterion, ignoreOverlaps);
			Statistics overall = sum(scores);
			finalResult.add(overall);
			boolean shouldPrint = true;
			if(printOnlyMistakes && (overall.calculateF1() == 1.0 || overall.totalGold+overall.totalPred == 0)){
				shouldPrint = false;
			}
			if(shouldPrint){
				print("Input:", true, outstreams);
				print(instance.input, true, outstreams);
				print("Gold:", true, outstreams);
				print(entityListToString(instance.output, instance), true, outstreams);
				print("Prediction:", true, outstreams);
				print(entityListToString(instance.prediction, instance), true, outstreams);
				if(instance.predictionTokenized != null){
					print("Prediction tokenized:", true, outstreams);
					print(instance.predictionTokenized.toString(), true, outstreams);
				}
				overall.printScore(outstreams);
				print("", true, outstreams);
				printDetailedScore(scores, outstreams);
				print("", true, outstreams);
				count += 1;
			}
		}
		if(printLimit > 0){
			print("", true, outstream, System.out);
		} else {
			print("", true, outstreams);
		}
		outstreams = new PrintStream[]{outstream, System.out};
		print("### Overall score ###", true, outstream, System.out);
		finalResult.printScore(outstreams);
		print("", true, outstream, System.out);
		Statistics[] scores = getScore(predictions, useRelaxedCriterion, ignoreOverlaps);
		printDetailedScore(scores, outstream, System.out);
		return scores;
	}
	
	private static Statistics sum(Statistics[] scores){
		Statistics result = new Statistics();
		for(Statistics score: scores){
			result.add(score);
		}
		return result;
	}
	
	private static Statistics[] getScore(Instance[] instances, boolean useRelaxedCriterion, boolean ignoreOverlaps){
		int size = SpanLabel.LABELS.size();
		Statistics[] result = createStatistics(size);
		for(Instance inst: instances){
			OEInstance instance = (OEInstance)inst;
			List<EntitySpan> predicted;
			List<EntitySpan> actual;
			predicted = duplicate(instance.getPrediction(), ignoreOverlaps);
			actual = duplicate(instance.getOutput(), ignoreOverlaps);
			for(EntitySpan span: actual){
				if(useRelaxedCriterion){
					double maxJaccard = 0.0;
					EntitySpan maxPred = null;
					for(EntitySpan pred: predicted){
						if(pred.label != span.label) continue;
						double jaccard = jaccard(span, pred);
						if(jaccard > maxJaccard){
							maxJaccard = jaccard;
							maxPred = pred;
						}
					}
					if(maxPred != null){
						predicted.remove(maxPred);
						SpanLabel label = maxPred.label;
						result[label.id].correct += 1;
						result[label.id].totalPred += 1;
					}
					result[span.label.id].totalGold += 1;
				} else {
					if(predicted.contains(span)){
						predicted.remove(span);
						SpanLabel label = span.label;
						result[label.id].correct += 1;
						result[label.id].totalPred += 1;
					}
					result[span.label.id].totalGold += 1;
				}
			}
			for(EntitySpan span: predicted){
				result[span.label.id].totalPred += 1;
			}
		}
		return result;
	}
	
	private static double jaccard(EntitySpan entitySpan1, EntitySpan entitySpan2){
		int itrsc = 0;
		int union = 0;
		int start = Math.min(entitySpan1.start(), entitySpan2.start());
		int end = Math.max(entitySpan1.end(), entitySpan2.end());
		int[] charseq = new int[end-start];
		if(entitySpan1.overlapsWith(entitySpan2)){
			for(Span span: entitySpan1.spans){
				for(int i=span.start-start; i<span.end-start; i++){
					charseq[i] += 1;
				}
			}
			for(Span span: entitySpan2.spans){
				for(int i=span.start-start; i<span.end-start; i++){
					charseq[i] += 1;
				}
			}
			for(int i=0; i<end-start; i++){
				if(charseq[i] > 0){
					union += 1;
				}
				if(charseq[i] == 2){
					itrsc += 1;
				}
			}
		}
		return 1.0*itrsc/union;
	}
	
	private static Statistics[] createStatistics(int size){
		Statistics[] result = new Statistics[size];
		for(int i=0; i<size; i++){
			result[i] = new Statistics();
		}
		return result;
	}
	
	public static void printDetailedScore(Statistics[] result, PrintStream... outstreams){
		double avgF1 = 0;
		for(int i=0; i<result.length; i++){
			double precision = result[i].calculatePrecision();
			double recall = result[i].calculateRecall();
			double f1 = result[i].calculateF1();
			avgF1 += f1;
			print(String.format("%6s: #Corr:%2$3d, #Pred:%3$3d, #Gold:%4$3d, Pr=%5$#5.2f%% Rc=%6$#5.2f%% F1=%7$#5.2f%%", SpanLabel.get(i).form, result[i].correct, result[i].totalPred, result[i].totalGold, precision*100, recall*100, f1*100), true, outstreams);
		}
		print(String.format("Macro average F1: %.2f%%", 100*avgF1/result.length), true, outstreams);
	}
	
	private static List<EntitySpan> duplicate(List<EntitySpan> list, boolean ignoreOverlaps){
		List<EntitySpan> result = new ArrayList<EntitySpan>();
		for(EntitySpan span: list){
			boolean add = true;
			if(ignoreOverlaps){
				for(int i=result.size()-1; i>=0; i--){
					EntitySpan span2 = result.get(i);
					if(span2.overlapsWith(span)){
						// If we are to ignore nested and find discontiguous entities, remove the discontiguous entities
						if(span2.spans.length > 1 && span.spans.length == 1){
							result.remove(i);
						} else if(span2.spans.length == 1 && span.spans.length > 1){
							add = false;
							break;
						} else {
							// If both are discontiguous or both are contiguous, remove the shorter one.
							if(span2.end()-span2.start() >= span.end()-span.start()){
								add = false;
								break;
							} else {
								result.remove(i);
							}
						}
					}
				}
			}
			if(add){
				result.add(span);
			}
		}
		return result;
	}

}