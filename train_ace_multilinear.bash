mkdir "multilinear_model_ace2004"
expDir="multilinear_model_ace2004" algo="MULTILINEAR" useSpecificIndicator="true" trainPath="data/ACE2004/train.data" devPath="data/ACE2004/dev.data" testPath="data/ACE2004/dev.data" l2Opts="0.0 0.001 0.01 0.1 1.0" features="words,word_ngram,pos_tag,pos_tag_ngram,wordtype,wordtype_ngram,bow,orthographic,transition,mention_penalty" halfWindowSize="3" ngramSize="4" halfBowSize="5" time bash run_multiple.bash
mkdir "multilinear_model_ace2005"
expDir="multilinear_model_ace2005" algo="MULTILINEAR" useSpecificIndicator="true" trainPath="data/ACE2005/train.data" devPath="data/ACE2005/dev.data" testPath="data/ACE2005/dev.data" l2Opts="0.0 0.001 0.01 0.1 1.0" features="words,word_ngram,pos_tag,pos_tag_ngram,wordtype,wordtype_ngram,bow,orthographic,transition,mention_penalty" halfWindowSize="3" ngramSize="4" halfBowSize="5" time bash run_multiple.bash

#mkdir "multilinear_model_ace2004.no_trans"
#expDir="multilinear_model_ace2004.no_trans" algo="MULTILINEAR" useSpecificIndicator="true" trainPath="data/ACE2004/train.data" devPath="data/ACE2004/dev.data" testPath="data/ACE2004/dev.data" l2Opts="0.0 0.001 0.01 0.1 1.0" features="words,word_ngram,pos_tag,pos_tag_ngram,wordtype,wordtype_ngram,bow,orthographic,mention_penalty" halfWindowSize="3" ngramSize="4" halfBowSize="5" time bash run_multiple.bash
#mkdir "multilinear_model_ace2005.no_trans"
#expDir="multilinear_model_ace2005.no_trans" algo="MULTILINEAR" useSpecificIndicator="true" trainPath="data/ACE2005/train.data" devPath="data/ACE2005/dev.data" testPath="data/ACE2005/dev.data" l2Opts="0.0 0.001 0.01 0.1 1.0" features="words,word_ngram,pos_tag,pos_tag_ngram,wordtype,wordtype_ngram,bow,orthographic,mention_penalty" halfWindowSize="3" ngramSize="4" halfBowSize="5" time bash run_multiple.bash
