# -*- coding: utf-8 -*-
"""
13 Jan 2017
A script to include POS tags into the dataset
"""

# Import statements
from __future__ import print_function
import sys
from bs4 import BeautifulSoup as BS
import re
from justhalf.log import Timing

def normalize_text(text):
    return text.replace(' ', '').replace('*PLUS*', '+').replace('&gt;','>').replace('&lt;','<').replace('&amp;','&')

class Sentence(object):
    def __init__(self, word_tags):
        if word_tags[0][1] == 'LS' or word_tags[0][0].isdigit():
            if len(word_tags) <= 3 or (word_tags[2][0].isdigit() and len(word_tags) <= 4):
                self.word_tags = None
                self.text_nospace = None
                self.text = None
                return
            if word_tags[1][1] == ':':
                self.word_tags = word_tags[2:]
            else:
                self.word_tags = word_tags
        else:
            self.word_tags = word_tags
        try:
            self.text_nospace = ''.join(list(zip(*self.word_tags))[0])
            self.text = ' '.join(list(zip(*self.word_tags))[0])
        except:
            print(word_tags)
            print(self.word_tags)
            raise

    def is_valid(self):
        return self.word_tags is not None

    def same_as(self, text):
        if text.endswith('.') and not self.text_nospace.endswith('.'):
            text = text[:-1]
        return self.text_nospace == normalize_text(text)

class Annotation(object):
    def __init__(self, text):
        try:
            start_end, self.label = text.split(' ')
        except:
            print(text)
            raise
        self.start, self.end = [int(n) for n in start_end.split(',')[:2]]

    def __str__(self):
        return self.__repr__()
    
    def __repr__(self):
        return '{},{},{},{} {}'.format(self.start, self.end, self.start, self.end, self.label)

class Adjustment(object):
    def __init__(self):
        self.values = []

    def add(self, idx, val):
        self.values.append((idx, val))

    def adjust(self, annotation):
        annotation.start = self._adjust(annotation.start)
        annotation.end = self._adjust(annotation.end, -1)
        return annotation

    def _adjust(self, num, offset=0):
        new_num = num
        for value in self.values:
            if new_num+offset >= value[0]:
                new_num += value[1]
        return new_num

class AnnotatedSentence(object):
    def __init__(self, word_tags, orig_tokens, orig_annotations):
        self.word_tags = word_tags
        self.orig_tokens = orig_tokens
        self.orig_annotations = orig_annotations
        self.adjust_annotations()

    def adjust_annotations(self):
        self.new_annotations = []
        adjustment = Adjustment()
        orig_orig_idx = 0
        orig_idx = 0
        new_idx = 0
        tokens = self.orig_tokens[:]
        if tokens[-1] == '.' and self.word_tags[-1][0] != '.':
            self.word_tags.append(('.', '.'))
        while orig_idx < len(tokens) or new_idx < len(self.word_tags):
            if self.word_tags[-1][0] == '.' and tokens[-1] != '.':
                tokens.append('.')
            try:
                token = normalize_text(tokens[orig_idx])
                word, pos = self.word_tags[new_idx]
            except:
                print(tokens)
                print(list(zip(*self.word_tags))[0])
                raise
            if token == word:
                orig_orig_idx += 1
                orig_idx += 1
                new_idx += 1
            else:
                if len(token) > len(word):
                    tokens.insert(orig_idx+1, token[len(word):])
                    tokens[orig_idx] = word
                    adjustment.add(orig_idx, 1)
                    orig_idx += 1
                    new_idx += 1
                    # print('"{}" longer than "{}", splitting. ({}, {})'.format(token, word, orig_idx, new_idx))
                elif len(token) < len(word):
                    tokens[orig_idx] += tokens[orig_idx+1]
                    del tokens[orig_idx+1]
                    adjustment.add(orig_idx+1, -1)
                    orig_orig_idx += 1
                    # print('"{}" shorter than "{}", combining, ({}, {})'.format(token, word, orig_idx, new_idx))
                else:
                    print(tokens[orig_idx-2:orig_idx+2])
                    print(self.word_tags[new_idx-2: new_idx+2])
                    raise Exception('Original differ with same length: {} vs {}'.format(token, word))
        if orig_idx != new_idx or orig_idx != len(tokens) or new_idx != len(self.word_tags):
            raise Exception('Assertion error: length not the same')
        for annotation in self.orig_annotations:
            self.new_annotations.append(adjustment.adjust(annotation))

    def __str__(self):
        return self.__repr__()
    
    def __repr__(self):
        words = ' '.join(list(zip(*self.word_tags))[0])
        pos = ' '.join(list(zip(*self.word_tags))[1])
        annotations = '|'.join(str(ann) for ann in self.new_annotations)
        result = '{}\n{}\n{}'.format(words, pos, annotations)
        return result

def main():
    with_postags = []
    if len(sys.argv) < 3:
        print('Usage: {} <POS-tagged file> <standoff annotated format> (any value here to do oversplitting)'.format(sys.argv[0]))
        sys.exit(1)
    postagged_filename = sys.argv[1]
    standoff_filename = sys.argv[2]
    oversplit = False
    if len(sys.argv) > 3:
        oversplit = True
    if postagged_filename.endswith('txt'):
        with Timing('Reading from text file {}...'.format(postagged_filename)):
            with open(postagged_filename, 'r') as infile:
                word_tags = []
                for line in infile:
                    line = line.strip()
                    if len(line) == 0:
                        continue
                    if '=====' in line:
                        sentence = Sentence(word_tags)
                        if sentence.is_valid():
                            with_postags.append(sentence)
                        word_tags = []
                    else:
                        if oversplit:
                            word, pos = [token.strip().replace(' ', '') for token in line.rsplit('/', 1)]
                            words = re.split('([-/.])', word)
                            for word in words:
                                if word == '':
                                    continue
                                word_tags.append([word, pos])
                        else:
                            word_tags.append([token.strip().replace(' ','') for token in line.rsplit('/', 1)])
    else:
        # split_word_set = set()
        with Timing('Reading from XML file {}...'.format(postagged_filename)):
            with open(postagged_filename, 'r') as infile:
                soup = BS(infile.read(), 'lxml')
            for sentence in soup.find_all('sentence'):
                word_tags = []
                # split_word = []
                for token in sentence.find_all('w'):
                    word_tags.append([token.get_text().strip().replace(' ', ''), token['c']])
                    if word_tags[-1][1] != '*':
                        idx = len(word_tags)-2
                        while idx >= 0 and word_tags[idx][1] == '*':
                            word_tags[idx][1] = word_tags[-1][1]
                            idx -= 1
                    # if word_tags[-1][1] == '*':
                    #     split_word.append(tuple(word_tags[-1]))
                    # elif len(word_tags) > 1 and word_tags[-1][1] != '*' and word_tags[-2][1] == '*':
                    #     split_word.append(tuple(word_tags[-1]))
                    #     split_word_set.add(tuple(split_word))
                    #     split_word = []
                sentence = Sentence(word_tags)
                if sentence.is_valid():
                    with_postags.append(sentence)
    # from pprint import pprint
    # pprint(split_word_set)
    if oversplit:
        output_filename = standoff_filename.replace('.data', '_withpos_oversplit.data')
    else:
        output_filename = standoff_filename.replace('.data', '_withpos.data')
    has_match = False
    annotated_sentences = []
    with open(standoff_filename, 'r') as infile:
        sentence_index = 0
        while True:
            tokenized = infile.readline().strip()
            if tokenized == '':
                break
            tokens = tokenized.split(' ')
            annotation_line = infile.readline().strip()
            try:
                annotations = [Annotation(text) for text in annotation_line.split('|') if text != '']
            except:
                print(annotation_line)
                raise
            blank_line = infile.readline().strip()
            if len(tokens) == 2 and tokens[0].isdigit() and tokens[1] == '.':
                continue
            sentence = with_postags[sentence_index]
            while not sentence.same_as(tokenized):
                if has_match:
                    print('Checking {}...'.format(tokenized), end='')
                    print('\n\tDoes not match at {}: {}'.format(sentence_index, sentence.text), end='')
                    print('\n\t{}\nvs\n\t{}'.format(tokenized.replace(' ',''), sentence.text_nospace))
                    sys.exit(1)
                sentence_index += 1
                sentence = with_postags[sentence_index]
            has_match = True
            sentence_index += 1
            annotated_sentences.append(AnnotatedSentence(sentence.word_tags, tokens, annotations))
    with open(output_filename, 'w') as outfile:
        for sentence in annotated_sentences:
            outfile.write('{}\n\n'.format(sentence))

if __name__ == '__main__':
    main()

