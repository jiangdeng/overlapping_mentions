# Print the statistics of the dataset
filter="all_instances"
echo "## Training data ##"
java -cp target/experiments-clinical-1.0-SNAPSHOT.jar com.statnlp.experiment.clinical.CTUtil getStats -sourceDir data/semeval-2014-task-7/data/train -annDir data/semeval-2014-task-7/data/train.gold -no_examples -no_sections -instanceFilter ${filter}
echo
echo "## Training data (80%)##"
java -cp target/experiments-clinical-1.0-SNAPSHOT.jar com.statnlp.experiment.clinical.CTUtil getStats -sourceDir data/semeval-2014-task-7/data/80train -annDir data/semeval-2014-task-7/data/80train.gold -no_examples -no_sections -instanceFilter ${filter}
echo
echo "## Training data (20%)##"
java -cp target/experiments-clinical-1.0-SNAPSHOT.jar com.statnlp.experiment.clinical.CTUtil getStats -sourceDir data/semeval-2014-task-7/data/20train -annDir data/semeval-2014-task-7/data/20train.gold -no_examples -no_sections -instanceFilter ${filter}
echo
echo "## Development data ##"
java -cp target/experiments-clinical-1.0-SNAPSHOT.jar com.statnlp.experiment.clinical.CTUtil getStats -sourceDir data/semeval-2014-task-7/data/dev -annDir data/semeval-2014-task-7/data/dev.gold -no_examples -no_sections -instanceFilter ${filter}
echo
echo "## Testing data ##"
java -cp target/experiments-clinical-1.0-SNAPSHOT.jar com.statnlp.experiment.clinical.CTUtil getStats -sourceDir data/semeval-2014-task-7/data/test -annDir data/semeval-2014-task-7/data/test.gold -no_examples -no_sections -instanceFilter ${filter}
