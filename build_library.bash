tar -zcf clinical-text-experiment.tar.gz \
    .aws/* \
    data/semeval-2014-task-7 \
    password_file \
    tokenized_clinical_text_combined-c100-p1.out \
    tokenized_clinical_text_combined-c1000-p1.out \
    experiments/run_one.bash \
    target/experiments-clinical-1.0-SNAPSHOT.jar
